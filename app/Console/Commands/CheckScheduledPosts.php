<?php

namespace App\Console\Commands;

use App\SocialLogin;
use Illuminate\Console\Command;
use App\Models\ScheduledPost;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Thujohn\Twitter\Facades\Twitter;
use Storage;

class CheckScheduledPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scheduled-posts:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for schedulded posts to be sended';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $timeNow = Carbon::now()->timestamp;
        $postsForSending = ScheduledPost::where('posting_time', '<=', $timeNow)
                                            ->get();
        foreach ($postsForSending as $post) {
            $this->sendPost($post);
            $post->socialLogins()->detach();
            $post->socialFbPages()->detach();
            $post->publicFbPages()->detach();
            Storage::delete($post->image_path);
            $post->delete();
        }
        $this->testCron();
    }

    protected function sendPost($post)
    {
        $fbLoginApiUrl = 'https://graph.facebook.com/me/photos';
        $fbLogins = $post->socialLogins()->where('social_logins.provider', 'facebook')->get();
        $twLogins = $post->socialLogins()->where('social_logins.provider', 'twitter')->get();
        $fbPages = $post->socialFbPages()->get();
        $publicFbPages = $post->publicFbPages()->get();
        foreach($fbLogins as $acc){
            $this->sendFacebookPost($acc, $post, $fbLoginApiUrl);
        }
        foreach($fbPages as $page){
            $fbPageApiUrl = 'https://graph.facebook.com/'.$page->fb_page_id.'/photos';
            $this->sendFacebookPost($page, $post, $fbPageApiUrl);
        }
        foreach($twLogins as $acc){
            $this->sendTwitterPost($acc, $post);
        }
        $client = new Client();
        foreach ($publicFbPages as $page) {
            $this->sendFacebookPostToPublic($page, $post,$client,$page);
        }
    }

    /**
     * sends post to public selected facebook pages
     *
     * @param $destination
     * @param $post
     * @param $photoApiUrl
     * @return array
     */
    protected function sendFacebookPostToPublic($destination, $post,$client, $page)
    {
        $postQuery = [];
        if($post->video_path !=''){
            $postQuery = ['access_token' => $destination->token,'message'=> $post->message.' '.$post->user_attached_link, 'link' => $post->video_path];
        }
        elseif($post->image_path != ''){
            $fbLoginApiUrl = 'https://graph.facebook.com/me/photos';
            $photoId = $this->uploadImageToFacebook($post->image_path,$client,$page, $fbLoginApiUrl);
            $postQuery = ['access_token' => $destination->token,'message'=> $post->message.' '.$post->user_attached_link, 'attached_media[0]' => '{"media_fbid":"'.$photoId.'"}'];
        }
        else{
            $postQuery = ['access_token' => $destination->token,'message'=> $post->message, 'link' => $post->user_attached_link];
        }
        $url = 'https://graph.facebook.com/'.$destination->social_id.'/feed';
        try {
            $result = $client->request('POST', $url, [
                    'query' => $postQuery
                ]
            );
            return ['status' => 'ok'];
        }
        catch(\Exception $ex){
            return ['status' => 'failed','message' => $ex->getMessage()];
        }
    }


    protected function sendFacebookPost($destination, $post, $photoApiUrl)
    {
        $client = new Client();
        $postQuery = [];
        if($post->video_path !=''){
            $postQuery = ['access_token' => $destination->token,'message'=> $post->message.' '.$post->user_attached_link, 'link' => $post->video_path];
        }
        elseif($post->image_path !=''){
            $photoId = $this->uploadImageToFacebook($post->image_path,$client,$destination, $photoApiUrl);
            $postQuery = ['access_token' => $destination->token,'message'=> $post->message.' '.$post->user_attached_link, 'attached_media[0]' => '{"media_fbid":"'.$photoId.'"}'];
        }
        else{
            $postQuery = ['access_token' => $destination->token,'message'=> $post->message, 'link' => $post->user_attached_link];
        }
        $url = 'https://graph.facebook.com/me/feed';
        try {
            $result = $client->request('POST', $url, [
                    'query' => $postQuery
                ]
            );
        }
        catch(\Exception $ex){
            return $ex->getMessage().' second catch';
        }
    }

    protected function sendTwitterPost($twUser, $post)
    {
        if($post->video_path !=''){
            $postParams = ['status' => $post->message.' '.$post->video_path, 'format' => 'json'];
        }
        elseif($post->image_path !=''){
            $photoId = $this->uploadImageToTwitter($post->image_path);
            $postParams = ['status' => $post->message.' '.$post->user_attached_link, 'format' => 'json', 'media_ids' => $photoId];
        }
        else {
            $postParams = ['status' => $post->message.' '.$post->user_attached_link, 'format' => 'json'];
        }
        Twitter::reconfig(['token' => $twUser->token, 'secret' => $twUser->token_secret]);
        Twitter::postTweet($postParams);
    }

    protected function testCron()
    {
        $testfile = fopen("cron_is_working.txt", "w");
        fwrite($testfile, Carbon::now()->toDateTimeString());
        fclose($testfile);
    }


    protected function uploadImageToFacebook($imagePath, $client, $fbDestination, $photoApiUrl)
    {
        $photo = Storage::get($imagePath);
        $filename = explode('/',$imagePath);
        $filename = end($filename);
        try {
            $result = $client->post($photoApiUrl, [
                'multipart' => [
                    [
                        'name'     => 'image',
                        'filename' => $filename,
                        'contents' => $photo,
                    ],
                ],
                'query' => ['access_token' => $fbDestination->token, 'published' => false]
            ]);
        } catch (\Exception $ex) {
            return $ex->getMessage().' first catch';
        }
        $photoId = json_decode($result->getBody()->getContents())->id;
        return $photoId;
    }

    protected function uploadImageToTwitter($imagePath)
    {
        $contents = Storage::get($imagePath);
        $uploaded_media_id = Twitter::uploadMedia(['media' => $contents])->media_id_string;
        return $uploaded_media_id;
    }
}
