<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class ComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $dashboardViews = ['backend.layouts.lte','backend.composer'];

        view()->composer(
            $dashboardViews,
            'App\Http\ViewComposers\ProfileComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(App\Http\ViewComposers\ProfileComposer::class);
    }
}
