<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialStreamCategory extends Model
{

    public function facebookPages()
    {
        return $this->hasMany('App\FacebookPage','category_id');
    }

    public function twitterPages()
    {
        return $this->hasMany('App\TwitterPage','category_id');
    }

    public function googleChannels()
    {
        return $this->hasMany('App\GooglePlusChannel','category_id');
    }

    public function youtubeChannels()
    {
        return $this->hasMany('App\YoutubeChannel','category_id');
    }
}
