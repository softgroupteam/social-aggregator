<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialFacebookPage extends Model
{
    public function User()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public static function getPages()
    {
        $pages = [];
        if($user = auth()->user()) {
            $pages = SocialFacebookPage::where('user_id',$user->id)->get();
        }
        return $pages;
    }

    public function scheduledPosts()
    {
        return $this->morphToMany('App\Models\ScheduledPost', 'destination', 'scheduled_posts_destination');
    }
}
