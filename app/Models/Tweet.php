<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    public function tweetHashtags()
    {
        return $this->hasMany('App\Models\TweetHashtag');
    }

    public function tweetMedias()
    {
        return $this->hasMany('App\Models\TweetMedia');
    }

    public function tweetUrls()
    {
        return $this->hasMany('App\Models\TweetUrl');
    }

    public function interestedUsers()
    {
        return $this->morphToMany('App\User', 'favouritepost', 'favourite_posts');
    }

    public function streamingUsers()
    {
        return $this->morphToMany('App\User', 'streampost', 'user_stream_posts');
    }

    public function UserWhoSaved()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
