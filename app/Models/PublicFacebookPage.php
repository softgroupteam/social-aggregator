<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicFacebookPage extends Model
{
    public function scheduledPosts()
    {
        return $this->morphToMany('App\Models\ScheduledPost', 'destination', 'scheduled_posts_destination');
    }
}
