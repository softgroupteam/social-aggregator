<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    public function userOccupation()
    {
        return $this->belongsTo('App\Models\UserOccupation','user_occupation_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
