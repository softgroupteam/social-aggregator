<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacebookPost extends Model
{
    public function facebookPostAttachments()
    {
        return $this->hasMany('App\Models\FacebookPostAttachment');
    }

    public function interestedUsers()
    {
        return $this->morphToMany('App\User', 'favouritepost', 'favourite_posts');
    }

    public function streamingUsers()
    {
        return $this->morphToMany('App\User', 'streampost', 'user_stream_posts');
    }

    public function UserWhoSaved()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
