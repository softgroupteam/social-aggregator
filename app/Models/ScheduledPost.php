<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduledPost extends Model
{
    public function socialLogins()
    {
        return $this->morphedByMany('App\SocialLogin', 'destination', 'scheduled_posts_destination');
    }

    public function socialFbPages()
    {
        return $this->morphedByMany('App\Models\SocialFacebookPage', 'destination', 'scheduled_posts_destination');
    }

    /**
     * Relationship with pages for promoting
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function publicFbPages()
    {
        return $this->morphedByMany('App\Models\PublicFacebookPage', 'destination', 'scheduled_posts_destination');
    }
}
