<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GooglePost extends Model
{
    public function googlePostAttachments()
    {
        return $this->hasMany('App\Models\GooglePostAttachment','post_id');
    }

    public function interestedUsers()
    {
        return $this->morphToMany('App\User', 'favouritepost', 'favourite_posts');
    }

    public function streamingUsers()
    {
        return $this->morphToMany('App\User', 'streampost', 'user_stream_posts');
    }

    public function UserWhoSaved()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
