<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GooglePostAttachment extends Model
{
    public function googlePost()
    {
        return $this->belongsTo('App\Models\GooglePost','post_id');
    }
}
