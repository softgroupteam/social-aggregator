<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacebookPromoteKeyword extends Model
{
    protected $fillable = array('name');
}
