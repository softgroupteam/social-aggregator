<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TweetHashtag extends Model
{
    public function tweet()
    {
        return $this->belongsTo('App\Models\Tweet','tweet_id');
    }
}
