<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOccupation extends Model
{
    public function user()
    {
        return $this->hasMany('App\Models\UserInfo','user_occupation_id');
    }
}
