<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    public function followers() // those who follow me
    {
        return $this->belongsTo(User::class,'follower_id');
    }

    public function following() // those who I follow
    {
        return $this->belongsTo(User::class,'followee_id');
    }
}
