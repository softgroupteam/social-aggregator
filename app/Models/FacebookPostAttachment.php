<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacebookPostAttachment extends Model
{
    public function facebookPost()
    {
        return $this->belongsTo('App\Models\FacebookPost','facebook_post_id');
    }
}
