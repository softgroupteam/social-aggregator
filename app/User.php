<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function socialLogins()
    {
        return $this->hasMany('App\SocialLogin');
    }

    public function userInfo()
    {
        return $this->hasOne('App\Models\UserInfo');
    }

    public function followers()
    {
        return $this->belongsToMany('App\User','followers','followee_id','follower_id');
    }

    public function following()
    {
        return $this->belongsToMany('App\User','followers','follower_id','followee_id');
    }


    public function favouriteFbPosts()
    {
        return $this->morphedByMany('App\Models\FacebookPost', 'favouritepost', 'favourite_posts');
    }

    public function favouriteTwPosts()
    {
        return $this->morphedByMany('App\Models\Tweet', 'favouritepost', 'favourite_posts');
    }

    public function favouriteYtVideos()
    {
        return $this->morphedByMany('App\Models\YoutubeVideo', 'favouritepost', 'favourite_posts');
    }

    public function favouriteGgPosts()
    {
        return $this->morphedByMany('App\Models\GooglePost', 'favouritepost', 'favourite_posts');
    }

    public function streamFbPosts()
    {
        return $this->morphedByMany('App\Models\FacebookPost', 'streampost', 'user_stream_posts');
    }

    public function streamTwPosts()
    {
        return $this->morphedByMany('App\Models\Tweet', 'streampost', 'user_stream_posts');
    }

    public function streamYtVideos()
    {
        return $this->morphedByMany('App\Models\YoutubeVideo', 'streampost', 'user_stream_posts');
    }

    public function streamGgPosts()
    {
        return $this->morphedByMany('App\Models\GooglePost', 'streampost', 'user_stream_posts');
    }

    public function savedFbPosts()
    {
        return $this->hasMany('App\Models\FacebookPost');
    }

    public function savedTwPosts()
    {
        return $this->hasMany('App\Models\Tweet');
    }

    public function savedYtVideos()
    {
        return $this->hasMany('App\Models\YoutubeVideo');
    }

    public function savedGgPosts()
    {
        return $this->hasMany('App\Models\GooglePost');
    }

    public function publicFbPages()
    {
        return $this->hasMany('App\Models\PublicFacebookPage');
    }

    public function promoteFbKeywords()
    {
        return $this->hasMany('App\Models\FacebookPromoteKeyword');
    }
}
