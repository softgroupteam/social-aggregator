<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TwitterPage extends Model
{
    public static function getPages()
    {
        if($user = auth()->user()) {
            return DB::table('twitter_pages')->where('user_id', $user->id)->get();
        }
        else
            return null;
    }

    public function category()
    {
        return $this->belongsTo('App\Models\SocialStreamCategory','category_id');
    }
}
