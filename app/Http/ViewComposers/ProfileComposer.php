<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\DB;
use App\SocialLogin;
use App\FacebookPage;
use App\YoutubeChannel;
use App\GooglePlusChannel;
use App\TwitterPage;
use App\Models\SocialFacebookPage;

class ProfileComposer
{

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */

    protected $accounts;
    protected $fbPages;
    protected $ytChannels;
    protected $gpChannels;
    protected $twPages;
    protected $fbSocialPages;


    public function getAccounts()
    {
        if($user = auth()->check()){
            $fbAccounts = SocialLogin::getAccounts('facebook')->toArray();
            $twAccounts = SocialLogin::getAccounts('twitter')->toArray();
            $ggAccounts = SocialLogin::getAccounts('google')->toArray();
            $fbPages = FacebookPage::getPages();
            $twPages = TwitterPage::getPages();
            $ytChannels = YoutubeChannel::getChannels();
            $gpChannels = GooglePlusChannel::getChannels();
            $fbSocialPages = SocialFacebookPage::getPages();
            $this->fbPages = $fbPages;
            $this->twPages = $twPages;
            $this->ytChannels = $ytChannels;
            $this->gpChannels = $gpChannels;
            $this->accounts['facebook'] = $fbAccounts;
            $this->accounts['twitter'] = $twAccounts;
            $this->accounts['google'] = $ggAccounts;
            $this->fbSocialPages = $fbSocialPages;
        }
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $this->getAccounts();
        $view->with('accounts', $this->accounts);
        $view->with('fbPages', $this->fbPages);
        $view->with('ytChannels', $this->ytChannels);
        $view->with('twPages', $this->twPages);
        $view->with('gpChannels', $this->gpChannels);
        $view->with('fbSocialPages', $this->fbSocialPages);
    }
}