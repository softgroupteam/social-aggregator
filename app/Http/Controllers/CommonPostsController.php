<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GooglePost;
use App\Models\FacebookPost;
use App\Models\Tweet;
use App\Models\YoutubeVideo;

class CommonPostsController extends Controller
{
    protected $limit;
    protected $timezone = '';

    public function __construct()
    {
        $this->limit = 3;
        if(array_key_exists('user_time_zone',$_COOKIE)){
            $this->timezone = $_COOKIE['user_time_zone'];
        }
    }

    public function getAllPosts(Request $request)
    {
        $data = [
            'posts' => [],
            'error' => ''
        ];
        $posts = [];
        $offset = $request->offset;
        $posts = array_merge($posts,$this->getGooglePosts($offset,true));
        $posts = array_merge($posts,$this->getFacebookPosts($offset,true));
        $posts = array_merge($posts,$this->getYoutubeVideos($offset,true));
        $posts = array_merge($posts,$this->getTwitterPosts($offset,true));
        $data = [
            'status' => 1,
            'posts' => $posts,
            'offset' => $offset+$this->limit,
        ];
        echo json_encode($data);
    }


    public function getGooglePosts($offset , $published , $allSaved = false)
    {
        $posts = [];
        if(!$allSaved) {
            $posts = GooglePost::where('published_here', $published)
                ->orderBy('social_created_at', 'desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        else
        {
            $posts = GooglePost::orderBy('social_created_at', 'desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['id'] = $post->id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
            $item['message'] = $post->content;
            $attachments = $post->googlePostAttachments();
            if($attachments->count()>0)
                $item['image_url'] = $attachments->first()->image_url;
            else
                $item['image_url'] = null;
            $item['filter_class'] = 'uss-google';
            $item['readmore_url'] = $post->url;
            $item['type'] = 'image';
            $item['created_at_timestamp'] = $post->social_created_at;
            array_push($resultPosts,$item);
        }
        return $resultPosts;

    }

    public function getFacebookPosts($offset, $published , $allSaved = false)
    {
        $posts = [];
        if(!$allSaved) {
        $posts = FacebookPost::where('published_here',true)
            ->orderBy('social_created_time','desc')
            ->offset($offset)
            ->limit($this->limit)
            ->get();
        }
        else{
            $posts = FacebookPost::orderBy('social_created_time','desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['id'] = $post->id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_time);
            $item['message'] = $post->message;
            $item['image_url'] = $post->fullpicture;
            $item['filter_class'] = 'uss-facebook';
            $item['type'] = 'image';
            $item['readmore_url'] = $post->permalink;
            $item['created_at_timestamp'] = $post->social_created_time;
            array_push($resultPosts,$item);
        }
        return $resultPosts;
    }

    public function getYoutubeVideos($offset , $published , $allSaved = false)
    {
        $posts = [];
        if(!$allSaved) {
            $posts = YoutubeVideo::where('published_here', true)
                ->orderBy('social_created_at', 'desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        else{
            $posts = YoutubeVideo::orderBy('social_created_at', 'desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['id'] = $post->id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
            $item['message'] = $post->title;
            $item['image_url'] = $post->thumbnail_standart;
            $item['filter_class'] = 'uss-youtube';
            $video_link = 'https://www.youtube.com/watch?v='.$post->video_id;
            $item['readmore_url'] = $video_link;
            $item['type'] = 'video';
            $embed_url = 'https://www.youtube.com/embed/'.$post->video_id;
            $item['embed_url'] = $embed_url;
            $item['created_at_timestamp'] = $post->social_created_at;
            array_push($resultPosts,$item);
        }
        return $resultPosts;
    }


    public function getTwitterPosts($offset , $published , $allSaved = false)
    {
        $posts = [];
        if(!$allSaved) {
            $posts = Tweet::where('published_here',true)
                ->orderBy('social_created_at','desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        else{
            $posts = Tweet::orderBy('social_created_at','desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['id'] = $post->id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
            $item['message'] = $post->text;
            $attachments = $post->tweetMedias();
            if($attachments->count()>0)
                $item['image_url'] = $attachments->first()->media_url;
            else
                $item['image_url'] = null;
            $item['filter_class'] = 'uss-twitter';
            $item['readmore_url'] = 'https://twitter.com/'.$post->posted_from_id.'/status/'.$post->post_id;
            $item['type'] = 'image';
            $item['created_at_timestamp'] = $post->social_created_at;
            array_push($resultPosts,$item);
        }
        return $resultPosts;
    }

    public function setPostLocaltime($timestamp)
    {
        $date = '';
        if($this->timezone != ''){
            $TZ = new \DateTimeZone($this->timezone);
            $date  = new \DateTime();
            $date->setTimestamp($timestamp);
            $date->setTimezone($TZ);
            $date = $date->format('Y-m-d H:i:s');
        }
        return $date;
    }
}
