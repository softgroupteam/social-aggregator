<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GooglePost;
use App\Models\FacebookPost;
use App\Models\Tweet;
use App\Models\YoutubeVideo;
use Illuminate\Support\Facades\DB;

class DashboardCommonPostsController extends Controller
{
    protected $limit;

    public function __construct()
    {
        $this->limit = 3;
    }

    public function getAllPosts(Request $request)
    {
        $data = [
            'posts' => [],
            'error' => ''
        ];
        $posts = [];
        $offset = $request->offset;
        $posts = array_merge($posts,$this->getGooglePosts($offset,true,true));
        $posts = array_merge($posts,$this->getFacebookPosts($offset,true,true));
        $posts = array_merge($posts,$this->getYoutubeVideos($offset,true,true));
        $posts = array_merge($posts,$this->getTwitterPosts($offset,true,true));
        $data = [
            'status' => 1,
            'posts' => $posts,
            'offset' => $offset+$this->limit,
        ];
        echo json_encode($data);
    }


    public function getGooglePosts($offset , $published , $allSaved = false)
    {
        $user = auth()->user();
        $posts = [];
        if(!$allSaved) {
            $posts = GooglePost::where('published_here', $published)
                ->where('user_id',$user->id)
                ->orderBy('created_at', 'desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        else
        {
            $posts = GooglePost::where('user_id',$user->id)
                ->orderBy('created_at', 'desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['local_id'] = $post->id;
            $item['id'] = $post->post_id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
            $item['message'] = $post->content;
            $attachments = $post->googlePostAttachments();
            if($attachments->count()>0)
                $item['image_url'] = $attachments->first()->image_url;
            else
                $item['image_url'] = null;
            $item['filter_class'] = 'uss-google';
            $item['readmore_url'] = $post->url;
            $item['type'] = 'image';
            $item['created_at_timestamp'] = $post->social_created_at;
            $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
            $item['social_type'] = 'google';
            $item['save_action'] = route('ajax.store-gglpost');
            $item['delete_action'] = route('ajax.delete-gglpost');
            $item['publish_action'] = route('ajax.publish-gglpost');
            $item['unpublish_action'] = route('ajax.unpublish-gglpost');
            $item['raw_message'] = htmlspecialchars($post->raw_message);
            array_push($resultPosts,$item);
        }
        return $resultPosts;

    }

    public function getFacebookPosts($offset, $published , $allSaved = false)
    {
        $user  = auth()->user();
        $posts = [];
        $state = '';
        if(!$allSaved) {
            $posts = FacebookPost::where('published_here',true)->with('facebookPostAttachments')
                ->where('user_id',$user->id)
                ->orderBy('created_at','desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        else{
            $posts = FacebookPost::where('user_id',$user->id)->with('facebookPostAttachments')
                ->orderBy('created_at','desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['local_id'] = $post->id;
            $item['id'] = $post->post_id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_time);
            $item['message'] = $post->message;
            $item['image_url'] = $post->fullpicture;
            $item['filter_class'] = 'uss-facebook';
            $attachment = $post->facebookPostAttachments()->first();
            if($attachment->embed_video_html !== ''){
                $item['type'] = 'video';
                $item['embed_video_html'] = $attachment->embed_video_html;
            }
            else{
                $item['type'] = 'image';
            }
            $item['readmore_url'] = $post->permalink;
            $item['created_at_timestamp'] = $post->social_created_time;
            $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
            $item['social_type'] = 'facebook';
            $item['save_action'] = route('ajax.store-fbpost');
            $item['delete_action'] = route('ajax.delete-fbpost');
            $item['publish_action'] = route('ajax.publish-fbpost');
            $item['unpublish_action'] = route('ajax.unpublish-fbpost');
            $item['raw_message'] = htmlspecialchars($post->raw_message);
            array_push($resultPosts,$item);
        }
        return $resultPosts;
    }

    public function getYoutubeVideos($offset , $published , $allSaved = false)
    {
        $user = auth()->user();
        $posts = [];
        $state = '';
        if(!$allSaved) {
            $posts = YoutubeVideo::where('published_here', true)
                ->where('user_id',$user->id)
                ->orderBy('created_at', 'desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        else{
            $posts = YoutubeVideo::where('user_id',$user->id)
                ->orderBy('created_at', 'desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['local_id'] = $post->id;
            $item['id'] = $post->post_id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
            $item['message'] = $post->title;
            $item['image_url'] = $post->thumbnail_standart;
            $item['filter_class'] = 'uss-youtube';
            $video_link = 'https://www.youtube.com/watch?v='.$post->video_id;
            $item['readmore_url'] = $video_link;
            $item['type'] = 'video';
            $item['embed_video_html'] = $post->embed_html;
            $embed_url = 'https://www.youtube.com/embed/'.$post->video_id;
            $item['embed_url'] = $embed_url;
            $item['created_at_timestamp'] = $post->social_created_at;
            $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
            $item['social_type'] = 'youtube';
            $item['save_action'] = route('ajax.store-ytpost');
            $item['delete_action'] = route('ajax.delete-ytpost');
            $item['publish_action'] = route('ajax.publish-ytpost');
            $item['unpublish_action'] = route('ajax.unpublish-ytpost');
            $item['raw_message'] = htmlspecialchars($post->title);
            array_push($resultPosts,$item);
        }
        return $resultPosts;
    }


    public function getTwitterPosts($offset , $published , $allSaved = false)
    {
        $user = auth()->user();
        $posts = [];
        $state = '';
        if(!$allSaved) {
            $posts = Tweet::where('published_here',true)
                ->where('user_id',$user->id)
                ->orderBy('created_at','desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        else{
            $posts = Tweet::where('user_id',$user->id)
                ->orderBy('created_at','desc')
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }
        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['local_id'] = $post->id;
            $item['id'] = $post->post_id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
            $item['message'] = $post->text;
            $attachments = $post->tweetMedias();
            if($attachments->count()>0)
                $item['image_url'] = $attachments->first()->media_url;
            else
                $item['image_url'] = null;
            $item['filter_class'] = 'uss-twitter';
            $item['readmore_url'] = 'https://twitter.com/'.$post->posted_from_id.'/status/'.$post->post_id;
            $item['type'] = 'image';
            $item['created_at_timestamp'] = $post->social_created_at;
            $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
            $item['social_type'] = 'twitter';
            $item['save_action'] = route('ajax.store-twpost');
            $item['delete_action'] = route('ajax.delete-twpost');
            $item['publish_action'] = route('ajax.publish-twpost');
            $item['unpublish_action'] = route('ajax.unpublish-twpost');
            $item['raw_message'] = htmlspecialchars($post->raw_message);
            array_push($resultPosts,$item);
        }
        return $resultPosts;
    }

    public function setPostLocaltime($timestamp)
    {
        $date = '';
        $timezone = auth()->user()->userInfo()->first()->timezone;
        if($timezone != ''){
            $TZ = new \DateTimeZone($timezone);
            $date  = new \DateTime();
            $date->setTimestamp($timestamp);
            $date->setTimezone($TZ);
            $date = $date->format('Y-m-d H:i:s');
        }
        return $date;
    }

    public function addToFavourite(Request $request)
    {
        $postId = $request->input('post_id');
        $postType = $request->input('post_type');
        $postClass = '';
        switch($postType){
            case 'facebook':
                $postClass = 'App\Models\FacebookPost';
                break;
            case 'twitter':
                $postClass = 'App\Models\Tweet';
                break;
            case 'youtube':
                $postClass = 'App\Models\YoutubeVideo';
                break;
            case 'google':
                $postClass = 'App\Models\GooglePost';
                break;
        }


        DB::table('favourite_posts')->insert(
            ['user_id' => auth()->user()->id, 'favouritepost_id' => $postId, 'favouritepost_type' => $postClass]);
        $data = ['status'=>'success', 'success_msg' => 'success','post_id' => $postId,'post_type' => $postType];
        return $data;
    }

    public function deleteFromFavourite(Request $request)
    {
        $postId = $request->input('post_id');
        $postType = $request->input('post_type');
        $postClass = '';
        switch($postType){
            case 'facebook':
                $postClass = 'App\Models\FacebookPost';
                break;
            case 'twitter':
                $postClass = 'App\Models\Tweet';
                break;
            case 'youtube':
                $postClass = 'App\Models\YoutubeVideo';
                break;
            case 'google':
                $postClass = 'App\Models\GooglePost';
                break;
        }


        DB::table('favourite_posts')->where('user_id', auth()->user()->id)
                                    ->where('favouritepost_id', $postId)
                                    ->where('favouritepost_type', $postClass)
                                    ->delete();
        $data = ['status'=>'success', 'success_msg' => 'success','post_id' => $postId,'post_type' => $postType];
        return $data;
    }
}
