<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FacebookPage;
use App\YoutubeChannel;
use App\GooglePlusChannel;
use App\TwitterPage;
use App\SocialLogin;
use App\Models\SocialStreamCategory;
use Laravel\Socialite\SocialiteManager;
use App\Models\SocialFacebookPage;


class ModalController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function addFacebookPage(Request $request)
    {
        $mName = $request->fbpageMachineName;
        $title = $request->fbpageTitle;
        $category_id = $request->category;
        $category = SocialStreamCategory::find($category_id);
        $userId = auth()->user()->id;
        $mName  = filter_var($mName , FILTER_SANITIZE_STRING);
        $title  = filter_var($title , FILTER_SANITIZE_STRING);
        if(strlen($mName) > 0 && strlen($title) > 0) {
            $page = new FacebookPage();
            $page->title = $title;
            $page->machine_name = $mName;
            $page->user_id = $userId;
            if($category!=null)
                $category->facebookPages()->save($page);
            else
                $page->category_id = null;
            $page->save();
            return response()->json([
                'status' => 'ok'
            ]);
        }
        else
            return response()->json([
                'status' => 'failed'
            ]);
    }

    public function addYoutubeChannel(Request $request)
    {
        $channelId = $request->ytchannelId;
        $title = $request->ytchannelTitle;
        $category_id = $request->category;
        $category = SocialStreamCategory::find($category_id);
        $userId = auth()->user()->id;
        $channelId  = trim(filter_var($channelId , FILTER_SANITIZE_STRING));
        $title  = trim(filter_var($title , FILTER_SANITIZE_STRING));
        if(strlen($channelId) > 0 && strlen($title) > 0) {
            $page = new YoutubeChannel();
            $page->title = $title;
            $page->channel_id = $channelId;
            $page->user_id = $userId;
            if($category!=null)
                $category->youtubeChannels()->save($page);
            else
                $page->category_id = null;
            $page->save();
            return response()->json([
                'status' => 'ok'
            ]);
        }
        else
            return response()->json([
                'status' => 'failed'
            ]);
    }

    public function addGooglePlusChannel(Request $request)
    {
        $channelId = $request->gpchannelId;
        $title = $request->gpchannelTitle;
        $category_id = $request->category;
        $category = SocialStreamCategory::find($category_id);
        $userId = auth()->user()->id;
        $channelId  = trim(filter_var($channelId , FILTER_SANITIZE_STRING));
        $title  = trim(filter_var($title , FILTER_SANITIZE_STRING));
        if(strlen($channelId) > 0 && strlen($title) > 0) {
            $page = new GooglePlusChannel();
            $page->title = $title;
            $page->channel_id = $channelId;
            $page->user_id = $userId;
            if($category!=null)
                $category->googleChannels()->save($page);
            else
                $page->category_id = null;
            $page->save();
            return response()->json([
                'status' => 'ok'
            ]);
        }
        else
            return response()->json([
                'status' => 'failed'
            ]);
    }

    public function addTwitterPage(Request $request)
    {
        $title = $request->twpageTitle;
        $pageName = $request->twpageName;
        $category_id = $request->category;
        $category = SocialStreamCategory::find($category_id);
        $userId = auth()->user()->id;
        $pageName  = trim(filter_var($pageName , FILTER_SANITIZE_STRING));
        $title  = trim(filter_var($title , FILTER_SANITIZE_STRING));
        if(strlen($pageName) > 0 && strlen($title) > 0) {
            $page = new TwitterPage();
            $page->title = $title;
            $page->page_name = $pageName;
            $page->user_id = $userId;
            if($category!=null)
                $category->twitterPages()->save($page);
            else
                $page->category_id = null;
            $page->save();
            return response()->json([
                'status' => 'ok'
            ]);
        }
        else
            return response()->json([
                'status' => 'failed'
            ]);
    }

    public function deleteSocialTab(Request $request)
    {
        $id = $request->id;
        $social = $request->social;
        $tab = null;
        switch($social){
            case 'facebook':
                $tab = FacebookPage::find($id);
                break;
            case 'youtube':
                $tab = YoutubeChannel::find($id);
                break;
            case 'google_plus':
                $tab = GooglePlusChannel::find($id);
                break;
            case 'twitter':
                $tab = TwitterPage::find($id);
                break;
        }
        if($tab != null){
            $tab->delete();
            return response()->json([
                'status' => 'ok'
            ]);
        }
        else
            return response()->json([
                'status' => 'failed'
            ]);
    }

    public function deleteSocialAcc(Request $request)
    {
        $id = $request->id;
        $acc = SocialLogin::find($id);
        if($acc != null){
            $acc->delete();
            return response()->json([
                'status' => 'ok'
            ]);
        }
        else
            return response()->json([
                'status' => 'failed'
            ]);
    }

    public function deleteSocialFbPage(Request $request)
    {
        $id = $request->id;
        $page = SocialFacebookPage::find($id);
        if($page != null){
            $page->delete();
            return response()->json([
                'status' => 'ok'
            ]);
        }
        else
            return response()->json([
                'status' => 'failed'
            ]);
    }

    public function addCategory(Request $request)
    {
        $name = trim($request->name);
        $category = new SocialStreamCategory();
        $category->text = $name;
        $category->save();
    }

    public function getCategories()
    {
        $categories = SocialStreamCategory::get();
        return $categories;
    }

}
