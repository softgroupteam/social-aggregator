<?php

namespace App\Http\Controllers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\Request;
use App\Models\ScheduledPost;
use Carbon\Carbon;
use Storage;
use App\SocialLogin;
use App\Models\SocialFacebookPage;
use Illuminate\Support\Facades\DB;

class ScheduledPostsController extends Controller
{

    public function getScheduledPosts()
    {
        $user = auth()->user();
        $timezone = 'GMT+0';
        if(array_key_exists('user_time_zone',$_COOKIE)){
            $timezone = $_COOKIE['user_time_zone'];
        }
        $posts = ScheduledPost::where('user_id', $user->id)->get();
        $resultPosts = [];
        foreach ($posts as $post){
            $post['id'] = $post->id;
            if($post->embed_video_picture != '')
                $post['image_path'] = $post->embed_video_picture;
            elseif ($post->image_path != '')
                $post['image_path'] = Storage::url($post->image_path);
            else
                $post['image_path'] = '';
            $post['message'] = $post->message;
            $time = Carbon::createFromTimestamp($post->posting_time,$timezone)->toDateTimeString();
            $post['time'] = $time;
            $resultPosts[] = $post;
        }
        return ['data' => $resultPosts];
    }

    public function getScheduledPostInfo(Request $request)
    {
        $id = $request->input('postId');
        $post = ScheduledPost::where('id',$id)->with('socialLogins')->with('socialFbPages')->first();
        $resultPost = [];
        $resultPost['id'] = $post->id;
        $resultPost['message'] = $post->message;
        if($post->embed_video_html != ''){
            $resultPost['video_html'] = $post->embed_video_html;
            $resultPost['post_type'] = 'video';
        }
        else {
            $resultPost['image'] = ($post->image_path != '') ? Storage::url($post->image_path) : '';
            $resultPost['post_type'] = 'image';
        }
        $resultPost['socialLogins'] = [];
        $resultPost['socialFbPages'] = [];
        foreach ($post->socialLogins as $acc ){
            $resultPost['socialLogins'][] = ['id' => $acc->id, 'provider' => $acc->provider ];
        }
        foreach ($post->socialFbPages as $page ){
            $resultPost['socialFbPages'][] = ['id' => $page->id, 'provider' => 'facebook' ];
        }
        $timezone = 'GMT+0';
        if(array_key_exists('user_time_zone',$_COOKIE)){
            $timezone = $_COOKIE['user_time_zone'];
        }
        $time = Carbon::createFromTimestamp($post->posting_time,$timezone);
        $month = ($time->month < 10)? '0'.$time->month : $time->month;
        $day = ($time->day < 10)?'0'.$time->day:$time->day;
        $hour = ($time->hour < 10)?'0'.$time->hour:$time->hour;
        $minute = ($time->minute < 10)?'0'.$time->minute:$time->minute;
        $resultPost['date'] = $month.'/'.$day.'/'.$time->year;
        $resultPost['hour'] = $hour;
        $resultPost['minute'] = $minute;
        return $resultPost;
    }

    public function editScheduledPost(Request $request)
    {
        $validationData = $this->validateComposerEditRequest($request);
        if($validationData['status'] == 'failed')
            return $validationData;
        $postId = $request->input('postId');
        $fbAccounts = ($request->has('fbaccounts'))?array_values($request->input('fbaccounts')):[];
        $twAccounts = ($request->has('twaccounts'))?array_values($request->input('twaccounts')):[];
        $fbSocialPages = ($request->has('fbsocialpages'))?array_values($request->input('fbsocialpages')):[];
        $localPhoto = ($request->hasFile('composer-post-image'))?$request->file('composer-post-image'):'';
        $existingPhoto = ($request->hasFile('edited-image-url'))?$request->file('edited-image-url'):'';
        $msg = ($request->has('message-text'))?$request->input('message-text'):'';
        $postingTime = $this->convertPostdateToTimestamp($request->input('scheduled-post-date'),$request->input('scheduled-post-hours'),$request->input('scheduled-post-minutes'));
        $userAttachedLink = ($request->has('user-attached-link'))?$request->input('user-attached-link'):'';
        $includePromotePages = ($request->has('include-promote-pages'))? true : false;
        $this->saveEditedScheduledPost($postId, $fbAccounts, $fbSocialPages, $twAccounts, $localPhoto, $existingPhoto, $msg ,$postingTime, $userAttachedLink, $includePromotePages);
        return $validationData;
    }

    public function deleteScheduledPost(Request $request){
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => ''
        ];
        $postId = $request->input('postId');
        $post = ScheduledPost::where('id', $postId)->first();
        $post->socialLogins()->detach();
        $post->socialFbPages()->detach();
        if($post->image_path != '')
            Storage::delete($post->image_path);
        $post->delete();
        $data['status'] = 'success';
        $data['success_msg'] = 'Post has been deleted';
        return $data;
    }

    public function massiveDeleteScheduledPosts(Request $request){
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => ''
        ];
        $postIds = json_decode($request->input('postId'));
        foreach($postIds as $id) {
            $post = ScheduledPost::where('id', $id)->first();
            $post->socialLogins()->detach();
            $post->socialFbPages()->detach();
            if ($post->image_path != '')
                Storage::delete($post->image_path);
            $post->delete();
        }
        $data['status'] = 'success';
        $data['success_msg'] = 'Post has been deleted';
        return $data;
    }

    protected function saveEditedScheduledPost($postId, $fbAccounts, $fbSocialPages, $twAccounts, $localPhoto,$existingPhoto, $msg ,$postingTime, $userAttachedLink, $includePromotePages)
    {
        $post = ScheduledPost::where('id',$postId)->first();
        $post->socialLogins()->detach();
        $post->socialFbPages()->detach();
        if($localPhoto != '') {
            Storage::delete($post->image_path);
            $imagePath = $this->saveLocalScheduledImage($localPhoto);
            $post->image_path = $imagePath;
        }
        else if($existingPhoto == ''){
            Storage::delete($post->image_path);
            $post->image_path = $existingPhoto;
        }
        $post->message = $msg;
        $post->posting_time = $postingTime;
        $post->user_attached_link = $userAttachedLink;
        $post->save();
        foreach($fbAccounts as $acc){
            DB::table('scheduled_posts_destination')->insert(
                ['scheduled_post_id' => $post->id, 'destination_id' => $acc, 'destination_type' => SocialLogin::class, 'created_at' => new \DateTime(),'updated_at' => new \DateTime()]
            );
        }
        foreach($fbSocialPages as $page){
            DB::table('scheduled_posts_destination')->insert(
                ['scheduled_post_id' => $post->id, 'destination_id' => $page, 'destination_type' => SocialFacebookPage::class, 'created_at' => new \DateTime(),'updated_at' => new \DateTime()]
            );
        }
        foreach($twAccounts as $acc){
            DB::table('scheduled_posts_destination')->insert(
                ['scheduled_post_id' => $post->id, 'destination_id' => $acc, 'destination_type' => SocialLogin::class, 'created_at' => new \DateTime(),'updated_at' => new \DateTime()]
            );
        }
        if($includePromotePages){
            $pages = auth()->user()->publicFbPages()->get();
            foreach ($pages as $page){
                DB::table('scheduled_posts_destination')->insert(
                    ['scheduled_post_id' => $post->id, 'destination_id' => $page->id, 'destination_type' => PublicFacebookPage::class, 'created_at' => new \DateTime(),'updated_at' => new \DateTime()]
                );
            }
        }
    }

    protected function savelocalScheduledImage($localPhoto)
    {
        $path = $localPhoto->store('public/scheduled_posts_images');
        return  $path;
    }

    protected function validateComposerEditRequest($request)
    {
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => ''
        ];
        if(strlen($request->input('message-text')) == 0)
            $data['errors'][] = 'Message is empty';
        if($request->has('twaccounts') && strlen($request->input('message-text'))>140)
            $data['errors'][] = 'You can`t post over 140 symbols due to twitter restrictions';
        if(!$request->has('fbaccounts') && !$request->has('twaccounts') && !$request->has('ggaccounts') && !$request->has('fbsocialpages') && !$request->has('user-attached-link'))
            $data['errors'][] = 'Choose at least one account';

        if ($request->input('scheduled-post-date') == '')
                $data['errors'][] = 'Choose schedule date';
        if(count($data['errors'])>0)
            $data['status'] = 'failed';
        else
            $data['status'] = 'success';
        $data['success_msg'] = 'Post has been saved to schedule';
        return $data;
    }

    protected function convertPostdateToTimestamp($date, $hours = 'HH', $minutes = 'MM')
    {
        $date.=' 00:00:00';
        $timezone = 'GMT+0';
        if(array_key_exists('user_time_zone',$_COOKIE)){
            $timezone = $_COOKIE['user_time_zone'];
        }
        $time = Carbon::createFromFormat('m/d/yy H:i:s', $date,$timezone);
        $time->setTimezone('GMT+0');
        if($hours != 'HH')
            $time->addHours(intval($hours));
        if($minutes != 'MM')
            $time->addMinutes(intval($minutes));
        return $time->timestamp;
    }
}
