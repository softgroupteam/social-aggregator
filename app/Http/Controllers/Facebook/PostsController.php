<?php

namespace App\Http\Controllers\Facebook;

use App\Models\FacebookPostAttachment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\SocialLogin;
use App\Models\FacebookPost;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{

    protected $limit;

    public function __construct()
    {
        $this->middleware('auth');
        $this->limit = 9;
    }

    private function getSocialUser($accountId)
    {
        $user = auth()->user();
        $fbUser = $user->socialLogins()
            ->where('provider','facebook')
            ->where('provider_id', $accountId)
            ->first();
        return $fbUser;
    }

    public function getPagePosts(Request $request, $pageTitle)
    {
        $user = auth()->user();
        if($user->hasRole('user'))
            return $this->getPagePostsForUser($request,$pageTitle);

        $fbUser = $user->socialLogins()->where('user_id',$user->id)
                                        ->where('provider','facebook')
                                        ->first();
        $client = new Client();
        $nextPage = '';
        $url = 'https://graph.facebook.com/'.$pageTitle.'/posts';
        try {
            $posts = $client->request('GET', $url, [
                    'query' => ['access_token' => $fbUser->token,'date_format'=> 'U','limit' => $this->limit, 'fields' => 'link,full_picture,message,story,created_time,attachments,shares,comments.limit(0).summary(true),likes.limit(0).summary(true)']
                ]
            );
        }
        catch(\Exception $ex){
            return view('backend.noresults');
        }
        $posts = json_decode($posts->getBody()->getContents());
       //dd($posts);
        foreach ($posts->data as $key => $value) {
            if(!property_exists($value ,'message')||!property_exists($value,'full_picture') || !property_exists($value,'attachments')) {
                array_splice($posts->data, $key, 1);
            }
            if(isset($value->attachments)) {
                if (count($value->attachments->data) > 0) {
                    $attachment = $value->attachments->data[0];
                    switch ($attachment->type) {
                        case 'video_inline':
                        case 'video_autoplay':
                            $value->embed_video_html = $this->getVideoEmbedHtml('facebookVideo', $attachment->target->url, $client, $fbUser);
                            break;
                        case 'video_share_youtube':
                            $value->embed_video_html = $this->getVideoEmbedHtml('youtubeVideo', $attachment->target->url, $client, $fbUser);
                            break;
                        default:
                            $value->embed_video_html = '';
                    }
                }
            }
            else{
                $value->embed_video_html = '';
            }
        }


        $posts->data = $this->addProperties($posts->data);
        if(property_exists($posts,'paging')) {
            if(property_exists($posts->paging,'next'))
                $nextPage = $posts->paging->next;
        }
        return view('backend.facebook-page-posts',compact(['posts','pageTitle','nextPage']));
    }

    public function getPagePostsAjax(Request $request)
    {
        $user = auth()->user();
        $fbUser = $user->socialLogins()->where('user_id',$user->id)
            ->where('provider','facebook')
            ->first();
        $data = [
            'posts' => [],
            'error' => ''
        ];
        if($request->nextPage == '') {
            $data['error'] = 'page token is empty';
            $data['status'] = 0;
            echo json_encode($data);
        }
        $client = new Client();
        $posts = $client->request('GET', $request->nextPage);
        $posts = json_decode($posts->getBody()->getContents());
        foreach ($posts->data as $key => $value) {
            if(!property_exists($value ,'message')||!property_exists($value,'full_picture') || !property_exists($value,'attachments')) {
                array_splice($posts->data, $key, 1);
            }
            if(isset($value->attachments)) {
                if (count($value->attachments->data) > 0) {
                    $attachment = $value->attachments->data[0];
                    switch ($attachment->type) {
                        case 'video_inline':
                        case 'video_autoplay':
                            $value->embed_video_html = $this->getVideoEmbedHtml('facebookVideo', $attachment->target->url, $client, $fbUser);
                            break;
                        case 'video_share_youtube':
                            $value->embed_video_html = $this->getVideoEmbedHtml('youtubeVideo', $attachment->target->url, $client, $fbUser);
                            break;
                        default:
                            $value->embed_video_html = '';
                    }
                }
            }
            else{
                $value->embed_video_html = '';
            }
        }
        $posts->data = $this->addProperties($posts->data);
        if(property_exists($posts,'paging')) {
            $data['nextPage'] = $posts->paging->next;
        }
        $data['posts'] = $posts->data;
        $data['status'] = 1;
        //dd($data);
        echo json_encode($data);
    }

    public function setPostLocaltime($post)
    {
        $timezone = auth()->user()->userInfo()->first()->timezone;
        if($timezone != ''){
            $TZ = new \DateTimeZone($timezone);
            $date  = new \DateTime();
            $date->setTimestamp($post->created_time);
            $date->setTimezone($TZ);
            $date = $date->format('Y-m-d H:i:s');
            $post->createdLocal = $date;
        }
        return $post;
    }

    public function store(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];

        $post = null;
        $existingPost = null;
        $fields = 'link,permalink_url,message,source,type,full_picture,description,from,admin_creator,created_time,updated_time,attachments';
        $client = new Client();
        $fbAccounts = SocialLogin::where('user_id',auth()->user()->id)->get();
        $existingPost = FacebookPost::where('post_id',$request->post_id)->first();
        if($existingPost)
            return  $data = [
                'status' => '0',
                'error' => 'Post already exists'
            ];
        if(count($fbAccounts)==0)
            return  $data = [
                'status' => '0',
                'error' => 'No accounts linked'
            ];

        $fbUser = $fbAccounts[0];
        $url = 'https://graph.facebook.com/'.$request->post_id;
        try {
            $post = $client->request('GET', $url, [
                    'query' => ['access_token' => $fbUser->token,'date_format'=> 'U', 'fields' => $fields]
                ]
            );
        }
        catch(\Exception $ex){
            return  $data = [
                'status' => '0',
                'error' => $ex->getMessage()
            ];
        }
        $post = json_decode($post->getBody()->getContents());
        $postEntity = new FacebookPost();
        $postEntity->post_id = $post->id;
        $postEntity->link = $post->link;
        $message = '';
        if(property_exists($post,'message'))
            $message = $post->message;
        if(property_exists($post,'story'))
            $message = $post->story;
        $postEntity->message = $message;
        $postEntity->raw_message = $message;
        $postEntity->fullpicture = $post->full_picture;
        $postEntity->permalink = $post->permalink_url;
        $postEntity->source = (property_exists($post,'source'))?$post->source:null;
        $postEntity->admin_creator = (property_exists($post,'admin_creator'))?$post->admin_creator:null;
        $postEntity->posted_from_id = $post->from->id;
        $postEntity->social_created_time = $post->created_time;
        $postEntity->social_updated_time = $post->updated_time;
        $postEntity->type = $post->type;
        $postEntity->published_here = false;
        $postEntity->user_id = auth()->user()->id;
        //dd($post->attachments->data);
        $postEntity->save();
        foreach ($post->attachments->data as $item){
            $attachment = new FacebookPostAttachment();
            $attachment->social_id = (isset($item->target->id)) ? $item->target->id : '';
            $attachment->image = (isset($item->media->image->src)) ? $item->media->image->src : '';
            $attachment->type =  $item->type;
            switch($item->type){
                case 'video_inline':
                case 'video_autoplay':
                    $attachment->embed_video_html = $this->getVideoEmbedHtml('facebookVideo',$item->target->url, $client,$fbUser);
                    break;
                case 'video_share_youtube':
                    $attachment->embed_video_html = $this->getVideoEmbedHtml('youtubeVideo',$item->target->url, $client,$fbUser);
                    break;
                default:
                    $attachment->embed_video_html = '';
            }
            $attachment->url = $item->target->url;
            $postEntity->facebookPostAttachments()->save($attachment);
        }

        $data['status'] = '1';
        $data['state'] = 'saved_notpublished';
        return $data;
    }

    private function getVideoEmbedHtml($type, $videoUrl, $client, $fbUser){
        $result = '';
        switch($type){
            case 'youtubeVideo':
                $videoUrl = urldecode(urldecode($videoUrl));
                $start = strpos($videoUrl, '?v=')+3;
                $end = strpos($videoUrl, '&',$start);
                $length = $end - $start;
                $ytId = substr($videoUrl, $start, $length);
                $result  = '<iframe width="640px" height="480px" src="https://www.youtube.com/embed/'.$ytId.'?autohide=1&version=3&autoplay=0"></iframe>';
                break;
            case 'facebookVideo':
                $videoUrl = rtrim($videoUrl,'/');
                $fbId = explode('/',$videoUrl);
                $fbId = end($fbId);
                $apiUrl = 'https://graph.facebook.com/'.$fbId;
                $embed = '';
                $embed = $client->request('GET', $apiUrl, [
                        'query' => ['access_token' => $fbUser->token, 'fields' => 'embed_html']
                    ]
                );
                $embed = json_decode($embed->getBody()->getContents());
                $result = $embed->embed_html;
                break;
        }
        return $result;
    }

    public function getPostAttachments($post_id, $user_token)
    {
        $client = new Client();
        $url = 'https://graph.facebook.com/'.$post_id.'/attachments';
        $attachments = null;
        try {
            $attachments = $client->request('GET', $url, [
                    'query' => ['access_token' => $user_token]
                ]
            );
        }
        catch(\Exception $ex){
            return  null;
        }

        return json_decode($attachments->getBody()->getContents());
    }

    public function publish(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $postId = $request->post_id;
        $post = FacebookPost::where('post_id',$postId)->first();
        $post->published_here = true;
        $post->save();
        $data['status'] = '1';
        $data['state'] = 'saved_published';
        return $data;
    }

    public function unpublish(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $postId = $request->post_id;
        $post = FacebookPost::where('post_id',$postId)->first();
        $post->published_here = false;
        $post->save();
        $data['status'] = '1';
        $data['state'] = 'saved_notpublished';
        return $data;
    }

    public function delete(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];

        $postId = $request->post_id;
        $post = FacebookPost::where('post_id',$postId)->first();
        $post->facebookPostAttachments()->delete();
        $post->delete();
        $data['status'] = '1';
        $data['state'] = 'notsaved';
        return $data;
    }

    protected function checkState($postId){
        $post = FacebookPost::where('post_id',$postId)->first();
        if(!$post)
            return 'notsaved';
        if($post->published_here == true)
            return 'saved_published';
        else
            return 'saved_notpublished';
    }

    protected function setPostState($post){
        $post->state = $this->checkState($post->id);
        return $post;
    }



    protected function addProperties($posts){
            foreach($posts as &$post){
            $post = $this->setPostState($post);
            $post = $this->setPostLocaltime($post);
            if(property_exists($post,'message')) {
                $post->message = formatUrlsInText($post->message);
            }
        }
        return $posts;
    }


    /*   user role methods  */


    public function getPagePostsForUser(Request $request, $pageTitle)
    {
        $user = auth()->user();
        $fbUser = $user->socialLogins()->where('user_id',$user->id)
            ->where('provider','facebook')
            ->first();
        $client = new Client();
        $nextPage = '';
        $url = 'https://graph.facebook.com/'.$pageTitle.'/posts';
        try {
            $posts = $client->request('GET', $url, [
                    'query' => ['access_token' => $fbUser->token,'date_format'=> 'U','limit' => $this->limit, 'fields' => 'link,full_picture,message,story,created_time,attachments,shares,comments.limit(0).summary(true),likes.limit(0).summary(true)']
                ]
            );
        }
        catch(\Exception $ex){
            return view('backend.noresults');
        }
        $posts = json_decode($posts->getBody()->getContents());
        foreach ($posts->data as $key => $value) {
            if(!property_exists($value ,'message')||!property_exists($value,'full_picture') || !property_exists($value,'attachments')) {
                array_splice($posts->data, $key, 1);
            }
            if(isset($value->attachments)) {
                if (count($value->attachments->data) > 0) {
                    $attachment = $value->attachments->data[0];
                    switch ($attachment->type) {
                        case 'video_inline':
                        case 'video_autoplay':
                            $value->embed_video_html = $this->getVideoEmbedHtml('facebookVideo', $attachment->target->url, $client, $fbUser);
                            break;
                        case 'video_share_youtube':
                            $value->embed_video_html = $this->getVideoEmbedHtml('youtubeVideo', $attachment->target->url, $client, $fbUser);
                            break;
                        default:
                            $value->embed_video_html = '';
                    }
                }
            }
            else{
                $value->embed_video_html = '';
            }
        }

        $posts->data = $this->addPropertiesForUser($posts->data);
        if(property_exists($posts,'paging')) {
            if(property_exists($posts->paging,'next'))
                $nextPage = $posts->paging->next;
        }
        return view('backend.facebook-page-posts-foruser',compact(['posts','pageTitle','nextPage']));
    }

    public function getPagePostsAjaxForUser(Request $request)
    {
        $data = [
            'posts' => [],
            'error' => ''
        ];
        $user = auth()->user();
        $fbUser = $user->socialLogins()->where('user_id',$user->id)
            ->where('provider','facebook')
            ->first();
        if($request->nextPage == '') {
            $data['error'] = 'page token is empty';
            $data['status'] = 0;
            echo json_encode($data);
        }
        $client = new Client();
        $posts = $client->request('GET', $request->nextPage);
        $posts = json_decode($posts->getBody()->getContents());
        foreach ($posts->data as $key => $value) {
            if(!property_exists($value ,'message')||!property_exists($value,'full_picture') || !property_exists($value,'attachments')) {
                array_splice($posts->data, $key, 1);
            }
            if(isset($value->attachments)) {
                if (count($value->attachments->data) > 0) {
                    $attachment = $value->attachments->data[0];
                    switch ($attachment->type) {
                        case 'video_inline':
                        case 'video_autoplay':
                            $value->embed_video_html = $this->getVideoEmbedHtml('facebookVideo', $attachment->target->url, $client, $fbUser);
                            break;
                        case 'video_share_youtube':
                            $value->embed_video_html = $this->getVideoEmbedHtml('youtubeVideo', $attachment->target->url, $client, $fbUser);
                            break;
                        default:
                            $value->embed_video_html = '';
                    }
                }
            }
            else{
                $value->embed_video_html = '';
            }
        }
        $posts->data = $this->addPropertiesForUser($posts->data);
        if(property_exists($posts,'paging')) {
            $data['nextPage'] = $posts->paging->next;
        }
        $data['posts'] = $posts->data;
        $data['status'] = 1;
        echo json_encode($data);
    }

    public function addToFavouriteForUser(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];

        $post = null;
        $existingPost = null;
        $fields = 'link,permalink_url,message,source,type,full_picture,description,from,admin_creator,created_time,updated_time,attachments';
        $client = new Client();
        $fbAccounts = SocialLogin::where('user_id',auth()->user()->id)->get();
        $existingPost = FacebookPost::where('post_id',$request->post_id)->where('user_id',auth()->user()->id)->first();
        if(!$existingPost) {
            if (count($fbAccounts) == 0)
                return $data = [
                    'status' => '0',
                    'error' => 'No accounts linked'
                ];

            $fbUser = $fbAccounts[0];
            $url = 'https://graph.facebook.com/' . $request->post_id;
            try {
                $post = $client->request('GET', $url, [
                        'query' => ['access_token' => $fbUser->token, 'date_format' => 'U', 'fields' => $fields]
                    ]
                );
            } catch (\Exception $ex) {
                return $data = [
                    'status' => '0',
                    'error' => $ex->getMessage()
                ];
            }
            $post = json_decode($post->getBody()->getContents());
            $existingPost = new FacebookPost();
            $existingPost->post_id = $post->id;
            $existingPost->link = $post->link;
            $message = '';
            if (property_exists($post, 'message'))
                $message = $post->message;
            if (property_exists($post, 'story'))
                $message = $post->story;
            $existingPost->message = $message;
            $existingPost->raw_message = $message;
            $existingPost->fullpicture = $post->full_picture;
            $existingPost->permalink = $post->permalink_url;
            $existingPost->source = (property_exists($post, 'source')) ? $post->source : null;
            $existingPost->admin_creator = (property_exists($post, 'admin_creator')) ? $post->admin_creator : null;
            $existingPost->posted_from_id = $post->from->id;
            $existingPost->social_created_time = $post->created_time;
            $existingPost->social_updated_time = $post->updated_time;
            $existingPost->type = $post->type;
            $existingPost->published_here = false;
            $existingPost->user_id = auth()->user()->id;
            $existingPost->save();

            foreach ($post->attachments->data as $item){
                $attachment = new FacebookPostAttachment();
                $attachment->social_id = (isset($item->target->id)) ? $item->target->id : '';
                $attachment->image = (isset($item->media->image->src)) ? $item->media->image->src : '';
                $attachment->type =  $item->type;
                switch($item->type){
                    case 'video_inline':
                    case 'video_autoplay':
                        $attachment->embed_video_html = $this->getVideoEmbedHtml('facebookVideo',$item->target->url, $client,$fbUser);
                        break;
                    case 'video_share_youtube':
                        $attachment->embed_video_html = $this->getVideoEmbedHtml('youtubeVideo',$item->target->url, $client,$fbUser);
                        break;
                    default:
                        $attachment->embed_video_html = '';
                }
                $attachment->url = $item->target->url;
                $existingPost->facebookPostAttachments()->save($attachment);
            }
        }
        $user = auth()->user();

        DB::table('favourite_posts')->insert(
            ['user_id' => auth()->user()->id, 'favouritepost_id' => $existingPost->id, 'favouritepost_type' => 'App\Models\FacebookPost']);

        $data['status'] = '1';
        $data['state'] = 'saved_favourite';
        return $data;
    }

    public function deleteFromFavouriteForUser(Request $request)
    {
        $user = auth()->user();
        $data = [
            'status' => '',
            'error' => ''
        ];
        $postId = $request->post_id;
        $post = FacebookPost::where('post_id',$postId)->first();
        $post->facebookPostAttachments()->delete();
        DB::table('favourite_posts')->where('user_id',$user->id)
            ->where('favouritepost_id',$post->id)
            ->where('favouritepost_type','App\Models\FacebookPost')
            ->delete();
        $post->delete();
        $data['status'] = '1';
        $data['state'] = 'notsaved_notfavourite';
        return $data;
    }


    protected function checkStateForUser($postId){
        $user = auth()->user();
        $post = FacebookPost::where('post_id',$postId)->first();
        $favPost = null;
        if($post) {
            $favPost = DB::table('favourite_posts')->where('user_id', $user->id)
                ->where('favouritepost_id', $post->id)
                ->where('favouritepost_type', 'App\Models\FacebookPost')
                ->first();
        }
        if(!$favPost)
            return 'notsaved_notfavourite';
        else
            return 'saved_favourite';
    }

    protected function setPostStateForUser($post){
        $post->state = $this->checkStateForUser($post->id);
        return $post;
    }



    protected function addPropertiesForUser($posts){
        foreach($posts as $post){
            $post = $this->setPostStateForUser($post);
            $post = $this->setPostLocaltime($post);
            if(property_exists($post,'message')) {
                $post->message = formatUrlsInText($post->message);
            }
        }
        return $posts;
    }

    /* favourite posts methods */


    public function addToUserStream(Request $request){
        $data = ['status' =>'',
                    'msg' =>''];
        $user = auth()->user();
        $postId = $request->input('postId');
        $post = $user->streamFbPosts()->where('streampost_id',$postId)->first();
        if(!$post){
            $streamPost = FacebookPost::where('id',$postId)->first();
            $user->streamFbPosts()->attach($streamPost);
            $data['status'] = 'success';
            $data['msg'] = 'success';
        }
        else{
            $data['status'] = 'failed';
            $data['msg'] = 'post already exists';
        }
        return $data;
    }

    public function deleteFromUserStream(Request $request){
        $data = ['status' =>'',
            'msg' =>''];
        $user = auth()->user();
        $postId = $request->input('postId');
        $post = $user->streamFbPosts()->where('streampost_id',$postId)->first();
        if($post){
            $streamPost = FacebookPost::where('id',$postId)->first();
            $user->streamFbPosts()->detach($streamPost);
            $data['status'] = 'success';
            $data['msg'] = 'success';
        }
        else{
            $data['status'] = 'failed';
            $data['msg'] = 'post doesn`t exist';
        }
        return $data;
    }

}
