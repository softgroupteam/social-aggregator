<?php

namespace App\Http\Controllers;

use App\Models\FacebookPost;
use App\Models\PublicFacebookPage;
use App\Models\SocialFacebookPage;
use Illuminate\Http\Request;
use App\SocialLogin;
use GuzzleHttp\Client;
use Thujohn\Twitter\Facades\Twitter;
use PulkitJalan\Google\Facades\Google;
use App\Models\ScheduledPost;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;


class ComposerController extends Controller
{
    public function savePost(Request $request)
    {
        $data = [];
        if($request->input('social_schedule_check') == 'now')
            $data = $this->postNow($request);
        else
            $data = $this->postScheduled($request);
        return $data;
    }

    protected function postNow($request)
    {
        $validationData = $this->validateRequest($request);
        if($validationData['status'] == 'failed')
            return $validationData;
        $fbAccounts = ($request->has('fbaccounts'))?array_values($request->input('fbaccounts')):[];
        $twAccounts = ($request->has('twaccounts'))?array_values($request->input('twaccounts')):[];
        $ggAccounts = ($request->has('ggaccounts'))?array_values($request->input('ggaccounts')):[];
        $fbSocialPages = ($request->has('fbsocialpages'))?array_values($request->input('fbsocialpages')):[];
        $photoUrl = ($request->hasFile('composer-post-image'))?$request->file('composer-post-image'):'';
        $msg = ($request->has('message-text'))?$request->input('message-text'):'';
        $remoteUrl = $request->input('remote-image-url');
        $originalLink = ($request->has('original-link'))? $request->input('original-link'):'';
        $videoLink = ($request->has('remote-video-link'))? $request->input('remote-video-link'):'';
        $userAttachedLink = $request->input('user-attached-link');
        foreach ($fbAccounts as $acc){
            $this->saveFacebookPost($acc, $photoUrl, $msg , $remoteUrl, $videoLink, $userAttachedLink);
        }
        foreach ($twAccounts as $acc){
            $this->saveTwitterPost($acc, $photoUrl, $msg, $remoteUrl, $videoLink, $userAttachedLink);
        }
        foreach ($ggAccounts as $acc){
            $this->saveGooglePost($acc, $photoUrl, $msg);
        }
        foreach ($fbSocialPages as $page){
            $this->saveFacebookPostToPage($page, $photoUrl, $msg, $remoteUrl,$videoLink, $userAttachedLink);
        }
        if($request->has('include-promote-pages')){
            $this->postToPromotePages($photoUrl, $msg, $remoteUrl,$videoLink, $userAttachedLink);
        }

        return $validationData;
    }

    protected function postToPromotePages($localPhoto, $msg, $remotePhotoUrl = '',$videoLink, $userAttachedLink)
    {
        $fbUser = auth()->user()->socialLogins()->where('user_id',auth()->user()->id)
            ->where('provider','facebook')
            ->first();
        $client = new Client();
        $publicPages = PublicFacebookPage::where('user_id',auth()->user()->id)->get();
        foreach($publicPages as $page) {
            $result = '';
            $postQuery = [];
            $url = 'https://graph.facebook.com/' . $fbUser->provider_id . '/photos';
            if ($videoLink !== '') {
                $postQuery = ['access_token' => $fbUser->token, 'message' => $msg . ' ' . $userAttachedLink, 'link' => $videoLink];
            } elseif($localPhoto != '') {
                $photoId = $this->uploadLocalImageToFacebook($localPhoto, $client, $url, $fbUser);
                $postQuery = ['access_token' => $fbUser->token, 'message' => $msg . ' ' . $userAttachedLink, 'attached_media[0]' => '{"media_fbid":"' . $photoId . '"}'];
            } elseif ($remotePhotoUrl != ''){
                $photoId = $this->uploadRemoteImageToFacebook($remotePhotoUrl, $client, $url, $fbUser);
                $postQuery = ['access_token' => $fbUser->token, 'message' => $msg . ' ' . $userAttachedLink, 'attached_media[0]' => '{"media_fbid":"' . $photoId . '"}'];
            } else {
                $postQuery = ['access_token' => $fbUser->token, 'message' => $msg, 'link' => $userAttachedLink];
            }
            $url = 'https://graph.facebook.com/' . $page->social_id . '/feed';
            try {
                $result = $client->request('POST', $url, [
                        'query' => $postQuery
                    ]
                );
            } catch (\Exception $ex) {
                throw $ex;
            }
        }
    }


    protected function postScheduled($request)
    {
        $validationData = $this->validateRequest($request,true);
        if($validationData['status'] == 'failed')
            return $validationData;
        $fbAccounts = ($request->has('fbaccounts'))?array_values($request->input('fbaccounts')):[];
        $twAccounts = ($request->has('twaccounts'))?array_values($request->input('twaccounts')):[];
        $fbSocialPages = ($request->has('fbsocialpages'))?array_values($request->input('fbsocialpages')):[];
        $localPhoto = ($request->hasFile('composer-post-image'))?$request->file('composer-post-image'):'';
        $msg = ($request->has('message-text'))?$request->input('message-text'):'';
        $remotePhotoUrl = $request->input('remote-image-url');
        $postingTime = $this->convertPostdateToTimestamp($request->input('scheduled-post-date'),$request->input('scheduled-post-hours'),$request->input('scheduled-post-minutes'));
        $videoLink = ($request->has('remote-video-link'))? $request->input('remote-video-link'):'';
        $videoHtml = ($request->has('video_html'))? $request->input('video_html'):'';
        $userAttachedLink = $request->input('user-attached-link');
        $includePromotePages = ($request->has('include-promote-pages'))? true : false;
        $this->saveScheduledPost($fbAccounts, $fbSocialPages,$userAttachedLink, $twAccounts, $localPhoto, $msg , $remotePhotoUrl,$postingTime, $videoLink, $videoHtml, $includePromotePages);
        return $validationData;
    }

    protected function convertPostdateToTimestamp($date, $hours = 'HH', $minutes = 'MM')
    {
        $date.=' 00:00:00';
        $timezone = 'GMT+0';
        if(array_key_exists('user_time_zone',$_COOKIE)){
            $timezone = $_COOKIE['user_time_zone'];
        }
        $time = Carbon::createFromFormat('m/d/yy H:i:s', $date,$timezone);
        $time->setTimezone('GMT+0');
        if($hours != 'HH')
            $time->addHours(intval($hours));
        if($minutes != 'MM')
            $time->addMinutes(intval($minutes));
        return $time->timestamp;
    }

    protected function saveScheduledPost($fbAccounts, $fbSocialPages, $userAttachedLink, $twAccounts, $localPhoto, $msg , $remotePhotoUrl, $postingTime, $videoLink, $videoHtml, $includePromotePages)
    {
        $post = new ScheduledPost();
        $imagePath = '';
        $post->video_path = '';
        $post->embed_video_html = '';
        $post->embed_video_picture = '';
        if($videoLink != ''){
            $post->video_path = $videoLink;
            $post->embed_video_html = $videoHtml;
            $post->embed_video_picture = $remotePhotoUrl;
        }
        elseif($localPhoto != '') {
            $imagePath = $this->saveLocalScheduledImage($localPhoto);
        }
        elseif($remotePhotoUrl!=''){
            $imagePath = $this->saveRemoteScheduledImage($remotePhotoUrl);
        }
        $post->user_attached_link = ($userAttachedLink != null)?$userAttachedLink:'';
        $post->image_path = $imagePath;
        $post->message = $msg;
        $post->posting_time = $postingTime;
        $post->user_id = auth()->user()->id;
        $post->save();
        foreach($fbAccounts as $acc){
            DB::table('scheduled_posts_destination')->insert(
                ['scheduled_post_id' => $post->id, 'destination_id' => $acc, 'destination_type' => SocialLogin::class, 'created_at' => new \DateTime(),'updated_at' => new \DateTime()]
            );
        }
        foreach($fbSocialPages as $page){
            DB::table('scheduled_posts_destination')->insert(
                ['scheduled_post_id' => $post->id, 'destination_id' => $page, 'destination_type' => SocialFacebookPage::class, 'created_at' => new \DateTime(),'updated_at' => new \DateTime()]
            );
        }
        foreach($twAccounts as $page){
            DB::table('scheduled_posts_destination')->insert(
                ['scheduled_post_id' => $post->id, 'destination_id' => $page, 'destination_type' => SocialLogin::class, 'created_at' => new \DateTime(),'updated_at' => new \DateTime()]
            );
        }
        if($includePromotePages){
            $pages = auth()->user()->publicFbPages()->get();
            foreach ($pages as $page){
                DB::table('scheduled_posts_destination')->insert(
                    ['scheduled_post_id' => $post->id, 'destination_id' => $page->id, 'destination_type' => PublicFacebookPage::class, 'created_at' => new \DateTime(),'updated_at' => new \DateTime()]
                );
            }
        }
    }

    protected function validateRequest($request, $scheduled = false)
    {
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => ''
        ];
        if(strlen($request->input('message-text')) == 0)
            $data['errors'][] = 'Message is empty';
        if($request->has('twaccounts') && strlen($request->input('message-text'))>140)
            $data['errors'][] = 'You can`t post over 140 symbols due to twitter restrictions';
        if(!$request->has('fbaccounts') && !$request->has('twaccounts') && !$request->has('ggaccounts') && !$request->has('fbsocialpages') &&!$request->has('include-promote-pages'))
            $data['errors'][] = 'Choose at least one account';

        if($scheduled == true) {
            if ($request->input('scheduled-post-date') == '')
                $data['errors'][] = 'Choose schedule date';
        }

        if(count($data['errors'])>0)
            $data['status'] = 'failed';
        else
            $data['status'] = 'success';
        if($scheduled == true){
            $data['success_msg'] = 'Post has been saved to schedule';
        }
        else{
            $data['success_msg'] = 'Post has been published';
        }
        return $data;
    }

    protected function saveFacebookPost($accId , $localPhoto, $msg, $remotePhotoUrl = '',$videoLink, $userAttachedLink)
    {
        $fbUser = SocialLogin::find($accId);
        $client = new Client();
        $url = 'https://graph.facebook.com/me/photos';
        $photoId = '';
        $result = '';
        $postQuery = [];
        if($videoLink !== ''){
            $postQuery = ['access_token' => $fbUser->token,'message'=> $msg.' '.$userAttachedLink, 'link' => $videoLink];
        }
        elseif($localPhoto != '') {
            $photoId = $this->uploadLocalImageToFacebook($localPhoto, $client, $url, $fbUser);
            $postQuery = ['access_token' => $fbUser->token,'message'=> $msg.' '.$userAttachedLink, 'attached_media[0]' => '{"media_fbid":"'.$photoId.'"}'];
        }
        elseif($remotePhotoUrl!=''){
            $photoId = $this->uploadRemoteImageToFacebook($remotePhotoUrl, $client, $url, $fbUser);
            $postQuery = ['access_token' => $fbUser->token,'message'=> $msg.' '.$userAttachedLink, 'attached_media[0]' => '{"media_fbid":"'.$photoId.'"}'];
        }
        else{
            $postQuery = ['access_token' => $fbUser->token,'message'=> $msg, 'link' => $userAttachedLink];
        }
        $url = 'https://graph.facebook.com/me/feed';

        try {
            $result = $client->request('POST', $url, [
                    'query' => $postQuery
                ]
            );
        }
        catch(\Exception $ex){
            return $ex->getMessage().' second catch';
        }
    }

    protected function saveFacebookPostToPage($pageId , $localPhoto, $msg, $remotePhotoUrl = '',$videoLink, $userAttachedLink)
    {
        $fbSocialPage = SocialFacebookPage::find($pageId);
        $client = new Client();
        $url = 'https://graph.facebook.com/'.$fbSocialPage->fb_page_id.'/photos';
        $photoId = '';
        $result = '';
        $postQuery = [];
        if($videoLink!== ''){
            $postQuery = ['access_token' => $fbSocialPage->token,'message'=> $msg.' '.$userAttachedLink, 'link' => $videoLink];
        }
        elseif($localPhoto != '') {
            $photoId = $this->uploadLocalImageToFacebook($localPhoto, $client, $url, $fbSocialPage);
            $postQuery = ['access_token' => $fbSocialPage->token,'message'=> $msg.' '.$userAttachedLink, 'attached_media[0]' => '{"media_fbid":"'.$photoId.'"}'];
        }
        elseif($remotePhotoUrl!=''){
            $photoId = $this->uploadRemoteImageToFacebook($remotePhotoUrl, $client, $url, $fbSocialPage);
            $postQuery = ['access_token' => $fbSocialPage->token,'message'=> $msg.' '.$userAttachedLink, 'attached_media[0]' => '{"media_fbid":"'.$photoId.'"}'];
        }
        else{
            $postQuery = ['access_token' => $fbSocialPage->token,'message'=> $msg, 'link' => $userAttachedLink];
        }
        $url = 'https://graph.facebook.com/'.$fbSocialPage->fb_page_id.'/feed';

        try {
            $result = $client->request('POST', $url, [
                    'query' => $postQuery
                ]
            );
        }
        catch(\Exception $ex){
            return $ex->getMessage().' second catch';
        }
    }

    protected function saveTwitterPost($accId , $localPhoto, $msg, $remotePhotoUrl = '', $videoLink, $userAttachedLink)
    {
        $twUser = SocialLogin::find($accId);
        $postParams = '';
        if($videoLink != ''){
            $postParams = ['status' => $msg.' '.$videoLink, 'format' => 'json'];
        }
        else if($localPhoto || $remotePhotoUrl != '') {
            $uploaded_media_id = $this->uploadImageToTwitter($localPhoto, $remotePhotoUrl);
            $postParams = ['status' => $msg.' '.$userAttachedLink, 'format' => 'json', 'media_ids' => $uploaded_media_id];
        }
        else {
            $postParams = ['status' => $msg.' '.$userAttachedLink, 'format' => 'json'];
        }
        Twitter::reconfig(['token' => $twUser->token, 'secret' => $twUser->token_secret]);
        Twitter::postTweet($postParams);
    }

    protected function saveGooglePost($accId , $photoUrl, $msg)
    {
        $googleScopes = ['https://www.googleapis.com/auth/plus.me',
            'https://www.googleapis.com/auth/plus.login',
            'https://www.googleapis.com/auth/plus.profile.emails.read',
            'https://www.googleapis.com/auth/plus.stream.write'];
        $ggUser = SocialLogin::find($accId);
        $client = Google::getClient();
        $client->setScopes($googleScopes);
        $client->useApplicationDefaultCredentials();
        Google::setClient($client);
        $service = Google::make('PlusDomains');
        $activity = new \Google_Service_PlusDomains_Activity();
        $activity->setTitle('test');
        $service->activities->insert('me',$activity);
    }


    protected function uploadLocalImageToFacebook($photo, $client, $url, $fbUser)
    {

        $image_path = $photo->getPathname();
        $image_mime = $photo->getmimeType();
        $image_org  = $photo->getClientOriginalName();
        try {
            $result = $client->post($url, [
                'multipart' => [
                    [
                        'name'     => 'image',
                        'filename' => $image_org,
                        'Mime-Type'=> $image_mime,
                        'contents' => fopen( $image_path, 'r' ),
                    ],
                ],
                'query' => ['access_token' => $fbUser->token, 'published' => false]
            ]);
        } catch (\Exception $ex) {
            return $ex->getMessage().' first catch';
        }
        $photoId = json_decode($result->getBody()->getContents())->id;
        return $photoId;
    }

    protected function uploadRemoteImageToFacebook($remoteUrl, $client, $url, $fbUser)
    {
        try{
            $result = $client->request('POST', $url, [
                    'query' => ['access_token' => $fbUser->token,'published'=> 'false', 'url' => $remoteUrl]
                ]
            );
        }
        catch (\Exception $ex) {
            return $ex->getMessage().' first catch';
        }
        $photoId = json_decode($result->getBody()->getContents())->id;
        return $photoId;
    }

    protected function uploadImageToTwitter($localPhoto, $remotePhotoUrl)
    {
        $contents = '';
        $uploaded_media_id = '';
        if($remotePhotoUrl != '')
            $contents = file_get_contents($remotePhotoUrl);
        if($localPhoto!='')
            $contents = $localPhoto->openFile()->fread($localPhoto->getSize());
        if($contents!='') {
            $uploaded_media_id = Twitter::uploadMedia(['media' => $contents])->media_id_string;
        }
        return $uploaded_media_id;
    }

    protected function saveRemoteScheduledImage($remotePhotoUrl)
    {
        $client = new Client();
        $mimeTypes = array(
            'image/png' => 'png',
            'image/jpeg'=> 'jpeg',
            'image/jpg'=> 'jpg',
            'image/gif'=> 'gif'
            );
        $image = $client->request('GET', $remotePhotoUrl);
        $contentType = strtolower($image->getHeader('Content-Type')[0]);
        $fileExt= $mimeTypes[$contentType];
        $newName = uniqid().uniqid().'.'.$fileExt;
        $destinationPath = storage_path('app/public/scheduled_posts_images/'.$newName);
        file_put_contents($destinationPath, $image->getBody());
        $imagePathforSave = 'public/scheduled_posts_images/'.$newName;
        return $imagePathforSave;
    }

    protected function savelocalScheduledImage($localPhoto)
    {
        $path = $localPhoto->store('public/scheduled_posts_images');
        return  $path;
    }
}