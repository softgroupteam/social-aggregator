<?php

namespace App\Http\Controllers\Googleplus;

use App\Models\GooglePostAttachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use App\Models\GooglePost;
use Illuminate\Support\Facades\DB;


class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getGooglePosts()
    {
        $user = auth()->user();
        $googleGroup = Input::get('channelId');
        if($user->hasRole('user'))
            return $this->getGooglePostsForUser($googleGroup);
        $data = [
            'posts' => [],
            'error' => '',
            'group' => $googleGroup
        ];
        if($user) {

            if ($googleGroup) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/plus/v1/people/$googleGroup/activities/public?key=".config('services.google.api_key'));
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($ch, CURLOPT_USERAGENT, 'PHP Bot');
                $response = json_decode(curl_exec($ch));
                if (!isset($response->error)) {
                    $response->items = $this->addProperties($response->items);
                    $data['posts'] = $response->items;
                    $data['pageToken'] = $response->nextPageToken;
                } else {
                    $data['error'] = $response->error->message;
                    return view('backend.noresults');
                }
                curl_close($ch);
            }
        }
        //dd($data);
        return view('backend.google-plus', $data);
    }


    public function getGooglePostsAjax() {
        $data = [
            'posts' => [],
            'error' => ''
        ];
        if($user = auth()->user()) {
            $googleGroup = Input::get('google_group');
            $pageToken = Input::get('page_token');

            if ($googleGroup) {
                $ch = curl_init();
                $pageToken = ($pageToken) ? "&pageToken=$pageToken" : '';

                curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/plus/v1/people/$googleGroup/activities/public?key=".config('services.google.api_key').$pageToken);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
                $response = json_decode(curl_exec($ch));
                $response->items = $this->addProperties($response->items);
                $info = curl_getinfo($ch);


                $posts = [];
                foreach($response->items as $item) {
                    $post = [];
                    if (isset($item->object->attachments[0]->image->url)) {
                        $post['image'] = $item->object->attachments[0]->image->url;
                    }
                    $post['id'] = $item->id;
                    $post['title'] = $item->title;
                    $post['content'] = $item->object->content;
                    $post['published'] = $item->published;
                    $post['createdLocal'] = $item->createdLocal;
                    $post['state'] = $item->state;
                    $post['resharers'] = $item->object->resharers->totalItems;
                    $post['comments'] = $item->object->replies->totalItems;
                    $post['likes'] = $item->object->plusoners->totalItems;
                    array_push($posts, $post);
                }
                if ($response && !isset($response->error)) {
                    $data = [
                        'status' => 1,
                        'posts' => $posts,
                        'group' => $googleGroup,
                        'page_token' => $response->nextPageToken,
                        'token' => Input::get('_token')
                    ];
                } else {
                    $data = [
                        'status' => 0,
                        'error' => $response->error->message
                    ];
                }
                curl_close($ch);
                echo json_encode($data);
            }
        }
    }


    public function setPostLocaltime($post)
    {
        $timezone = auth()->user()->userInfo()->first()->timezone;
        if($timezone != ''){
                $TZ = new \DateTimeZone($timezone);
                $date  = new \DateTime($post->published);
                $date->setTimezone($TZ);
                $date = $date->format('Y-m-d H:i:s');
                $post->createdLocal = $date;
        }
        return $post;
    }


    public function store(Request $request){
        $data = [
            'status' => '',
            'error' => ''
        ];
        $id = $request->post_id;
        $client = new Client();
        $url = 'https://www.googleapis.com/plus/v1/activities/'.$id;
        $post = $client->request('GET', $url,[
            'query' => ['key' => config('services.google.api_key')]
        ]);
        $post = json_decode($post->getBody()->getContents());

        $postEntity = new GooglePost();
        $postEntity->post_id = $post->id;
        $postEntity->title = $post->title;
        $postEntity->content = $post->object->content;
        $postEntity->raw_message = $post->object->content;
        $postEntity->actor_id = $post->actor->id;
        $postEntity->url = $post->object->url;
        $postEntity->verb = $post->verb;
        $postEntity->type = $post->object->objectType;
        $postEntity->social_created_at = strtotime($post->published);
        $postEntity->published_here  = false;
        $postEntity->user_id  = auth()->user()->id;

        $postEntity->save();

        if(property_exists($post->object,'attachments')){
            foreach ($post->object->attachments as $attachment){
                $entity = new GooglePostAttachment();
                $entity->type = $attachment->objectType;
                $entity->display_name = $attachment->displayName;
                $entity->content = $attachment->content;
                $entity->url = $attachment->url;
                $entity->image_url = (property_exists($attachment,'image'))?$attachment->image->url:null;
                $entity->video_embed = (property_exists($attachment,'embed'))?$attachment->embed->url:null;
                $entity->video_embed_type = (property_exists($attachment,'embed'))?$attachment->embed->type:null;

                $postEntity->googlePostAttachments()->save($entity);
            }
        }


        $data['status'] = '1';
        $data['state'] = 'saved_notpublished';
        return $data;
    }

    public function publish(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $postId = $request->post_id;
        $post = GooglePost::where('post_id',$postId)->first();
        $post->published_here = true;
        $post->save();
        $data['status'] = '1';
        $data['state'] = 'saved_published';
        return $data;
    }

    public function unpublish(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $postId = $request->post_id;
        $post = GooglePost::where('post_id',$postId)->first();
        $post->published_here = false;
        $post->save();
        $data['status'] = '1';
        $data['state'] = 'saved_notpublished';
        return $data;
    }

    public function delete(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];

        $postId = $request->post_id;
        $post = GooglePost::where('post_id',$postId)->first();
        $post->googlePostAttachments()->delete();
        $post->delete();
        $data['status'] = '1';
        $data['state'] = 'notsaved';
        return $data;
    }

    protected function checkState($postId){
        $post = GooglePost::where('post_id',$postId)->first();
        if(!$post)
            return 'notsaved';
        if($post->published_here == true)
            return 'saved_published';
        else
            return 'saved_notpublished';
    }

    protected function setPostState($post){
        $post->state = $this->checkState($post->id);
        return $post;
    }


    protected function addProperties($posts){
        foreach($posts as $post){
            $post = $this->setPostState($post);
            $post = $this->setPostLocaltime($post);
        }
        return $posts;
    }

    /* user role methods */

    public function getGooglePostsForUser($googleGroup)
    {
        $data = [
            'posts' => [],
            'error' => '',
            'group' => $googleGroup
        ];
        if($user = auth()->user()) {

            if ($googleGroup) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/plus/v1/people/$googleGroup/activities/public?key=".config('services.google.api_key'));
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($ch, CURLOPT_USERAGENT, 'PHP Bot');
                $response = json_decode(curl_exec($ch));
                if (!isset($response->error)) {
                    $response->items = $this->addPropertiesForUser($response->items);
                    $data['posts'] = $response->items;
                    $data['pageToken'] = (property_exists($response,'nextPageToken'))?$response->nextPageToken:'';
                } else {
                    $data['error'] = $response->error->message;
                    return view('backend.noresults');
                }
                curl_close($ch);
            }
        }
        return view('backend.google-plus-foruser', $data);
    }


    public function getGooglePostsAjaxForUser()
    {
        $data = [
            'posts' => [],
            'error' => ''
        ];
        if($user = auth()->user()) {
            $googleGroup = Input::get('google_group');
            $pageToken = Input::get('page_token');

            if ($googleGroup) {
                $ch = curl_init();
                $pageToken = ($pageToken) ? "&pageToken=$pageToken" : '';

                curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/plus/v1/people/$googleGroup/activities/public?key=".config('services.google.api_key').$pageToken);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
                $response = json_decode(curl_exec($ch));
                $response->items = $this->addPropertiesForUser($response->items);
                $info = curl_getinfo($ch);


                $posts = [];
                foreach($response->items as $item) {
                    $post = [];
                    if (isset($item->object->attachments[0]->image->url)) {
                        $post['image'] = $item->object->attachments[0]->image->url;
                    }
                    $post['id'] = $item->id;
                    $post['title'] = $item->title;
                    $post['content'] = $item->object->content;
                    $post['published'] = $item->published;
                    $post['createdLocal'] = $item->createdLocal;
                    $post['state'] = $item->state;
                    $post['resharers'] = $item->object->resharers->totalItems;
                    $post['comments'] = $item->object->replies->totalItems;
                    $post['likes'] = $item->object->plusoners->totalItems;
                    array_push($posts, $post);
                }
                if ($response && !isset($response->error)) {
                    $data = [
                        'status' => 1,
                        'posts' => $posts,
                        'group' => $googleGroup,
                        'page_token' => $response->nextPageToken,
                        'token' => Input::get('_token')
                    ];
                } else {
                    $data = [
                        'status' => 0,
                        'error' => $response->error->message
                    ];
                }
                curl_close($ch);
                echo json_encode($data);
            }
        }
    }

    public function addToFavouriteForUser(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $id = $request->post_id;
        $client = new Client();
        $url = 'https://www.googleapis.com/plus/v1/activities/'.$id;
        $post = $client->request('GET', $url,[
            'query' => ['key' => config('services.google.api_key')]
        ]);
        $post = json_decode($post->getBody()->getContents());

        $postEntity = new GooglePost();
        $postEntity->post_id = $post->id;
        $postEntity->title = $post->title;
        $postEntity->content = $post->object->content;
        $postEntity->raw_message = $post->object->content;
        $postEntity->actor_id = $post->actor->id;
        $postEntity->url = $post->object->url;
        $postEntity->verb = $post->verb;
        $postEntity->type = $post->object->objectType;
        $postEntity->social_created_at = strtotime($post->published);
        $postEntity->published_here  = false;
        $postEntity->user_id  = auth()->user()->id;
        $postEntity->save();

        if(property_exists($post->object,'attachments')){
            foreach ($post->object->attachments as $attachment){
                $entity = new GooglePostAttachment();
                $entity->type = $attachment->objectType;
                $entity->display_name = $attachment->displayName;
                $entity->content = $attachment->content;
                $entity->url = $attachment->url;
                $entity->image_url = (property_exists($attachment,'image'))?$attachment->image->url:null;
                $entity->video_embed = (property_exists($attachment,'embed'))?$attachment->embed->url:null;
                $entity->video_embed_type = (property_exists($attachment,'embed'))?$attachment->embed->type:null;

                $postEntity->googlePostAttachments()->save($entity);
            }
        }

        $user = auth()->user();

        DB::table('favourite_posts')->insert(
            ['user_id' => auth()->user()->id, 'favouritepost_id' => $postEntity->id, 'favouritepost_type' => 'App\Models\GooglePost']);

        $data['status'] = '1';
        $data['state'] = 'saved_favourite';
        return $data;
    }

    public function deleteFromFavouriteForUser(Request $request)
    {
        $user = auth()->user();

        $data = [
            'status' => '',
            'error' => ''
        ];

        $postId = $request->post_id;
        $post = GooglePost::where('post_id',$postId)->first();
        $post->googlePostAttachments()->delete();
        DB::table('favourite_posts')->where('user_id',$user->id)
            ->where('favouritepost_id',$post->id)
            ->where('favouritepost_type','App\Models\GooglePost')
            ->delete();
        $post->delete();
        $data['status'] = '1';
        $data['state'] = 'notsaved_notfavourite';
        return $data;
    }

    protected function checkStateForUser($postId)
    {
        $user = auth()->user();
        $post = GooglePost::where('post_id',$postId)->first();
        $favPost = null;
        if($post) {
            $favPost = DB::table('favourite_posts')->where('user_id', $user->id)
                ->where('favouritepost_id', $post->id)
                ->where('favouritepost_type', 'App\Models\GooglePost')
                ->first();
        }
        if(!$favPost)
            return 'notsaved_notfavourite';
        else
            return 'saved_favourite';
    }

    protected function setPostStateForUser($post)
    {
        $post->state = $this->checkStateForUser($post->id);
        return $post;
    }


    protected function addPropertiesForUser($posts)
    {
        foreach($posts as $post){
            $post = $this->setPostStateForUser($post);
            $post = $this->setPostLocaltime($post);
        }
        return $posts;
    }

    /* favourite posts methods */
    public function addToUserStream(Request $request){
        $data = ['status' =>'',
            'msg' =>''];
        $user = auth()->user();
        $postId = $request->input('postId');
        $post = $user->streamGgPosts()->where('streampost_id',$postId)->first();
        if(!$post){
            $streamPost = GooglePost::where('id',$postId)->first();
            $user->streamGgPosts()->attach($streamPost);
            $data['status'] = 'success';
            $data['msg'] = 'success';
        }
        else{
            $data['status'] = 'failed';
            $data['msg'] = 'post already exists';
        }
        return $data;
    }

    public function deleteFromUserStream(Request $request){
        $data = ['status' =>'',
            'msg' =>''];
        $user = auth()->user();
        $postId = $request->input('postId');
        $post = $user->streamGgPosts()->where('streampost_id',$postId)->first();
        if($post){
            $streamPost = GooglePost::where('id',$postId)->first();
            $user->streamGgPosts()->detach($streamPost);
            $data['status'] = 'success';
            $data['msg'] = 'success';
        }
        else{
            $data['status'] = 'failed';
            $data['msg'] = 'post doesn`t exist';
        }
        return $data;
    }

}
