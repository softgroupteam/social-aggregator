<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SocialLogin;
use App\Models\SocialFacebookPage;

class SocialLoginsController extends Controller
{
    public function getFacebookSocialLogins()
    {
        $user = auth()->user();
        $facebookLogins = SocialLogin::where('user_id',$user->id)
                                        ->where('provider', 'facebook')
                                        ->get();
        $facebookPages = SocialFacebookPage::where('user_id',$user->id)
                                             ->get();
        $result = [];
        foreach($facebookLogins as $login){
            $item['id'] = $login->id;
            $item['name'] = $login->name;
            $item['type'] = 'account';
            $item['deleteAction'] = route('modal.delete-socialacc');
            $result[] = $item;
        }
        foreach($facebookPages as $page){
            $item['id'] = $page->id;
            $item['name'] = $page->name;
            $item['type'] = 'page';
            $item['deleteAction'] = route('modal.delete-socialfbpage');
            $result[] = $item;
        }

        return ['data' => $result];
    }

    public function getTwitterSocialLogins()
    {
        $user = auth()->user();
        $twitterLogins = SocialLogin::where('user_id',$user->id)
            ->where('provider', 'twitter')
            ->get();
        $result = [];
        foreach($twitterLogins as $login){
            $item['id'] = $login->id;
            $item['name'] = $login->name;
            $item['type'] = 'account';
            $item['deleteAction'] = route('modal.delete-socialacc');
            $result[] = $item;
        }
        return ['data' => $result];
    }

    public function getGoogleSocialLogins()
    {
        $user = auth()->user();
        $googleLogins = SocialLogin::where('user_id',$user->id)
            ->where('provider', 'google')
            ->get();
        $result = [];
        foreach($googleLogins as $login){
            $item['id'] = $login->id;
            $item['name'] = $login->name;
            $item['type'] = 'account';
            $item['deleteAction'] = route('modal.delete-socialacc');
            $result[] = $item;
        }
        return ['data' => $result];
    }
}
