<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FacebookPage;
use App\YoutubeChannel;
use App\GooglePlusChannel;
use App\TwitterPage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Thujohn\Twitter\Facades\Twitter;
use Alaouy\Youtube\Facades\Youtube;

class SocialStreamsController extends Controller
{
    public function getFacebookStreams()
    {
        $data = [];
        $fbPages = FacebookPage::getPages();
        $data['data'] = $fbPages;
        return $data;
    }

    public function getTwitterStreams()
    {
        $data = [];
        $twPages = TwitterPage::getPages();
        $data['data'] = $twPages;
        return $data;
    }

    public function getYoutubeStreams()
    {
        $data = [];
        $ytChannels = YoutubeChannel::getChannels();
        $data['data'] = $ytChannels;
        return $data;
    }

    public function getGoogleStreams()
    {
        $data = [];
        $gpChannels = GooglePlusChannel::getChannels();
        $data['data'] = $gpChannels;
        return $data;
    }

    public function addFacebookStream(Request $request)
    {
        $url = $request->input('fb-stream-url');
        $title = $request->input('fb-stream-title');
        $category = ($request->has('category'))?$request->input('category'):'';
        $data = $this->validateFacebookUrl($url);
        if($title == '') {
            $data['status'] = 'failed';
            $data['errors'][] = 'title is empty';
        }
        if($data['status'] == 'failed')
            return $data;
        $fbPage = new FacebookPage();
        $fbPage->user_id  = auth()->user()->id;
        $fbPage->machine_name = $data['indentifier'];
        $fbPage->title = $title;
        $fbPage->category_id = ($category != '' )?$category:null;
        $fbPage->save();
        return $data;
    }

    protected function validateFacebookUrl($url)
    {
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => ''
        ];
        $url = rtrim($url, '/');
        $url = strtok($url, '?');
        $user = auth()->user();
        $fbUser = $user->socialLogins()
            ->where('user_id',$user->id)
            ->where('provider','facebook')
            ->first();
        $parts = explode('/',$url);
        $client = new Client();
        if(count($parts)>0){
            $indentifier = $parts[count($parts)-1];
            try{
                $url = 'https://graph.facebook.com/'.$indentifier.'/posts';
                $posts = $client->request('GET', $url, [
                        'query' => ['access_token' => $fbUser->token,'date_format'=> 'U', 'fields' => 'created_time']
                    ]
                );
            }
            catch(RequestException $ex) {
                $data['status'] = 'failed';
                $data['errors'][] = 'Unable to get information. Try another url.';
                return $data;
            }
        }
        $data['status'] = 'success';
        $data['success_msg'] = 'success';
        $data['indentifier'] = $indentifier;
        return $data;
    }

    public function addTwitterStream(Request $request)
    {
        $url = $request->input('tw-stream-url');
        $title = $request->input('tw-stream-title');
        $category = ($request->has('category'))?$request->input('category'):'';
        $data = $this->validateTwitterUrl($url);
        if($title == '') {
            $data['status'] = 'failed';
            $data['errors'][] = 'title is empty';
        }
        if($data['status'] == 'failed')
            return $data;
        $twPage = new TwitterPage();
        $twPage->user_id  = auth()->user()->id;
        $twPage->page_name = $data['indentifier'];
        $twPage->title = $title;
        $twPage->category_id = ($category != '' )?$category:null;
        $twPage->save();
        return $data;
    }

    protected function validateTwitterUrl($url)
    {
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => ''
        ];
        $url = rtrim($url, '/');
        $url = strtok($url, '?');
        $parts = explode('/',$url);
        if(count($parts)>0){
            $params = array(
                'screen_name' => $parts[count($parts)-1],
                'count' => 1,
            );
            $indentifier = $parts[count($parts)-1];
            try {
                $timeline = Twitter::getUserTimeline($params);
            }
            catch(\RuntimeException $ex) {
                $data['status'] = 'failed';
                $data['errors'][] = 'Unable to get information. Try another url.';
                return $data;
            }
        }
        $data['status'] = 'success';
        $data['success_msg'] = 'success';
        $data['indentifier'] = $indentifier;
        return $data;
    }

    public function addYoutubeStream(Request $request)
    {
        $url = $request->input('yt-stream-url');
        $title = $request->input('yt-stream-title');
        $category = ($request->has('category'))?$request->input('category'):'';
        $data = $this->validateYoutubeUrl($url);
        if($title == '') {
            $data['status'] = 'failed';
            $data['errors'][] = 'title is empty';
        }
        if($data['status'] == 'failed')
            return $data;
        $ytChannel = new YoutubeChannel();
        $ytChannel->user_id  = auth()->user()->id;
        $ytChannel->channel_id = $data['indentifier'];
        $ytChannel->title = $title;
        $ytChannel->category_id = ($category != '' )?$category:null;
        $ytChannel->save();
        return $data;
    }

    protected function validateYoutubeUrl($url)
    {
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => ''
        ];
        $identifier = '';
        $url = rtrim($url, '/');
        $url = strtok($url, '?');
        $channelUrl = strpos($url,'channel');
        $userUrl = strpos($url,'user');
        $parts = explode('/',$url);
        if($channelUrl != false){
            $identifier = $parts[count($parts)-1];
        }
        if($userUrl != false){
            $result = Youtube::getChannelByName($parts[count($parts)-1]);
            $identifier = $result->id;
        }
        if(count($parts)>0){
            $part = ['id', 'snippet'];
            $maxResults = 1;
            $params = array(
                'type' => 'video',
                'channelId' => $identifier,
                'part' => implode(', ', $part),
                'maxResults' => $maxResults,
                'order' => 'date'
            );
            try {
                $result = Youtube::searchAdvanced($params, true);
                if($result['results'] === false)
                    throw new \Exception('Youtube api returned empty results');
            }
            catch(\Exception $ex) {
                $data['status'] = 'failed';
                $data['errors'][] = 'Unable to get information. Try another url.';
                return $data;
            }
        }
        $data['status'] = 'success';
        $data['success_msg'] = 'success';
        $data['indentifier'] = $identifier;
        return $data;
    }

    public function addGoogleStream(Request $request)
    {
        $url = $request->input('gg-stream-url');
        $title = $request->input('gg-stream-title');
        $category = ($request->has('category'))?$request->input('category'):'';
        $data = $this->validateGoogleUrl($url);
        if($title == '') {
            $data['status'] = 'failed';
            $data['errors'][] = 'title is empty';
        }
        if($data['status'] == 'failed')
            return $data;
        $ggChannel = new GooglePlusChannel();
        $ggChannel->user_id  = auth()->user()->id;
        $ggChannel->channel_id = $data['indentifier'];
        $ggChannel->title = $title;
        $ggChannel->category_id = ($category != '' )?$category:null;
        $ggChannel->save();
        return $data;
    }

    protected function validateGoogleUrl($url)
    {
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => ''
        ];
        $url = rtrim($url, '/');
        $url = strtok($url, '?');
        $parts = explode('/',$url);
        if(count($parts)>0){
            $client = new Client();
            $indentifier = $parts[count($parts)-1];
            try{
                $url = 'https://www.googleapis.com/plus/v1/people/'.$indentifier.'/activities/public/';
                $posts = $client->request('GET', $url, [
                        'query' => ['key' => config('services.google.api_key')]
                    ]
                );
            }
            catch(RequestException $ex) {
                $data['status'] = 'failed';
                $data['errors'][] = 'Unable to get information. Try another url.';
                return $data;
            }
            $posts = json_decode($posts->getBody()->getContents());
            if(count($posts->items) == 0) {
                $data['status'] = 'failed';
                $data['errors'][] = 'Unable to get information. Try another url.';
                return $data;
            }
        }
        $data['status'] = 'success';
        $data['success_msg'] = 'success';
        $data['indentifier'] = $indentifier;
        return $data;
    }
}
