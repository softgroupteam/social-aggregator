<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GooglePost;
use App\Models\FacebookPost;
use App\Models\Tweet;
use App\Models\YoutubeVideo;
use Illuminate\Support\Facades\DB;
use App\User;

class MyStreamsController extends Controller
{

    public function getFavouritePostsPage()
    {
        return view('backend.my-favourite-posts');
    }

    public function getAllFavouritePosts(Request $request)
    {
        $offset = $request->input('offset');
        $limit = 9;
        $data = [
            'posts' => [],
            'error' => ''
        ];
        $favouritePosts = DB::table('favourite_posts')
            ->where('user_id',auth()->user()->id)
            ->orderBy('created_at')
            ->offset($offset)
            ->limit($limit)
            ->get();
        $posts = [];
        foreach ($favouritePosts as $favPost){
            switch($favPost->favouritepost_type){
                case 'App\Models\FacebookPost':
                    $post = $this->getFacebookPost($favPost);
                    $posts[] = $post;
                    break;
                case 'App\Models\Tweet':
                    $post = $this->getTwitterPost($favPost);
                    $posts[] = $post;
                    break;
                case 'App\Models\YoutubeVideo':
                    $post = $this->getYoutubeVideo($favPost);
                    $posts[] = $post;
                    break;
                case 'App\Models\GooglePost':
                    $post = $this->getGooglePost($favPost);
                    $posts[] = $post;
                    break;
            }
        }
        $data = [
            'status' => 1,
            'posts' => $posts,
            'offset' => $limit+$offset
        ];
        echo json_encode($data);
    }



    public function getFacebookPost($favPost)
    {
        $interestedUser = User::where('id',$favPost->user_id)->first();
        $post = FacebookPost::where('id',$favPost->favouritepost_id)->first();
        $attachment = $post->facebookPostAttachments()->first();
        $item = [];
        switch($attachment->type){
            case 'video_inline':
            case 'video_autoplay':
            case 'video_share_youtube':
            $item['embed_video_html'] = $attachment->embed_video_html;
                break;
            default:
                $item['embed_video_html'] = '';
        }
        $item['id'] = $post->id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_time);
        $item['message'] = $post->message;
        $item['image_url'] = $post->fullpicture;
        $item['type'] = 'image';
        $item['readmore_url'] = $post->permalink;
        $item['created_at_timestamp'] = $post->social_created_time;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'facebook';
        $item['interested_user_id'] = $interestedUser->id;
        $item['interested_user_name'] = $interestedUser->name;
        $item['raw_message'] = htmlspecialchars($post->raw_message);
        $item['actions'] = $this->setActionsFavouritePost($post,'facebook');

        return $item;
    }

    public function getYoutubeVideo($favPost)
    {
        $interestedUser = User::where('id',$favPost->user_id)->first();
        $post = YoutubeVideo::where('id',$favPost->favouritepost_id)->first();
        $item = [];
        $item['id'] = $post->id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
        $item['message'] = $post->title;
        $item['image_url'] = $post->thumbnail_standart;
        $video_link = 'https://www.youtube.com/watch?v='.$post->video_id;
        $item['readmore_url'] = $video_link;
        $item['type'] = 'video';
        $embed_url = 'https://www.youtube.com/embed/'.$post->video_id;
        $item['embed_url'] = $embed_url;
        $item['embed_video_html'] = '<iframe width="640px" height="480px" src="https://www.youtube.com/embed/'.$post->video_id.'?autohide=1&version=3&autoplay=0"></iframe>';
        $item['created_at_timestamp'] = $post->social_created_at;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'youtube';
        $item['interested_user_id'] = $interestedUser->id;
        $item['interested_user_name'] = $interestedUser->name;
        $item['raw_message'] = htmlspecialchars($post->title);
        $item['actions'] = $this->setActionsFavouritePost($post,'youtube');

        return $item;
    }


    public function getTwitterPost($favPost)
    {
        $interestedUser = User::where('id',$favPost->user_id)->first();
        $post = Tweet::where('id',$favPost->favouritepost_id)->first();
        $item = [];
        $item['id'] = $post->id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
        $item['message'] = $post->text;
        $attachments = $post->tweetMedias();
        if($attachments->count()>0)
            $item['image_url'] = $attachments->first()->media_url;
        else
            $item['image_url'] = null;
        $item['readmore_url'] = 'https://twitter.com/'.$post->posted_from_id.'/status/'.$post->post_id;
        $item['type'] = 'image';
        $item['created_at_timestamp'] = $post->social_created_at;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'twitter';
        $item['interested_user_id'] = $interestedUser->id;
        $item['interested_user_name'] = $interestedUser->name;
        $item['raw_message'] = htmlspecialchars($post->raw_message);
        $item['actions'] = $this->setActionsFavouritePost($post,'twitter');
        return $item;
    }

    public function getGooglePost($favPost)
    {
        $interestedUser = User::where('id',$favPost->user_id)->first();
        $post = GooglePost::where('id',$favPost->favouritepost_id)->first();
        $item = [];
        $item['id'] = $post->id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
        $item['message'] = $post->content;
        $attachments = $post->googlePostAttachments();
        if($attachments->count()>0)
            $item['image_url'] = $attachments->first()->image_url;
        else
            $item['image_url'] = null;
        $item['readmore_url'] = $post->url;
        $item['type'] = 'image';
        $item['created_at_timestamp'] = $post->social_created_at;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'google';
        $item['interested_user_id'] = $interestedUser->id;
        $item['interested_user_name'] = $interestedUser->name;
        $item['raw_message'] = htmlspecialchars($post->raw_message);
        $item['actions'] = $this->setActionsFavouritePost($post,'google');
        return $item;

    }

    protected function setActionsFavouritePost($post,$socialType){
        $data = ['publish_to_stream' => '',
                'unpublish_from_stream' => ''];
        $user = auth()->user();
        switch($socialType){
            case 'facebook':
                $post = $user->streamFbPosts()->where('streampost_id',$post->id)->first();
                break;
            case 'twitter':
                $post = $user->streamTwPosts()->where('streampost_id',$post->id)->first();
                break;
            case 'youtube':
                $post = $user->streamYtVideos()->where('streampost_id',$post->id)->first();
                break;
            case 'google':
                $post = $user->streamGgPosts()->where('streampost_id',$post->id)->first();
                break;
        }
        if($post){
            $data['publish_to_stream'] = 'false';
            $data['unpublish_from_stream'] = 'true';
        }
        else{
            $data['publish_to_stream'] = 'true';
            $data['unpublish_from_stream'] = 'false';
        }
        return $data;

    }


    public function setPostLocaltime($timestamp)
    {
        $date = '';
        $timezone = auth()->user()->userInfo()->first()->timezone;
        if($timezone != ''){
            $TZ = new \DateTimeZone($timezone);
            $date  = new \DateTime();
            $date->setTimestamp($timestamp);
            $date->setTimezone($TZ);
            $date = $date->format('Y-m-d H:i:s');
        }
        return $date;
    }

    /* stream posts methods */
    public function getAllStreamPosts(Request $request)
    {
        $offset = $request->input('offset');
        $limit = 9;
        $data = [
            'posts' => [],
            'error' => ''
        ];
        $streamPosts = DB::table('user_stream_posts')
            ->where('user_id',auth()->user()->id)
            ->orderBy('created_at')
            ->offset($offset)
            ->limit($limit)
            ->get();
        $posts = [];
        foreach ($streamPosts as $streamPost){
            switch($streamPost->streampost_type){
                case 'App\Models\FacebookPost':
                    $post = $this->getStreamFacebookPost($streamPost);
                    $posts[] = $post;
                    break;
                case 'App\Models\Tweet':
                    $post = $this->getStreamTwitterPost($streamPost);
                    $posts[] = $post;
                    break;
                case 'App\Models\YoutubeVideo':
                    $post = $this->getStreamYoutubeVideo($streamPost);
                    $posts[] = $post;
                    break;
                case 'App\Models\GooglePost':
                    $post = $this->getStreamGooglePost($streamPost);
                    $posts[] = $post;
                    break;
            }
        }
        $data = [
            'status' => 1,
            'posts' => $posts,
            'offset' => $limit+$offset
        ];
        echo json_encode($data);
    }

    public function getAllStreamPostsByUser(Request $request)
    {
        $offset = $request->input('offset');
        $userId = $request->input('userId');
        $limit = 9;
        $data = [
            'posts' => [],
            'error' => ''
        ];
        $streamPosts = DB::table('user_stream_posts')
            ->where('user_id',$userId)
            ->orderBy('created_at')
            ->offset($offset)
            ->limit($limit)
            ->get();
        $posts = [];
        foreach ($streamPosts as $streamPost){
            switch($streamPost->streampost_type){
                case 'App\Models\FacebookPost':
                    $post = $this->getStreamFacebookPost($streamPost);
                    $posts[] = $post;
                    break;
                case 'App\Models\Tweet':
                    $post = $this->getStreamTwitterPost($streamPost);
                    $posts[] = $post;
                    break;
                case 'App\Models\YoutubeVideo':
                    $post = $this->getStreamYoutubeVideo($streamPost);
                    $posts[] = $post;
                    break;
                case 'App\Models\GooglePost':
                    $post = $this->getStreamGooglePost($streamPost);
                    $posts[] = $post;
                    break;
            }
        }
        $data = [
            'status' => 1,
            'posts' => $posts,
            'offset' => $limit+$offset
        ];
        echo json_encode($data);
    }

    public function getStreamFacebookPost($sPost)
    {
        $post = FacebookPost::where('id',$sPost->streampost_id)->first();
        $attachment = $post->facebookPostAttachments()->first();
        $item = [];
        switch($attachment->type){
            case 'video_inline':
            case 'video_autoplay':
            case 'video_share_youtube':
                $item['embed_video_html'] = $attachment->embed_video_html;
                break;
            default:
                $item['embed_video_html'] = '';
        }
        $item['id'] = $post->id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_time);
        $item['message'] = $post->message;
        $item['image_url'] = $post->fullpicture;
        $item['type'] = 'image';
        $item['readmore_url'] = $post->permalink;
        $item['created_at_timestamp'] = $post->social_created_time;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'facebook';
        $item['raw_message'] = htmlspecialchars($post->raw_message);

        return $item;
    }

    public function getStreamYoutubeVideo($sPost)
    {
        $post = YoutubeVideo::where('id',$sPost->streampost_id)->first();
        $item = [];
        $item['id'] = $post->id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
        $item['message'] = $post->title;
        $item['image_url'] = $post->thumbnail_standart;
        $video_link = 'https://www.youtube.com/watch?v='.$post->video_id;
        $item['readmore_url'] = $video_link;
        $item['type'] = 'video';
        $embed_url = 'https://www.youtube.com/embed/'.$post->video_id;
        $item['embed_url'] = $embed_url;
        $item['embed_video_html'] = '<iframe width="640px" height="480px" src="https://www.youtube.com/embed/'.$post->video_id.'?autohide=1&version=3&autoplay=0"></iframe>';
        $item['created_at_timestamp'] = $post->social_created_at;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'youtube';
        $item['raw_message'] = htmlspecialchars($post->title);

        return $item;
    }


    public function getStreamTwitterPost($sPost)
    {
        $post = Tweet::where('id',$sPost->streampost_id)->first();
        $item = [];
        $item['id'] = $post->id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
        $item['message'] = $post->text;
        $attachments = $post->tweetMedias();
        if($attachments->count()>0)
            $item['image_url'] = $attachments->first()->media_url;
        else
            $item['image_url'] = null;
        $item['readmore_url'] = 'https://twitter.com/'.$post->posted_from_id.'/status/'.$post->post_id;
        $item['type'] = 'image';
        $item['created_at_timestamp'] = $post->social_created_at;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'twitter';
        $item['raw_message'] = htmlspecialchars($post->raw_message);
        return $item;
    }

    public function getStreamGooglePost($sPost)
    {
        $post = GooglePost::where('id',$sPost->favouritepost_id)->first();
        $item = [];
        $item['id'] = $post->id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
        $item['message'] = $post->content;
        $attachments = $post->googlePostAttachments();
        if($attachments->count()>0)
            $item['image_url'] = $attachments->first()->image_url;
        else
            $item['image_url'] = null;
        $item['readmore_url'] = $post->url;
        $item['type'] = 'image';
        $item['created_at_timestamp'] = $post->social_created_at;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'google';
        $item['raw_message'] = htmlspecialchars($post->raw_message);
        return $item;

    }


}
