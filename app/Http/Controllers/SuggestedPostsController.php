<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GooglePost;
use App\Models\FacebookPost;
use App\Models\Tweet;
use App\Models\YoutubeVideo;
use Illuminate\Support\Facades\DB;

class SuggestedPostsController extends Controller
{
    protected $limit;

    public function __construct()
    {
        $this->limit = 9;
    }

    public function getPage()
    {
        return view('backend.suggested-posts');
    }


    public function getGooglePosts(Request $request)
    {
        $posts = [];
        $offset  = $request->input('offset');
        $favouritePostsIds = auth()->user()->favouriteGgPosts()->allRelatedIds()->toArray();
        $posts = GooglePost::where('published_here', true)
            ->whereNotIn('id',  $favouritePostsIds)
            ->orderBy('social_created_at', 'desc')
            ->offset($offset)
            ->limit($this->limit)
            ->get();

        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['id'] = $post->id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
            $item['message'] = $post->content;
            $attachments = $post->googlePostAttachments();
            if($attachments->count()>0)
                $item['image_url'] = $attachments->first()->image_url;
            else
                $item['image_url'] = null;
            $item['filter_class'] = 'uss-google';
            $item['readmore_url'] = $post->url;
            $item['type'] = 'image';
            $item['created_at_timestamp'] = $post->social_created_at;
            $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
            $item['social_type'] = 'google';
            $item['save_action'] = route('ajax.store-gglpost');
            $item['delete_action'] = route('ajax.delete-gglpost');
            $item['publish_action'] = route('ajax.publish-gglpost');
            $item['unpublish_action'] = route('ajax.unpublish-gglpost');
            $item['raw_message'] = htmlspecialchars($post->raw_message);
            array_push($resultPosts,$item);
        }
        $data = [
            'status' => 1,
            'posts' => $resultPosts,
            'offset' => $offset+$this->limit,
        ];
        echo json_encode($data);

    }

    public function getFacebookPosts(Request $request)
    {
        $posts = [];
        $state = '';
        $offset  = $request->input('offset');
        $favouritePostsIds = auth()->user()->favouriteFbPosts()->allRelatedIds()->toArray();
        $posts = FacebookPost::where('published_here',true)
            ->whereNotIn('id',  $favouritePostsIds)
            ->orderBy('social_created_time','desc')
            ->offset($offset)
            ->limit($this->limit)
            ->get();
        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['id'] = $post->id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_time);
            $item['message'] = $post->message;
            $item['image_url'] = $post->fullpicture;
            $item['filter_class'] = 'uss-facebook';
            $item['type'] = 'image';
            $item['readmore_url'] = $post->permalink;
            $item['created_at_timestamp'] = $post->social_created_time;
            $item['social_type'] = 'facebook';
            $item['add_favourite_action'] = route('ajax.add-to-favourite');
            array_push($resultPosts,$item);
        }
        $data = [
            'status' => 1,
            'posts' => $resultPosts,
            'offset' => $offset+$this->limit,
        ];
        echo json_encode($data);
    }

    public function getYoutubeVideos(Request $request)
    {
        $posts = [];
        $state = '';
        $offset  = $request->input('offset');
        $favouritePostsIds = auth()->user()->favouriteYtVideos()->allRelatedIds()->toArray();
        $posts = YoutubeVideo::where('published_here', true)
            ->whereNotIn('id',  $favouritePostsIds)
            ->orderBy('social_created_at', 'desc')
            ->offset($offset)
            ->limit($this->limit)
            ->get();
        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['id'] = $post->id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
            $item['message'] = $post->title;
            $item['image_url'] = $post->thumbnail_standart;
            $item['filter_class'] = 'uss-youtube';
            $video_link = 'https://www.youtube.com/watch?v='.$post->video_id;
            $item['readmore_url'] = $video_link;
            $item['type'] = 'video';
            $embed_url = 'https://www.youtube.com/embed/'.$post->video_id;
            $item['embed_url'] = $embed_url;
            $item['created_at_timestamp'] = $post->social_created_at;
            $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
            $item['social_type'] = 'youtube';
            $item['add_favourite_action'] = route('ajax.add-to-favourite');
            $item['raw_message'] = htmlspecialchars($post->title);
            array_push($resultPosts,$item);
        }
        $data = [
            'status' => 1,
            'posts' => $resultPosts,
            'offset' => $offset+$this->limit,
        ];
        echo json_encode($data);
    }


    public function getTwitterPosts(Request $request)
    {
        $posts = [];
        $state = '';
        $offset  = $request->input('offset');
        $favouritePostsIds = auth()->user()->favouriteTwPosts()->allRelatedIds()->toArray();
        $posts = Tweet::where('published_here',true)
            ->whereNotIn('id',  $favouritePostsIds)
            ->orderBy('social_created_at','desc')
            ->offset($offset)
            ->limit($this->limit)
            ->get();
        $resultPosts = [];
        foreach($posts as $post){
            $item = [];
            $item['id'] = $post->id;
            $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
            $item['message'] = $post->text;
            $attachments = $post->tweetMedias();
            if($attachments->count()>0)
                $item['image_url'] = $attachments->first()->media_url;
            else
                $item['image_url'] = null;
            $item['filter_class'] = 'uss-twitter';
            $item['readmore_url'] = 'https://twitter.com/'.$post->posted_from_id.'/status/'.$post->post_id;
            $item['type'] = 'image';
            $item['created_at_timestamp'] = $post->social_created_at;
            $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
            $item['social_type'] = 'twitter';
            $item['add_favourite_action'] = route('ajax.add-to-favourite');
            $item['raw_message'] = htmlspecialchars($post->raw_message);
            array_push($resultPosts,$item);
        }
        $data = [
            'status' => 1,
            'posts' => $resultPosts,
            'offset' => $offset+$this->limit,
        ];
        echo json_encode($data);
    }

    public function addToFavourite(Request $request)
    {
        $postId = $request->input('post_id');
        $postType = $request->input('post_type');
        $postClass = '';
        switch($postType){
            case 'facebook':
                $postClass = 'App\Models\FacebookPost';
                break;
            case 'twitter':
                $postClass = 'App\Models\Tweet';
                break;
            case 'youtube':
                $postClass = 'App\Models\YoutubeVideo';
                break;
            case 'google':
                $postClass = 'App\Models\GooglePost';
                break;
        }


        DB::table('favourite_posts')->insert(
        ['user_id' => auth()->user()->id, 'favouritepost_id' => $postId, 'favouritepost_type' => $postClass]);
        $data = ['status'=>'success', 'success_msg' => 'success','post_id' => $postId,'post_type' => $postType];
        return $data;
    }

    public function setPostLocaltime($timestamp)
    {
        $date = '';
        $timezone = auth()->user()->userInfo()->first()->timezone;
        if($timezone != ''){
            $TZ = new \DateTimeZone($timezone);
            $date  = new \DateTime();
            $date->setTimestamp($timestamp);
            $date->setTimezone($TZ);
            $date = $date->format('Y-m-d H:i:s');
        }
        return $date;
    }
}
