<?php

namespace App\Http\Controllers\Youtube;

use App\Models\YoutubeVideo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alaouy\Youtube\Facades\Youtube;
use Illuminate\Support\Facades\DB;



class VideosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getVideosByChannel($channelId)
    {
        $user = auth()->user();
        if($user->hasRole('user'))
            return $this->getVideosByChannelForUser($channelId);

        $part = ['id', 'snippet'];
        $maxResults = 6;
        $params = array(
            'type' => 'video',
            'channelId' => $channelId,
            'part' => implode(', ', $part),
            'maxResults' => $maxResults,
            'order' => 'date'
        );
        try {
            $result = Youtube::searchAdvanced($params, true);
            if($result['results'] === false)
                throw new \Exception('Youtube api returned empty results');
        }
        catch(\Exception $ex){
            return view('backend.noresults');
        }
        if (isset($result['results'])) {
            $videos = $result['results'];
        }

        if (isset($result['info']['nextPageToken'])) {
            $pageToken = $result['info']['nextPageToken'];
        }
        $detailedVideos = [];
        foreach ($videos as $video){
            $detailedVideos[] = Youtube::getVideoInfo($video->id->videoId);
        }
        $detailedVideos = $this->addProperties($detailedVideos);
        return view('backend.youtube-channel-feed')->with(['videos' => $detailedVideos, 'pageToken' => $pageToken,'channelId' => $channelId]);
    }

    public function getVideosByChannelAjax(Request $request)
    {
        $data = [
            'posts' => [],
            'error' => ''
        ];
        if($request->pageToken == '') {
            $data['error'] = 'page token is empty';
            $data['status'] = 0;
            echo json_encode($data);
        }
        $part = ['id', 'snippet'];
        $maxResults = 6;
        $params = array(
            'type' => 'video',
            'channelId' => $request->channelId,
            'part' => implode(', ', $part),
            'maxResults' => $maxResults,
            'order' => 'date'
        );

        $params['pageToken'] = $request->pageToken;

        $result = Youtube::searchAdvanced($params,true);

        if (isset($result['results'])) {
            $videos = $result['results'];
            $detailedVideos = [];
            foreach ($videos as $video){
                $detailedVideos[] = Youtube::getVideoInfo($video->id->videoId);
            }
            $detailedVideos = $this->addProperties($detailedVideos);
            $data['posts'] = $detailedVideos;
        }

        if (isset($result['info']['nextPageToken']))
            $data['pageToken'] = $result['info']['nextPageToken'];
        else
            $data['pageToken'] = '';
        $data['channelId'] = $request->channelId;
        $data['status'] = 1;
        echo json_encode($data);
    }

    public function setPostLocaltime($video)
    {
        $timezone = auth()->user()->userInfo()->first()->timezone;
        if($timezone != ''){
                $TZ = new \DateTimeZone($timezone);
                $date  = new \DateTime($video->snippet->publishedAt);
                $date->setTimezone($TZ);
                $date = $date->format('Y-m-d H:i:s');
                $video->createdLocal = $date;
        }
        return $video;
    }

    public function store(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $video = Youtube::getVideoInfo($request->video_id);
        $videoEntity = new YoutubeVideo();
        $videoEntity->video_id = $video->id;
        $videoEntity->title = $video->snippet->title;
        $videoEntity->description = formatUrlsInText($video->snippet->description);
        $videoEntity->raw_message = $video->snippet->description;
        $videoEntity->thumbnail_standart = $video->snippet->thumbnails->standard->url;
        $videoEntity->channel_id = $video->snippet->channelId;
        $videoEntity->social_category = $video->snippet->categoryId;
        $videoEntity->social_created_at = strtotime($video->snippet->publishedAt);
        $videoEntity->embed_html = $video->player->embedHtml;
        $videoEntity->published_here = false;
        $videoEntity->user_id = auth()->user()->id;
        $videoEntity->save();

        $data['status'] = '1';
        $data['state'] = 'saved_notpublished';
        return $data;
    }

    public function publish(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $videoId = $request->video_id;
        $video = YoutubeVideo::where('video_id',$videoId)->first();
        $video->published_here = true;
        $video->save();
        $data['status'] = '1';
        $data['state'] = 'saved_published';
        return $data;
    }

    public function unpublish(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $videoId = $request->video_id;
        $video = YoutubeVideo::where('video_id',$videoId)->first();
        $video->published_here = false;
        $video->save();
        $data['status'] = '1';
        $data['state'] = 'saved_notpublished';
        return $data;
    }

    public function delete(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];

        $videoId = $request->video_id;
        $video = YoutubeVideo::where('video_id',$videoId)->first();
        $video->delete();
        $data['status'] = '1';
        $data['state'] = 'notsaved';
        return $data;
    }

    protected function checkState($videoId){
        $video = YoutubeVideo::where('video_id',$videoId)->first();
        if(!$video)
            return 'notsaved';
        if($video->published_here == true)
            return 'saved_published';
        else
            return 'saved_notpublished';
    }

    protected function setVideoState($video){
        $video->state = $this->checkState($video->id);
        return $video;
    }

    protected function addProperties($videos){
        foreach($videos as $video){
            $video = $this->setVideoState($video);
            $video = $this->setPostLocaltime($video);
        }
        return $videos;
    }

    /* user role methods */

    public function getVideosByChannelForUser($channelId)
    {
        $part = ['id', 'snippet'];
        $maxResults = 6;
        $params = array(
            'type' => 'video',
            'channelId' => $channelId,
            'part' => implode(', ', $part),
            'maxResults' => $maxResults,
            'order' => 'date'
        );
        try {
            $result = Youtube::searchAdvanced($params, true);
            if($result['results'] === false)
                throw new \Exception('Youtube api returned empty results');
        }
        catch(\Exception $ex){
            return view('backend.noresults');
        }
        if (isset($result['results'])) {
            $videos = $result['results'];
        }

        if (isset($result['info']['nextPageToken'])) {
            $pageToken = $result['info']['nextPageToken'];
        }
        $detailedVideos = [];
        foreach ($videos as $video){
            $detailedVideos[] = Youtube::getVideoInfo($video->id->videoId);
        }
        $detailedVideos = $this->addPropertiesForUser($detailedVideos);

        return view('backend.youtube-channel-feed-foruser')->with(['videos' => $detailedVideos, 'pageToken' => $pageToken,'channelId' => $channelId]);
    }

    public function getVideosByChannelAjaxForUser(Request $request)
    {
        $data = [
            'posts' => [],
            'error' => ''
        ];
        if($request->pageToken == '') {
            $data['error'] = 'page token is empty';
            $data['status'] = 0;
            echo json_encode($data);
        }
        $part = ['id', 'snippet'];
        $maxResults = 6;
        $params = array(
            'type' => 'video',
            'channelId' => $request->channelId,
            'part' => implode(', ', $part),
            'maxResults' => $maxResults,
            'order' => 'date'
        );
        $params['pageToken'] = $request->pageToken;

        $result = Youtube::searchAdvanced($params,true);

        if (isset($result['results'])) {
            $videos = $result['results'];
            $detailedVideos = [];
            foreach ($videos as $video){
                $detailedVideos[] = Youtube::getVideoInfo($video->id->videoId);
            }
            $detailedVideos = $this->addPropertiesForUser($detailedVideos);
            $data['posts'] = $detailedVideos;
        }

        if (isset($result['info']['nextPageToken']))
            $data['pageToken'] = $result['info']['nextPageToken'];
        else
            $data['pageToken'] = '';
        $data['channelId'] = $request->channelId;
        $data['status'] = 1;
        echo json_encode($data);
    }

    public function addToFavouriteForUser(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $video = Youtube::getVideoInfo($request->video_id);
        $videoEntity = new YoutubeVideo();
        $videoEntity->video_id = $video->id;
        $videoEntity->title = $video->snippet->title;
        $videoEntity->description = formatUrlsInText($video->snippet->description);
        $videoEntity->raw_message = $video->snippet->description;
        $videoEntity->thumbnail_standart = $video->snippet->thumbnails->standard->url;
        $videoEntity->channel_id = $video->snippet->channelId;
        $videoEntity->social_category = $video->snippet->categoryId;
        $videoEntity->social_created_at = strtotime($video->snippet->publishedAt);
        $videoEntity->embed_html = $video->player->embedHtml;
        $videoEntity->published_here = false;
        $videoEntity->user_id = auth()->user()->id;
        $videoEntity->save();

        DB::table('favourite_posts')->insert(
            ['user_id' => auth()->user()->id, 'favouritepost_id' => $videoEntity->id, 'favouritepost_type' => 'App\Models\YoutubeVideo']);

        $data['status'] = '1';
        $data['state'] = 'saved_favourite';
        return $data;
    }

    public function deleteFromFavouriteForUser(Request $request)
    {
        $user = auth()->user();

        $data = [
            'status' => '',
            'error' => ''
        ];

        $videoId = $request->video_id;
        $video = YoutubeVideo::where('video_id',$videoId)->first();
        DB::table('favourite_posts')->where('user_id',$user->id)
            ->where('favouritepost_id',$video->id)
            ->where('favouritepost_type','App\Models\YoutubeVideo')
            ->delete();
        $video->delete();
        $data['status'] = '1';
        $data['state'] = 'notsaved_notfavourite';
        return $data;
    }

    protected function checkStateForUser($videoId){
        $user = auth()->user();
        $post = YoutubeVideo::where('video_id',$videoId)->first();
        $favPost = null;
        if($post) {
            $favPost = DB::table('favourite_posts')->where('user_id', $user->id)
                ->where('favouritepost_id', $post->id)
                ->where('favouritepost_type', 'App\Models\YoutubeVideo')
                ->first();
        }
        if(!$favPost)
            return 'notsaved_notfavourite';
        else
            return 'saved_favourite';
    }

    protected function setVideoStateForUser($video){
        $video->state = $this->checkStateForUser($video->id);
        return $video;
    }

    protected function addPropertiesForUser($videos){
        foreach($videos as $video){
            $video = $this->setVideoStateForUser($video);
            $video = $this->setPostLocaltime($video);
        }
        return $videos;
    }

    /* favourite posts methods */
    public function addToUserStream(Request $request){
        $data = ['status' =>'',
            'msg' =>''];
        $user = auth()->user();
        $postId = $request->input('postId');
        $post = $user->streamYtVideos()->where('streampost_id',$postId)->first();
        if(!$post){
            $streamPost = YoutubeVideo::where('id',$postId)->first();
            $user->streamYtVideos()->attach($streamPost);
            $data['status'] = 'success';
            $data['msg'] = 'success';
        }
        else{
            $data['status'] = 'failed';
            $data['msg'] = 'post already exists';
        }
        return $data;
    }

    public function deleteFromUserStream(Request $request){
        $data = ['status' =>'',
            'msg' =>''];
        $user = auth()->user();
        $postId = $request->input('postId');
        $post = $user->streamYtVideos()->where('streampost_id',$postId)->first();
        if($post){
            $streamPost = YoutubeVideo::where('id',$postId)->first();
            $user->streamYtVideos()->detach($streamPost);
            $data['status'] = 'success';
            $data['msg'] = 'success';
        }
        else{
            $data['status'] = 'failed';
            $data['msg'] = 'post doesn`t exist';
        }
        return $data;
    }
}
