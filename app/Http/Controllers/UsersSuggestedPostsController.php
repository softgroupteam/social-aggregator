<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\GooglePost;
use App\Models\FacebookPost;
use App\Models\Tweet;
use App\Models\YoutubeVideo;

class UsersSuggestedPostsController extends Controller
{

    public function getPage()
    {
        return view('backend.users-suggested-posts');
    }


    public function getAllPosts(Request $request)
    {
        $limit = 9;
        $data = [
            'posts' => [],
            'error' => ''
        ];
        $followees = auth()->user()->following()->allRelatedIds()->toArray();
        $followees[] = auth()->user()->id;
        $favouritePosts = DB::table('favourite_posts')
            ->whereNotIn('user_id',$followees)
            ->inRandomOrder()
            ->limit($limit)
            ->get();
        $posts = [];
        foreach ($favouritePosts as $favPost){
            switch($favPost->favouritepost_type){
                case 'App\Models\FacebookPost':
                    $post = $this->getFacebookPost($favPost);
                    $posts[] = $post;
                    break;
                case 'App\Models\Tweet':
                    $post = $this->getTwitterPost($favPost);
                    $posts[] = $post;
                    break;
                case 'App\Models\YoutubeVideo':
                    $post = $this->getYoutubeVideo($favPost);
                    break;
                    $posts[] = $post;
                case 'App\Models\GooglePost':
                    $post = $this->getGooglePost($favPost);
                    $posts[] = $post;
                    break;
            }
        }
        $data = [
            'status' => 1,
            'posts' => $posts
        ];
        echo json_encode($data);
    }



    public function getFacebookPost($favPost)
    {
        $interestedUser = User::where('id',$favPost->user_id)->first();
        $post = FacebookPost::where('id',$favPost->favouritepost_id)->first();
        $item = [];
        $item['id'] = $post->post_id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_time);
        $item['message'] = $post->message;
        $item['image_url'] = $post->fullpicture;
        $item['type'] = 'image';
        $item['readmore_url'] = $post->permalink;
        $item['created_at_timestamp'] = $post->social_created_time;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'facebook';
        $item['interested_user_id'] = $interestedUser->id;
        $item['interested_user_name'] = $interestedUser->name;
        $item['save_action'] = route('ajax.store-fbpost');
        $item['delete_action'] = route('ajax.delete-fbpost');
        $item['publish_action'] = route('ajax.publish-fbpost');
        $item['unpublish_action'] = route('ajax.unpublish-fbpost');
        $item['raw_message'] = htmlspecialchars($post->raw_message);

        return $item;
    }

    public function getYoutubeVideo($favPost)
    {
        $interestedUser = User::where('id',$favPost->user_id)->first();
        $post = YoutubeVideo::where('id',$favPost->favouritepost_id)->first();
        $item = [];
        $item['id'] = $post->post_id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
        $item['message'] = $post->title;
        $item['image_url'] = $post->thumbnail_standart;
        $video_link = 'https://www.youtube.com/watch?v='.$post->video_id;
        $item['readmore_url'] = $video_link;
        $item['type'] = 'video';
        $embed_url = 'https://www.youtube.com/embed/'.$post->video_id;
        $item['embed_url'] = $embed_url;
        $item['created_at_timestamp'] = $post->social_created_at;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'youtube';
        $item['interested_user_id'] = $interestedUser->id;
        $item['interested_user_name'] = $interestedUser->name;
        $item['save_action'] = route('ajax.store-ytpost');
        $item['delete_action'] = route('ajax.delete-ytpost');
        $item['publish_action'] = route('ajax.publish-ytpost');
        $item['unpublish_action'] = route('ajax.unpublish-ytpost');
        $item['raw_message'] = htmlspecialchars($post->title);

        return $item;
    }


    public function getTwitterPost($favPost)
    {
        $interestedUser = User::where('id',$favPost->user_id)->first();
        $post = Tweet::where('id',$favPost->favouritepost_id)->first();
        $item = [];
        $item['id'] = $post->post_id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
        $item['message'] = $post->text;
        $attachments = $post->tweetMedias();
        if($attachments->count()>0)
            $item['image_url'] = $attachments->first()->media_url;
        else
            $item['image_url'] = null;
        $item['readmore_url'] = 'https://twitter.com/'.$post->posted_from_id.'/status/'.$post->post_id;
        $item['type'] = 'image';
        $item['created_at_timestamp'] = $post->social_created_at;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'twitter';
        $item['interested_user_id'] = $interestedUser->id;
        $item['interested_user_name'] = $interestedUser->name;
        $item['save_action'] = route('ajax.store-twpost');
        $item['delete_action'] = route('ajax.delete-twpost');
        $item['publish_action'] = route('ajax.publish-twpost');
        $item['unpublish_action'] = route('ajax.unpublish-twpost');
        $item['raw_message'] = htmlspecialchars($post->raw_message);
        return $item;
    }

    public function getGooglePost($favPost)
    {
        $interestedUser = User::where('id',$favPost->user_id)->first();
        $post = GooglePost::where('id',$favPost->favouritepost_id)->first();
        $item = [];
        $item['id'] = $post->post_id;
        $item['social_created_at'] = $this->setPostLocaltime($post->social_created_at);
        $item['message'] = $post->content;
        $attachments = $post->googlePostAttachments();
        if($attachments->count()>0)
            $item['image_url'] = $attachments->first()->image_url;
        else
            $item['image_url'] = null;
        $item['readmore_url'] = $post->url;
        $item['type'] = 'image';
        $item['created_at_timestamp'] = $post->social_created_at;
        $item['state'] = ($post->published_here == true)?'saved_published':'saved_notpublished';
        $item['social_type'] = 'google';
        $item['interested_user_id'] = $interestedUser->id;
        $item['interested_user_name'] = $interestedUser->name;
        $item['save_action'] = route('ajax.store-gglpost');
        $item['delete_action'] = route('ajax.delete-gglpost');
        $item['publish_action'] = route('ajax.publish-gglpost');
        $item['unpublish_action'] = route('ajax.unpublish-gglpost');
        $item['raw_message'] = htmlspecialchars($post->raw_message);
        return $item;

    }

    public function followUser(Request $request)
    {
        $user = auth()->user();
        $followee_id = $request->input('user_id');
        $user->following()->attach($followee_id);
        $data = [
            'status' => 'success',
            'user_id' => $followee_id
        ];
        echo json_encode($data);
    }

    public function setPostLocaltime($timestamp)
    {
        $date = '';
        $timezone = auth()->user()->userInfo()->first()->timezone;
        if($timezone != ''){
            $TZ = new \DateTimeZone($timezone);
            $date  = new \DateTime();
            $date->setTimestamp($timestamp);
            $date->setTimezone($TZ);
            $date = $date->format('Y-m-d H:i:s');
        }
        return $date;
    }

}
