<?php

namespace App\Http\Controllers\Twitter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Thujohn\Twitter\Facades\Twitter;
use App\Models\Tweet;
use App\Models\TweetHashtag;
use App\Models\TweetMedia;
use App\Models\TweetUrl;
use Illuminate\Support\Facades\DB;

class TweetsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTweetsByUser(Request $request, $twitterUserName)
    {
        $lastElementId = '';
        $user = auth()->user();
        if($user->hasRole('user'))
            return $this->getTweetsByUserForUser($request,$twitterUserName);

        $params = array(
            'screen_name' => $twitterUserName,
            'count' => 10,
            'tweet_mode' => 'extended'
        );
        try {
            $timeline = Twitter::getUserTimeline($params);
        }
        catch(\Exception $ex){
            return $ex->getMessage();
            return view('backend.noresults');
        }

        if(count($timeline)!=0) {
            $lastElementId = $timeline[count($timeline) - 1]->id_str;
        }
        $timeline = $this->addProperties($timeline);
        return view('backend.twitter-user-tweets',compact(['timeline','lastElementId','twitterUserName']));
    }

    public function getTweetsByUserAjax(Request $request)
    {
        $data = [
            'posts' => [],
            'error' => ''
        ];
        if($request->lastElement == '') {
            $data['error'] = 'page token is empty';
            $data['status'] = 0;
            echo json_encode($data);
        }
        $lastElementId = '';

        $params = array(
            'screen_name' => $request->twitterUserName,
            'count' => 10,
            'tweet_mode' => 'extended'
        );

        if($request->lastElement != '')
            $params['max_id'] = $request->lastElement;


        $timeline =  Twitter::getUserTimeline($params);

        if(count($timeline)!=0) {
            $lastElementId = $timeline[count($timeline) - 1]->id_str;
        }
        $timeline = $this->addProperties($timeline);
        $data['posts'] =  $timeline;
        $data['lastElement'] = $lastElementId;
        $data['twitterUserName'] = $request->twitterUserName;
        $data['status'] = 1;
        echo json_encode($data);
    }

    public function setPostLocaltime($post)
    {
        $timezone = auth()->user()->userInfo()->first()->timezone;
        if($timezone != ''){
                $TZ = new \DateTimeZone($timezone);
                $date  = new \DateTime($post->created_at);
                $date->setTimezone($TZ);
                $date = $date->format('Y-m-d H:i:s');
                $post->createdLocal = $date;
        }
        return $post;
    }

    public function store(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $tweet = Twitter::getTweet($request->post_id);
        //dd($tweet);
        $tweetEntity = new Tweet();
        $tweetEntity->post_id = $tweet->id_str;
        $text = clearPlainUrlsInText($tweet->text);
        $text = formatTweetHashtags($text);
        $text = formatTweetUserMentions($text);
        $tweetEntity->text = $text;
        $tweetEntity->raw_message = $tweet->text;
        $tweetEntity->source = $tweet->source;
        $tweetEntity->posted_from_id = $tweet->user->id;
        $tweetEntity->published_here = false;
        $tweetEntity->social_created_at = strtotime($tweet->created_at);
        $tweetEntity->user_id = auth()->user()->id;
        $tweetEntity->save();

        if(property_exists($tweet->entities,'hashtags')){
            foreach ($tweet->entities->hashtags as $hashtag){
                $entity = new TweetHashtag();
                $entity->text = $hashtag->text;
                $tweetEntity->tweetHashtags()->save($entity);
            }
        }

        if(property_exists($tweet->entities,'media')){
            foreach ($tweet->entities->media as $media){
                $entity = new TweetMedia();
                $entity->media_id = $media->id;
                $entity->media_url = $media->media_url;
                $entity->url = $media->url;
                $entity->type = $media->type;
                $tweetEntity->tweetMedias()->save($entity);
            }
        }

        if(property_exists($tweet->entities,'urls')){
            foreach ($tweet->entities->urls as $url){
                $entity = new TweetUrl();
                $entity->url = $url->url;
                $entity->expanded_url = $url->expanded_url;
                $entity->display_url = $url->display_url;
                $tweetEntity->tweetUrls()->save($entity);
            }
        }

        $data['status'] = '1';
        $data['state'] = 'saved_notpublished';
        return $data;

    }

    public function publish(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $postId = $request->post_id;
        $post = Tweet::where('post_id',$postId)->first();
        $post->published_here = true;
        $post->save();
        $data['status'] = '1';
        $data['state'] = 'saved_published';
        return $data;
    }

    public function unpublish(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $postId = $request->post_id;
        $post = Tweet::where('post_id',$postId)->first();
        $post->published_here = false;
        $post->save();
        $data['status'] = '1';
        $data['state'] = 'saved_notpublished';
        return $data;
    }

    public function delete(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];

        $postId = $request->post_id;
        $post = Tweet::where('post_id',$postId)->first();
        $post->tweetHashtags()->delete();
        $post->tweetMedias()->delete();
        $post->tweetUrls()->delete();
        $post->delete();
        $data['status'] = '1';
        $data['state'] = 'notsaved';
        return $data;
    }

    protected function checkState($postId){
        $post = Tweet::where('post_id',$postId)->first();
        if(!$post)
            return 'notsaved';
        if($post->published_here == true)
            return 'saved_published';
        else
            return 'saved_notpublished';
    }

    protected function setPostState($post){
        $post->state = $this->checkState($post->id);
        return $post;
    }



    protected function addProperties($posts){
        foreach($posts as $post){
            $post = $this->setPostState($post);
            $post = $this->setPostLocaltime($post);
            $formated_text = formatUrlsInText($post->full_text);
            $formated_text = formatTweetHashtags($formated_text);
            $formated_text = formatTweetUserMentions($formated_text);
            $post->formatted_text = $formated_text;
        }
        return $posts;
    }


    public function testTweetPublish()
    {
        Twitter::reconfig(['token' => '869191753194188800-e5DE31NCRVsR0ZAh5CG5dSeEHSB4Tk6', 'secret' => 'dLFqmLYrc9kA6Z4ij6LC6XWWHxvhm7zRmncYhmaJ7kxZa']);
        Twitter::postTweet(['status' => 'Laravel is beautiful', 'format' => 'json']);
    }


    /* user role methods */

    public function getTweetsByUserForUser(Request $request, $twitterUserName)
    {
        $lastElementId = '';

        $params = array(
            'screen_name' => $twitterUserName,
            'count' => 10,
            'tweet_mode' => 'extended'
        );
        try {
            $timeline = Twitter::getUserTimeline($params);
        }
        catch(\Exception $ex){
            return $ex->getMessage();
            return view('backend.noresults');
        }

        if(count($timeline)!=0) {
            $lastElementId = $timeline[count($timeline) - 1]->id_str;
        }
        $timeline = $this->addPropertiesForUser($timeline);
        return view('backend.twitter-user-tweets-foruser',compact(['timeline','lastElementId','twitterUserName']));
    }

    public function getTweetsByUserAjaxForUser(Request $request)
    {
        $data = [
            'posts' => [],
            'error' => ''
        ];
        if($request->lastElement == '') {
            $data['error'] = 'page token is empty';
            $data['status'] = 0;
            echo json_encode($data);
        }
        $lastElementId = '';

        $params = array(
            'screen_name' => $request->twitterUserName,
            'count' => 10,
            'tweet_mode' => 'extended'
        );

        if($request->lastElement != '')
            $params['max_id'] = $request->lastElement;


        $timeline =  Twitter::getUserTimeline($params);

        if(count($timeline)!=0) {
            $lastElementId = $timeline[count($timeline) - 1]->id_str;
        }
        $timeline = $this->addPropertiesForUser($timeline);
        $data['posts'] =  $timeline;
        $data['lastElement'] = $lastElementId;
        $data['twitterUserName'] = $request->twitterUserName;
        $data['status'] = 1;
        echo json_encode($data);
    }

    public function addToFavouriteForUser(Request $request)
    {
        $data = [
            'status' => '',
            'error' => ''
        ];
        $tweet = Twitter::getTweet($request->post_id);
        //dd($tweet);
        $tweetEntity = new Tweet();
        $tweetEntity->post_id = $tweet->id_str;
        $text = clearPlainUrlsInText($tweet->text);
        $text = formatTweetHashtags($text);
        $text = formatTweetUserMentions($text);
        $tweetEntity->text = $text;
        $tweetEntity->raw_message = $tweet->text;
        $tweetEntity->source = $tweet->source;
        $tweetEntity->posted_from_id = $tweet->user->id;
        $tweetEntity->published_here = false;
        $tweetEntity->social_created_at = strtotime($tweet->created_at);
        $tweetEntity->user_id = auth()->user()->id;
        $tweetEntity->save();

        if(property_exists($tweet->entities,'hashtags')){
            foreach ($tweet->entities->hashtags as $hashtag){
                $entity = new TweetHashtag();
                $entity->text = $hashtag->text;
                $tweetEntity->tweetHashtags()->save($entity);
            }
        }

        if(property_exists($tweet->entities,'media')){
            foreach ($tweet->entities->media as $media){
                $entity = new TweetMedia();
                $entity->media_id = $media->id;
                $entity->media_url = $media->media_url;
                $entity->url = $media->url;
                $entity->type = $media->type;
                $tweetEntity->tweetMedias()->save($entity);
            }
        }

        if(property_exists($tweet->entities,'urls')){
            foreach ($tweet->entities->urls as $url){
                $entity = new TweetUrl();
                $entity->url = $url->url;
                $entity->expanded_url = $url->expanded_url;
                $entity->display_url = $url->display_url;
                $tweetEntity->tweetUrls()->save($entity);
            }
        }

        $user = auth()->user();

        DB::table('favourite_posts')->insert(
            ['user_id' => auth()->user()->id, 'favouritepost_id' => $tweetEntity->id, 'favouritepost_type' => 'App\Models\Tweet']);

        $data['status'] = '1';
        $data['state'] = 'saved_favourite';
        return $data;
    }

    public function deleteFromFavouriteForUser(Request $request)
    {
        $user = auth()->user();
        $data = [
            'status' => '',
            'error' => ''
        ];

        $postId = $request->post_id;
        $post = Tweet::where('post_id',$postId)->first();
        $post->tweetHashtags()->delete();
        $post->tweetMedias()->delete();
        $post->tweetUrls()->delete();
        DB::table('favourite_posts')->where('user_id',$user->id)
            ->where('favouritepost_id',$post->id)
            ->where('favouritepost_type','App\Models\Tweet')
            ->delete();
        $post->delete();
        $data['status'] = '1';
        $data['state'] = 'notsaved_notfavourite';
        return $data;
    }

    protected function checkStateForUser($postId)
    {
        $user = auth()->user();
        $post = Tweet::where('post_id',$postId)->first();
        $favPost = null;
        if($post) {
            $favPost = DB::table('favourite_posts')->where('user_id', $user->id)
                ->where('favouritepost_id', $post->id)
                ->where('favouritepost_type', 'App\Models\Tweet')
                ->first();
        }
        if(!$favPost)
            return 'notsaved_notfavourite';
        else
            return 'saved_favourite';
    }

    protected function setPostStateForUser($post){
        $post->state = $this->checkStateForUser($post->id);
        return $post;
    }

    protected function addPropertiesForUser($posts){
        foreach($posts as $post){
            $post = $this->setPostStateForUser($post);
            $post = $this->setPostLocaltime($post);
            $formated_text = formatUrlsInText($post->full_text);
            $formated_text = formatTweetHashtags($formated_text);
            $formated_text = formatTweetUserMentions($formated_text);
            $post->formatted_text = $formated_text;
        }
        return $posts;
    }

    /* favourite posts methods */
    public function addToUserStream(Request $request){
        $data = ['status' =>'',
            'msg' =>''];
        $user = auth()->user();
        $postId = $request->input('postId');
        $post = $user->streamTwPosts()->where('streampost_id',$postId)->first();
        if(!$post){
            $streamPost = Tweet::where('id',$postId)->first();
            $user->streamTwPosts()->attach($streamPost);
            $data['status'] = 'success';
            $data['msg'] = 'success';
        }
        else{
            $data['status'] = 'failed';
            $data['msg'] = 'post already exists';
        }
        return $data;
    }

    public function deleteFromUserStream(Request $request){
        $data = ['status' =>'',
            'msg' =>''];
        $user = auth()->user();
        $postId = $request->input('postId');
        $post = $user->streamTwPosts()->where('streampost_id',$postId)->first();
        if($post){
            $streamPost = Tweet::where('id',$postId)->first();
            $user->streamTwPosts()->detach($streamPost);
            $data['status'] = 'success';
            $data['msg'] = 'success';
        }
        else{
            $data['status'] = 'failed';
            $data['msg'] = 'post doesn`t exist';
        }
        return $data;
    }
}
