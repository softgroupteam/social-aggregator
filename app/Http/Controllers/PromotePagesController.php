<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\FacebookPromoteKeyword;
use App\Models\PublicFacebookPage;
use GuzzleHttp\Client;


class PromotePagesController extends Controller
{
    protected $statusData = [
        'status' => '',
        'errors' => [],
        'success_msg' => ''];


    public function storeKeywords(Request $request){
        $user = auth()->user();
        $user->promoteFbKeywords()->delete();
        $user->publicFbPages()->delete();
        $keywords = ($request->has('keyword'))?array_values($request->input('keyword')):[];
        $keywords = array_filter($keywords,'strlen');
        $keywordObjects = array_map(function($item){return new FacebookPromoteKeyword(['name' => $item]);},$keywords);
        $user->promoteFbKeywords()->saveMany($keywordObjects);
        $this->statusData['status']= 'success';
        $this->statusData['success_msg'] = 'success';
        return $this->statusData;
    }

    public function getPromotePages(){
        $user = auth()->user();
        $client = new Client();
        $fbUser = $user->socialLogins()->where('user_id',$user->id)
            ->where('provider','facebook')
            ->first();
        $keywords = $user->promoteFbKeywords()->get();
        $searchStr = '';
        if(count($keywords) == 0){
            return ['data' => []];
        }
        else{
            foreach($keywords as $key => $item){
                $searchStr.= $item->name.(($key==(count($keywords)-1))?'':'/');
            }
        }
        $url = 'https://graph.facebook.com/search';
        $fields = 'id,name,fan_count,talking_about_count,can_post';
        $pages = $client->request('GET', $url, [
                'query' => ['access_token' => $fbUser->token,'q' => $searchStr,'type' => 'page','fields' => $fields,'limit' => '1000']
            ]
        );
        $pages = $this->preparePagesResponse(json_decode($pages->getBody()->getContents()));
        return $pages;
    }

    protected function preparePagesResponse($pages)
    {
        $result = ['data' => []];
        foreach ($pages->data as $page){
            if($page->can_post == true) {
                $item = [];
                $item['id'] = $page->id;
                $item['name'] = $page->name;
                $item['likes'] = $page->fan_count;
                $item['talkingAbout'] = $page->talking_about_count;
                $item['promoted'] = PublicFacebookPage::where('social_id', $page->id)->first() != null;
                $result['data'][] = $item;
            }
        }
        return $result;
    }

    protected function storePromotePage(Request $request)
    {
        $user = auth()->user();
        $fbUser = $user->socialLogins()->where('user_id',auth()->user()->id)
            ->where('provider','facebook')
            ->first();
        $page = new PublicFacebookPage();
        $page->user_id = $user->id;
        $page->social_id = $request->input('id');
        $page->name = $request->input('name');
        $page->fan_count = $request->input('likes');
        $page->talking_about = $request->input('talkingAbout');
        $page->token = $fbUser->token;
        $page->save();
    }

    protected function deletePromotePage(Request $request)
    {
        $page = PublicFacebookPage::where('social_id', $request->input('id'))
                                    ->where('user_id', auth()->user()->id)
                                    ->first();
        if($page != null){
            $page->delete();
        }
    }

}
