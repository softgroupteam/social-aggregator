<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Intervention\Image\Exception\ImageException;
use Storage;
use File;

class ProfileController extends Controller
{
    public function getRandomUsers()
    {
        $limit = 10;
        $user = auth()->user();
        $followees = $user->following()->allRelatedIds()->toArray();
        $followees[] = $user->id;
        $usersForFollow = User::whereNotIn('id',$followees)->inRandomOrder()->limit($limit)->get();
        $data = [
            'status' => 1,
            'users' => $usersForFollow
        ];
        echo json_encode($data);
    }

    public function changeUserAvatar(Request $request)
    {
        $mockAvatar = 'public/user_avatars/mock_user_avatar_small.png';
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => ''
        ];
        $user = auth()->user();
        $image = $request->file('image');
        try {
            $resize = Image::make($image->getPathname())->resize(100, 100)->encode('jpg');
        }
        catch(ImageException $ex){
            $data['status'] = 'failed';
            $data['errors'][] = 'Wrong file format';
            return $data;
        }
        if($user->userInfo->avatar != $mockAvatar && $resize){
            $this->deleteUserAvatar();
        }
        $hash = md5($resize->__toString());
        $path = "public/user_avatars/{$hash}.jpg";
        $saved = Storage::put($path,$resize->__toString());
        $user->userInfo->avatar = $path;
        $user->userInfo->save();
        $data['avatarPath'] = Storage::url($path);
        $data['status'] = 'success';
        $data['success_msg'] = 'Avatar has been changed';
        return $data;
    }

    public function deleteUserAvatar()
    {
        $mockAvatar = 'public/user_avatars/mock_user_avatar_small.png';
        $user = auth()->user();
        Storage::delete($user->userInfo->avatar);
        $user->userInfo->avatar = $mockAvatar;
        $user->userInfo->save();
    }
}
