<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Hash;
use App\User;
use Storage;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function updateUser(Request $request)
    {
        $user = auth()->user();
        $validationData = $this->validateUserData($request, $user);
        if($validationData['status'] == 'failed')
            return $validationData;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();
        $userInfo = $user->userInfo()->first();
        $userInfo->user_occupation_id = $request->input('occupation');
        $userInfo->interest = $request->input('interest');
        $userInfo->save();
        return $validationData;
    }

    protected function validateUserData($request, $user)
    {
        $uniqueCheck = ($user->email == $request->input('email'))?'':'|unique:users';
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => ''
        ];
        $validator = Validator::make([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
        ], [
            'name' => 'required',
            'email' => 'required|email'.$uniqueCheck,
        ]);
        if ($validator->fails()) {
            foreach($validator->errors()->all() as $message){
                $data['errors'][] = $message;
            }
            $data['status'] = 'failed';
        }
        else {
            $data['status'] = 'success';
            $data['success_msg'] = 'success';
        }
        return $data;
    }

    public function updateUserPassword(Request $request){
        $user = auth()->user();
        $validationData = $this->validateUserPasswordData($request, $user);
        if($validationData['status'] == 'failed')
            return $validationData;
        $user->password = Hash::make($request->input('new-password'));
        $user->save();
        return $validationData;
    }

    protected function validateUserPasswordData($request, $user)
    {
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => ''
        ];
        if(!Hash::check($request->input('old-password'), $user->password))
            $data['errors'][] = 'Old password doesn`t match';
        if($request->input('new-password') != $request->input('confirm-new-password'))
            $data['errors'][] = 'Password confirmation must match previous field';
        if(strlen($request->input('new-password')) < 6)
            $data['errors'][] = 'New password should contain 6 or more symbols';
        if(count($data['errors']) > 0){
            $data['status'] = 'failed';
        }
        else{
            $data['status'] = 'success';
            $data['success_msg'] = 'success';
        }
        return $data;
    }

    public function followUser(Request $request)
    {
        $user = auth()->user();
        $followee_id = $request->input('user_id');
        $user->following()->attach($followee_id);
        $data = [
            'status' => 'success',
            'user_id' => $followee_id,
            'actions' => ['follow' => 'false','unfollow' => 'true']
        ];
        echo json_encode($data);
    }

    public function unfollowUser(Request $request)
    {
        $user = auth()->user();
        $followee_id = $request->input('user_id');
        $user->following()->detach($followee_id);
        $data = [
            'status' => 'success',
            'user_id' => $followee_id,
            'actions' => ['follow' => 'true','unfollow' => 'false']
        ];
        echo json_encode($data);
    }

    public function getRandomUsers()
    {
        $limit = 10;
        $user = auth()->user();
        $followees = $user->following()->allRelatedIds()->toArray();
        $followees[] = $user->id;
        $usersForFollow = User::whereNotIn('id',$followees)->inRandomOrder()->limit($limit)->get();
        foreach ($usersForFollow as $user){
            $this->setUserAvatarUrl($user);
        }
        $data = [
            'status' => 1,
            'users' => $usersForFollow
        ];
        echo json_encode($data);
    }


    public function getFollowers()
    {
        $user = auth()->user();
        $followers = $user->followers()->get();
        foreach ($followers as $follower){
            $follower = $this->setFollowerActions($follower);
            $follower = $this->setUserAvatarUrl($follower);
        }
        $data = [
            'status' => 1,
            'users' => $followers
        ];
        echo json_encode($data);
    }

    public function getFollowedUsers()
    {
        $user = auth()->user();
        $followees = $user->following()->get();
        foreach ($followees as $followed){
            $followed = $this->setFollowerActions($followed);
            $followed = $this->setUserAvatarUrl($followed);
        }
        $data = [
            'status' => 1,
            'users' => $followees
        ];
        echo json_encode($data);
    }

    public function getFollowersByUser(Request $request)
    {
        $user = User::where('id', $request->input('userId'))->first();
        $followers = $user->followers()->get();
        foreach ($followers as $follower) {
            $follower = $this->setUserAvatarUrl($follower);
        }
        $data = [
            'status' => 1,
            'users' => $followers
        ];
        echo json_encode($data);
    }

    public function getFollowedUsersByUSer(Request $request)
    {
        $user = User::where('id', $request->input('userId'))->first();
        $followees = $user->following()->get();
        foreach ($followees as $followed) {
            $followed = $this->setUserAvatarUrl($followed);
        }
        $data = [
            'status' => 1,
            'users' => $followees
        ];
        echo json_encode($data);
    }

    protected function setFollowerActions($follower){
        $actions = ['follow' => '',
                    'unfollow' => ''];
        $user = auth()->user();
        $followedUser = $user->following()->where('users.id',$follower->id)->first();
        if($followedUser){
            $actions['follow'] = 'false';
            $actions['unfollow'] = 'true';
        }
        else{
            $actions['follow'] = 'true';
            $actions['unfollow'] = 'false';
        }
        $follower->actions = $actions;
        return $follower;
    }

    protected function setFollowedUserActions($followed){
        $actions = ['follow' => '',
            'unfollow' => ''];
        $user = auth()->user();
        $followedUser = $user->following()->where('users.id',$followed->id)->first();
        if($followedUser){
            $actions['follow'] = 'false';
            $actions['unfollow'] = 'true';
        }
        else{
            $actions['follow'] = 'true';
            $actions['unfollow'] = 'false';
        }
        $followed->actions = $actions;
        return $followed;
    }

    protected function setUserAvatarUrl($user){
        $user->avatarUrl = Storage::url($user->userInfo->avatar);
        return $user;
    }
}
