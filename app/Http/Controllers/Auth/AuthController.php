<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use Auth;
use App\User;
use App\SocialLogin;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Models\SocialFacebookPage;
use App\Models\SocialFacebookPagePerm;
use App\Models\UserOccupation;

class AuthController extends Controller
{

    protected $scopes;

    public function __construct()
    {
        $googleScopes = ['https://www.googleapis.com/auth/plus.me',
                         'https://www.googleapis.com/auth/plus.login',
                         'https://www.googleapis.com/auth/plus.profile.emails.read',
                         'https://www.googleapis.com/auth/plus.stream.write'];
        $facebookScopes = ['email','user_posts','publish_actions','manage_pages','pages_show_list','publish_pages'];
        $twitterScopes = ['read','read-write'];
        $this->scopes = ['facebook' => $facebookScopes,'twitter' => $twitterScopes,'google' => $googleScopes];
    }

    public function redirectToProvider($provider)
    {
        if($provider == 'twitter')
        {
            return Socialite::driver($provider)->redirect();
        }
        return Socialite::driver($provider)->scopes($this->scopes[$provider])->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $socialUser = Socialite::driver($provider)->user();
        if($provider == 'facebook'){
            $client = new Client();
            $url = 'https://graph.facebook.com/me/accounts';
            $socialFbPages = $client->request('GET', $url,[
                    'query' => ['access_token' => $socialUser->token]
                ]
            );
            $socialFbPages = json_decode($socialFbPages->getBody()->getContents());
            if($socialFbPages) {
                $socialFbPages->userId = $socialUser->id;
            }
            $user = User::where('id',auth()->user()->id)->with(['userInfo','userInfo.userOccupation'])->first();
            $followersCount = $user->followers()->count();
            $followingCount = $user->following()->count();
            $occupations = UserOccupation::get();
            return view('backend.profile',compact(['user','occupations','followersCount','followingCount','socialUser','socialFbPages']));
        }

        if(!Auth::check()) {
            $authUser = $this->findSocialUser($socialUser, $provider);

            if($authUser) {
                Auth::login($authUser);
            }
            return redirect('admin/dashboard/profile');
        }
        else
        {
            $this->createSocialLogin($socialUser,$provider);
            return redirect('admin/dashboard/profile');
        }
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findSocialUser($user, $provider)
    {
        $authUser = SocialLogin::where('provider_id', $user->id)->first();
        if ($authUser) {
            $authUser->token = $user->token;
            $authUser->save();
            return $authUser->user()->first();
        }
        else
        {
            return null;
        }
    }




    public function createSocialLogin($socialUser,$provider)
    {
        $existingUser = SocialLogin::where('provider_id', $socialUser->id)->first();
        if(!$existingUser) {
            $user = Auth::user();
            $socialLogin = new SocialLogin();
            $socialLogin->provider = $provider;
            $socialLogin->provider_id = $socialUser->getId();
            $socialLogin->user_id = $user->id;
            $socialLogin->name = $socialUser->getName();
            $socialLogin->nickname = $socialUser->getNickname();
            $socialLogin->email = $socialUser->getEmail();
            $socialLogin->token = $socialUser->token;
            if(property_exists($socialUser,'tokenSecret'))
                $socialLogin->token_secret = $socialUser->tokenSecret;
            else
                $socialLogin->token_secret = null;
            $socialLogin->avatar = null;
            $socialLogin->save();
        }
    }


    public function linkFacebookAccNPages(Request $request){
        $data = [
            'status' => '',
            'errors' => [],
            'success_msg' => 'Success'
        ];
        $fbUser = ($request->has('fb_user'))?json_decode($request->input('fb_user')):'';
        $fbPages = [];
        if($request->has('fb_pages')) {
            $tempArr = array_values($request->input('fb_pages'));
            foreach ($tempArr as $jsonPage){
                $fbPages[] = json_decode($jsonPage);
            }
        }
        $user = Auth::user();
        if($fbUser != '') {
            $authUser = SocialLogin::where('provider_id', $fbUser->id)->first();
            if(!$authUser){
                $authUser = new SocialLogin();
                $authUser->provider = 'facebook';
                $authUser->provider_id = $fbUser->id;
                $authUser->user_id = $user->id;
                $authUser->name = $fbUser->name;
                $authUser->nickname = $fbUser->nickname;
                $authUser->email = $fbUser->email;
                $authUser->token = $fbUser->token;
                if(property_exists($fbUser,'tokenSecret'))
                    $authUser->token_secret = $fbUser->tokenSecret;
                else
                    $authUser->token_secret = null;
                $authUser->avatar = null;
                $authUser->save();
            }
        }
        foreach ($fbPages as $page){
                $socialFbPage = new SocialFacebookPage();
                $socialFbPage->fb_page_id = $page->id;
                $socialFbPage->name = $page->name;
                $socialFbPage->category = $page->category;
                $socialFbPage->user_id = $user->id;
                $socialFbPage->token = $page->access_token;
                $socialFbPage->save();
        }
        $data['status'] = 'success';
        return $data;
    }



}
