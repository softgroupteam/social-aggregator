<?php

namespace App\Http\Controllers;

use App\SocialLogin;
use Illuminate\Http\Request;
use App\User;
use App\Models\UserOccupation;
use App\Models\SocialStreamCategory;
use App\Models\GooglePost;
use App\Models\FacebookPost;
use App\Models\Tweet;
use App\Models\YoutubeVideo;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('backend.dashboard');
    }

    public function promotePagesView()
    {
        $user = auth()->user();
        $fbUser = $user->socialLogins()->where('user_id',$user->id)
            ->where('provider','facebook')
            ->first();
        $keywords = $user->promoteFbKeywords()->get();
        return view('backend.facebook-promote-pages',compact(['keywords','fbUser']));
    }

    public function getComposerView()
    {
        $postType = 'image';
        return view('backend.composer',compact(['postType']));
    }

    public function loadPostToComposer(Request $request)
    {
        $postId = $request->post_id;
        $socialType = $request->social_type;
        $editingPost = '';
        switch($socialType){
            case 'facebook':
                $editingPost = $this->getFbPostForComposer($postId);
                break;
            case 'twitter':
                $editingPost = $this->getTwPostForComposer($postId);
                break;
            case 'youtube':
                $editingPost = $this->getYtPostForComposer($postId);
                break;
            case 'google':
                $editingPost = $this->getGgPostForComposer($postId);
                break;
        }
        $message = $editingPost['message'];
        $imageUrlFromSocial = $editingPost['imageUrl'];
        $originalLink = $editingPost['originalLink'];
        $postType = $editingPost['postType'];
        $videoLink = $editingPost['videoLink'];
        $embedVideoHtml = $editingPost['embedVideoHtml'];
        return view('backend.composer',compact(['message','imageUrlFromSocial','originalLink','postType','videoLink','embedVideoHtml']));
    }

    private function getFbPostForComposer($id)
    {
        $resultPost = [];
        $post = FacebookPost::where('id',$id)->with('facebookPostAttachments')->first();
        $attachment = $post->facebookPostAttachments->first();
        $resultPost['imageUrl'] = $attachment->image;
        $resultPost['message'] = $post->message;
        $resultPost['originalLink'] = $post->permalink;
        $resultPost['postType'] = ($attachment->embed_video_html != '')?'video':'image';
        $resultPost['videoLink'] = ($attachment->embed_video_html != '' && $attachment->url != '')? $attachment->url:'';
        $resultPost['embedVideoHtml'] = ($attachment->embed_video_html != '')? $attachment->embed_video_html:'';
        return $resultPost;
    }

    private function getTwPostForComposer($id)
    {
        $resultPost = [];
        $post = Tweet::where('id',$id)->with('tweetMedias')->first();
        $attachment = $post->tweetMedias->first();
        $resultPost['imageUrl'] = $attachment->media_url;
        $resultPost['message'] = $post->raw_message;
        $resultPost['originalLink'] = 'https://twitter.com/'.$post->posted_from_id.'/status/'.$post->post_id;
        $resultPost['postType'] = 'image';
        $resultPost['videoLink'] = '';
        $resultPost['embedVideoHtml'] = '';
        return $resultPost;
    }

    private function getYtPostForComposer($id)
    {
        $resultPost = [];
        $post = YoutubeVideo::where('id',$id)->first();
        $resultPost['imageUrl'] = $post->thumbnail_standart;
        $resultPost['message'] = $post->title;
        $resultPost['originalLink'] = 'https://www.youtube.com/watch?v='.$post->video_id;
        $resultPost['postType'] = 'video';
        $resultPost['videoLink'] = 'https://www.youtube.com/watch?v='.$post->video_id;
        $resultPost['embedVideoHtml'] = $post->embed_html;
        return $resultPost;
    }

    private function getGgPostForComposer($id)
    {
        $resultPost = [];
        $post = GooglePost::where('id',$id)->first();
        $attachment = $post->googlePostAttachments()->first();
        $resultPost['imageUrl'] = $attachment->image_url;
        $resultPost['message'] = $post->content;
        $resultPost['originalLink'] = $post->url;
        $resultPost['postType'] = 'image';
        $resultPost['videoLink'] = '';
        $resultPost['embedVideoHtml'] = '';
        return $resultPost;
    }

    public function getSavedPostsView()
    {
        return view('backend.saved-posts');
    }

    public function getProfile()
    {
        $user = User::where('id',auth()->user()->id)->with(['userInfo','userInfo.userOccupation'])->first();
        $followersCount = $user->followers()->count();
        $followingCount = $user->following()->count();
        $occupations = UserOccupation::get();
        return view('backend.profile',compact(['user','occupations','followersCount','followingCount']));
    }

    public function getProfileByUser($id)
    {
        $user = User::where('id',$id)->first();
        $followersCount = $user->followers()->count();
        $followingCount = $user->following()->count();
        $occupations = UserOccupation::get();
        return view('backend.profile-by-user',compact(['user','occupations','followersCount','followingCount']));
    }

    function getSocialStreamsPage()
    {
        $fbUser = SocialLogin::where('user_id',auth()->user()->id)
                                ->where('provider','facebook')
                                ->first();
        $streamCategories = SocialStreamCategory::get();
        return view('backend.social-streams',compact(['streamCategories','fbUser']));
    }
}
