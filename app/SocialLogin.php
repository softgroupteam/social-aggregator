<?php

namespace App;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;


class SocialLogin extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'social_logins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'provider', 'provider_id', 'token', 'avatar'];

    public static function getAccounts($provider)
    {
        if($user = auth()->user()){
           return DB::table('social_logins')
                ->select(['id','provider_id','email','name'])
                ->where('user_id',$user->id)
               ->where('provider',$provider)
                ->get();
        }
        return null;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scheduledPosts()
    {
        return $this->morphToMany('App\Models\ScheduledPost', 'destination', 'scheduled_posts_destination');
    }
}
