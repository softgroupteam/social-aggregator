<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GooglePlusChannel extends Model
{
    public static function getChannels()
    {
        if($user = auth()->user()) {
            return DB::table('google_plus_channels')->where('user_id', $user->id)->get();
        }
        else
            return null;
    }

    public function category()
    {
        return $this->belongsTo('App\Models\SocialStreamCategory','category_id');
    }
}
