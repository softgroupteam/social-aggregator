<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FacebookPage extends Model
{
    public static function getPages()
    {
        if($user = auth()->user()) {
            return DB::table('facebook_pages')->where('user_id', $user->id)->get();
        }
        else
            return null;
    }

    public function category()
    {
        return $this->belongsTo('App\Models\SocialStreamCategory','category_id');
    }
}
