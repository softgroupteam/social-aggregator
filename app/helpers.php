<?php


if (! function_exists('findinstring'))
{
    function findinstring($str)
    {
        if(strpos( $str, "gif" ) === false && strpos($str, "jpg" ) === false && strpos($str, "png" ) === false && strpos($str, "peg" ) === false && strpos($str, "img" ) === false && strpos($str, "photo" ) === false ) { // substring not found in string
            return 0;
        } else { // substring found in string
            return 1;
        }
    }
}


if (! function_exists('formatUrlsInText'))
{
    /**
    * Helper to transform plain urls to html tags in text .
    *
    * @return mixed
    */
    function formatUrlsInText($text)
    {
        $reg_exUrl = "/((http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?)|((www)\.[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?)/";
        preg_match_all($reg_exUrl, $text, $matches);
        $usedPatterns = array();

        foreach($matches[0] as $pattern){
            //twitter url dot fix
            if($pattern[strlen($pattern)-1] === '.')
                $pattern = substr($pattern, 0, -1);
            if(!array_key_exists($pattern, $usedPatterns)){
                $usedPatterns[$pattern]=true;
                // now try to catch last thing in text
                $checkifimage = findinstring($pattern);
                if($checkifimage==1){
                    $text = str_replace($pattern, '<img src="'.((strpos($pattern, 'www') === 0)?'http://'.$pattern:$pattern).'">', $text);
                } else {
                    $text = str_replace($pattern, '<a href="'.((strpos($pattern, 'www') === 0)?'http://'.$pattern:$pattern).'">'.$pattern.'</a>', $text);
                }
            }
        }
        $text = nl2br($text);
        return $text;
    }
}

if (! function_exists('clearPlainUrlsInText'))
{
    /**
     * Helper to transform plain urls to html tags in text .
     *
     * @return mixed
     */
    function clearPlainUrlsInText($text)
    {
        $reg_exUrl = "/((http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?)|((www)\.[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?)/";
        preg_match_all($reg_exUrl, $text, $matches);
        $usedPatterns = array();

        foreach($matches[0] as $pattern){
            if(!array_key_exists($pattern, $usedPatterns)){
                $usedPatterns[$pattern]=true;
                $text = str_replace($pattern,'', $text);
            }
        }
        $text = nl2br($text);
        return $text;
    }
}

if (! function_exists('formatTweetHashtags'))
{
    /**
     * Helper to transform plain urls to html tags in text .
     *
     * @return mixed
     */
    function formatTweetHashtags($text)
    {
        $reg_ex = "/[#]+[A-Za-z0-9-_]+/";
        preg_match_all($reg_ex, $text, $matches);
        $usedPatterns = array();

        foreach($matches[0] as $pattern){
            if(!array_key_exists($pattern, $usedPatterns)){
                $usedPatterns[$pattern]=true;
                $text = str_replace($pattern, '<a href="https://twitter.com/hashtag/'.substr($pattern, 1).'?src=hash">'.substr($pattern, 1).'</a>', $text);
            }
        }
        return $text;
    }
}

if (! function_exists('formatTweetUserMentions'))
{
    /**
     * Helper to transform plain urls to html tags in text .
     *
     * @return mixed
     */
    function formatTweetUserMentions($text)
    {
        $reg_ex = "/[@]+[A-Za-z0-9-_]+/";
        preg_match_all($reg_ex, $text, $matches);
        $usedPatterns = array();

        foreach($matches[0] as $pattern){
            if(!array_key_exists($pattern, $usedPatterns)){
                $usedPatterns[$pattern]=true;
                $text = str_replace($pattern, '<a href="https://twitter.com/'.substr($pattern, 1).'">'.substr($pattern, 1).'</a>', $text);
            }
        }
        return $text;
    }
}