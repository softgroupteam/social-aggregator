<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYoutubeVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youtube_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('video_id');
            $table->string('title');
            $table->text('description');
            $table->text('raw_message');
            $table->string('thumbnail_standart');
            $table->string('channel_id');
            $table->string('social_category');
            $table->integer('social_created_at');
            $table->string('embed_html');
            $table->boolean('published_here');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('youtube_videos');
    }
}
