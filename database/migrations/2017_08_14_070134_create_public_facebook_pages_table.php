<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicFacebookPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public_facebook_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('social_id');
            $table->string('name');
            $table->string('fan_count');
            $table->string('talking_about');
            $table->string('token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('public_facebook_pages');
    }
}
