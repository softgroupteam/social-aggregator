<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGooglePostAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_post_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('post_id');
            $table->string('type');
            $table->text('display_name');
            $table->text('content');
            $table->text('url');
            $table->text('image_url')->nullable();
            $table->text('video_embed')->nullable();
            $table->text('video_embed_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_post_attachments');
    }
}
