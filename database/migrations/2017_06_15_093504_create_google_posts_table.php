<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGooglePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('post_id');
            $table->text('title');
            $table->text('content');
            $table->text('raw_message');
            $table->string('actor_id');
            $table->text('url');
            $table->string('verb');
            $table->string('type');
            $table->integer('social_created_at');
            $table->string('published_here');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_posts');
    }
}
