<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacebookPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facebook_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('post_id');
            $table->string('link');
            $table->text('message');
            $table->text('raw_message');
            $table->text('fullpicture');
            $table->string('permalink');
            $table->text('source')->nullable();
            $table->string('admin_creator')->nullable();
            $table->string('posted_from_id');
            $table->integer('social_created_time');
            $table->integer('social_updated_time');
            $table->string('type');
            $table->boolean('published_here');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facebook_posts');
    }
}
