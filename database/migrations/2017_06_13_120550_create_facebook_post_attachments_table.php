<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacebookPostAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facebook_post_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('social_id');
            $table->text('image');
            $table->string('type');
            $table->text('url');
            $table->text('embed_video_html');
            $table->integer('facebook_post_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facebook_post_attachments');
    }
}
