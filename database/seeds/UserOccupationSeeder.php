<?php

use Illuminate\Database\Seeder;
use App\Models\UserOccupation;

class UserOccupationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Truncating User occupations');
        $this->truncateTable();
        $this->command->info('Seeding user_ocupations table');
        $occupations = ['Restaurant Owner','Caterer','Marketer','Agency','Other'];
        foreach ($occupations as $occupation){
            $obj = new UserOccupation();
            $obj->name = $occupation;
            $obj->save();
        }
    }
    public function truncateTable()
    {
        DB::table('user_occupations')->truncate();
        \App\Models\UserOccupation::truncate();
    }
}
