<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = \App\Models\Role::create([
            'name' => 'superadministrator',
            'display_name' => 'Superadministrator',
            'description' => 'Superadministrator'
        ]);
        $role = \App\Models\Role::create([
            'name' => 'administrator',
            'display_name' => 'Administrator',
            'description' => 'Administrator'
        ]);
        $role = \App\Models\Role::create([
            'name' => 'user',
            'display_name' => 'User',
            'description' => 'User'
        ]);
    }
}
