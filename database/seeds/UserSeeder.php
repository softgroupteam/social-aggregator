<?php

use Illuminate\Database\Seeder;
use App\Models\UserInfo;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@admin.admin',
            'password' => bcrypt('111111'),
        ]);
        $user->attachRole('superadministrator');
        $userInfo = new UserInfo();
        $userInfo->city = 'Rivne';
        $userInfo->country_code = 'UA';
        $userInfo->country_name = 'Ukraine';
        $userInfo->timezone = 'Europe/Kiev';
        $userInfo->interest = 'test';
        $userInfo->user_occupation_id = '1';
        $userInfo->avatar = 'public/user_avatars/mock_user_avatar_small.png';
        $userInfo->user_id = $user->id;
        $userInfo->save();

        //brijesh user
        $user = User::create([
            'name' => 'Brijesh',
            'email' => 'Brijeshvnair@gmail.com',
            'password' => bcrypt('Nair1710'),
        ]);
        $user->attachRole('superadministrator');
        $userInfo = new UserInfo();
        $userInfo->city = 'Sydney';
        $userInfo->country_code = 'AU';
        $userInfo->country_name = 'Australia';
        $userInfo->timezone = '	Australia/Sydney';
        $userInfo->interest = 'test';
        $userInfo->user_occupation_id = '1';
        $userInfo->avatar = 'public/user_avatars/mock_user_avatar_small.png';
        $userInfo->user_id = $user->id;
        $userInfo->save();
    }
}
