(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://localhost',
            routes : [{"host":null,"methods":["GET","HEAD"],"uri":"api\/user","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"login","name":"login","action":"App\Http\Controllers\Auth\LoginController@showLoginForm"},{"host":null,"methods":["POST"],"uri":"login","name":null,"action":"App\Http\Controllers\Auth\LoginController@login"},{"host":null,"methods":["POST"],"uri":"logout","name":"logout","action":"App\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"register","name":"register","action":"App\Http\Controllers\Auth\RegisterController@getRegister"},{"host":null,"methods":["POST"],"uri":"register","name":null,"action":"App\Http\Controllers\Auth\RegisterController@register"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset","name":"password.request","action":"App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm"},{"host":null,"methods":["POST"],"uri":"password\/email","name":"password.email","action":"App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset\/{token}","name":"password.reset","action":"App\Http\Controllers\Auth\ResetPasswordController@showResetForm"},{"host":null,"methods":["POST"],"uri":"password\/reset","name":null,"action":"App\Http\Controllers\Auth\ResetPasswordController@reset"},{"host":null,"methods":["GET","HEAD"],"uri":"home","name":"home","action":"App\Http\Controllers\HomeController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"logout","name":null,"action":"App\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"auth\/{provider}","name":"social.auth","action":"App\Http\Controllers\Auth\AuthController@redirectToProvider"},{"host":null,"methods":["GET","HEAD"],"uri":"auth\/{provider}\/callback","name":null,"action":"App\Http\Controllers\Auth\AuthController@handleProviderCallback"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard","name":"dashboard","action":"App\Http\Controllers\DashboardController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/promote-pages","name":"promote-pages","action":"App\Http\Controllers\DashboardController@promotePagesView"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/facebook-page-posts\/{pageTitle}","name":"get-posts-by-page","action":"App\Http\Controllers\Facebook\PostsController@getPagePosts"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/youtube-channel-feed\/{channelId}","name":"get-feed-by-youtubeChannel","action":"App\Http\Controllers\Youtube\VideosController@getVideosByChannel"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/twitter-user-tweets\/{userName}","name":"get-tweets-by-user","action":"App\Http\Controllers\Twitter\TweetsController@getTweetsByUser"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/google-plus","name":"google-plus","action":"App\Http\Controllers\Googleplus\PostsController@getGooglePosts"},{"host":null,"methods":["POST"],"uri":"admin\/get-google-plus","name":"get-google-plus","action":"App\Http\Controllers\Googleplus\PostsController@getGooglePostsAjax"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/composer","name":"composer","action":"App\Http\Controllers\DashboardController@getComposerView"},{"host":null,"methods":["POST"],"uri":"admin\/dashboard\/composer\/load-post","name":"composer-load-post","action":"App\Http\Controllers\DashboardController@loadPostToComposer"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/saved-posts","name":"get-saved-posts-dashboard","action":"App\Http\Controllers\DashboardController@getSavedPostsView"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/profile","name":"profile","action":"App\Http\Controllers\DashboardController@getProfile"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/profile\/{id}","name":"profile-by-user","action":"App\Http\Controllers\DashboardController@getProfileByUser"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/social-streams","name":"social-streams-page","action":"App\Http\Controllers\DashboardController@getSocialStreamsPage"},{"host":null,"methods":["POST"],"uri":"modal\/add-fbpage","name":"modal.add-fbpage","action":"App\Http\Controllers\ModalController@addFacebookPage"},{"host":null,"methods":["POST"],"uri":"modal\/add-ytchannel","name":"modal.add-ytchannel","action":"App\Http\Controllers\ModalController@addYoutubeChannel"},{"host":null,"methods":["POST"],"uri":"modal\/add-twpage","name":"modal.add-twpage","action":"App\Http\Controllers\ModalController@addTwitterPage"},{"host":null,"methods":["POST"],"uri":"modal\/delete-socialtab","name":"modal.delete-socialtab","action":"App\Http\Controllers\ModalController@deleteSocialTab"},{"host":null,"methods":["POST"],"uri":"modal\/delete-socialacc","name":"modal.delete-socialacc","action":"App\Http\Controllers\ModalController@deleteSocialAcc"},{"host":null,"methods":["POST"],"uri":"modal\/delete-socialfbpage","name":"modal.delete-socialfbpage","action":"App\Http\Controllers\ModalController@deleteSocialFbPage"},{"host":null,"methods":["POST"],"uri":"modal\/add-gpchannel","name":"modal.add-gpchannel","action":"App\Http\Controllers\ModalController@addGooglePlusChannel"},{"host":null,"methods":["POST"],"uri":"modal\/add-category","name":"modal.add-category","action":"App\Http\Controllers\ModalController@addCategory"},{"host":null,"methods":["GET","HEAD"],"uri":"modal\/get-categories","name":"modal.get-categories","action":"App\Http\Controllers\ModalController@getCategories"},{"host":null,"methods":["POST"],"uri":"ajax\/facebook-user-posts","name":"ajax.get-posts-by-fbuser","action":"App\Http\Controllers\Facebook\PostsController@getPostsByUserAjax"},{"host":null,"methods":["POST"],"uri":"ajax\/facebook-user-feed","name":"ajax.get-feed-by-fbuser","action":"App\Http\Controllers\Facebook\PostsController@getFeedAjax"},{"host":null,"methods":["POST"],"uri":"ajax\/facebook-page-posts","name":"ajax.get-posts-by-page","action":"App\Http\Controllers\Facebook\PostsController@getPagePostsAjax"},{"host":null,"methods":["POST"],"uri":"ajax\/youtube-channel-feed","name":"ajax.get-feed-by-youtubeChannel","action":"App\Http\Controllers\Youtube\VideosController@getVideosByChannelAjax"},{"host":null,"methods":["POST"],"uri":"ajax\/twitter-user-tweets","name":"ajax.get-tweets-by-user","action":"App\Http\Controllers\Twitter\TweetsController@getTweetsByUserAjax"},{"host":null,"methods":["POST"],"uri":"ajax\/facebook-page-posts-foruser","name":"ajax.get-posts-by-page-foruser","action":"App\Http\Controllers\Facebook\PostsController@getPagePostsAjaxForUser"},{"host":null,"methods":["POST"],"uri":"ajax\/youtube-channel-feed-foruser","name":"ajax.get-feed-by-youtubeChannel-foruser","action":"App\Http\Controllers\Youtube\VideosController@getVideosByChannelAjaxForUser"},{"host":null,"methods":["POST"],"uri":"ajax\/twitter-user-tweets-foruser","name":"ajax.get-tweets-by-user-foruser","action":"App\Http\Controllers\Twitter\TweetsController@getTweetsByUserAjaxForUser"},{"host":null,"methods":["POST"],"uri":"ajax\/store-fbpost","name":"ajax.store-fbpost","action":"App\Http\Controllers\Facebook\PostsController@store"},{"host":null,"methods":["POST"],"uri":"ajax\/publish-fbpost","name":"ajax.publish-fbpost","action":"App\Http\Controllers\Facebook\PostsController@publish"},{"host":null,"methods":["POST"],"uri":"ajax\/delete-fbpost","name":"ajax.delete-fbpost","action":"App\Http\Controllers\Facebook\PostsController@delete"},{"host":null,"methods":["POST"],"uri":"ajax\/unpublish-fbpost","name":"ajax.unpublish-fbpost","action":"App\Http\Controllers\Facebook\PostsController@unpublish"},{"host":null,"methods":["POST"],"uri":"ajax\/store-twpost","name":"ajax.store-twpost","action":"App\Http\Controllers\Twitter\TweetsController@store"},{"host":null,"methods":["POST"],"uri":"ajax\/publish-twpost","name":"ajax.publish-twpost","action":"App\Http\Controllers\Twitter\TweetsController@publish"},{"host":null,"methods":["POST"],"uri":"ajax\/delete-twpost","name":"ajax.delete-twpost","action":"App\Http\Controllers\Twitter\TweetsController@delete"},{"host":null,"methods":["POST"],"uri":"ajax\/unpublish-twpost","name":"ajax.unpublish-twpost","action":"App\Http\Controllers\Twitter\TweetsController@unpublish"},{"host":null,"methods":["POST"],"uri":"ajax\/store-ytpost","name":"ajax.store-ytpost","action":"App\Http\Controllers\Youtube\VideosController@store"},{"host":null,"methods":["POST"],"uri":"ajax\/publish-ytpost","name":"ajax.publish-ytpost","action":"App\Http\Controllers\Youtube\VideosController@publish"},{"host":null,"methods":["POST"],"uri":"ajax\/delete-ytpost","name":"ajax.delete-ytpost","action":"App\Http\Controllers\Youtube\VideosController@delete"},{"host":null,"methods":["POST"],"uri":"ajax\/unpublish-ytpost","name":"ajax.unpublish-ytpost","action":"App\Http\Controllers\Youtube\VideosController@unpublish"},{"host":null,"methods":["POST"],"uri":"ajax\/store-gglpost","name":"ajax.store-gglpost","action":"App\Http\Controllers\Googleplus\PostsController@store"},{"host":null,"methods":["POST"],"uri":"ajax\/publish-gglpost","name":"ajax.publish-gglpost","action":"App\Http\Controllers\Googleplus\PostsController@publish"},{"host":null,"methods":["POST"],"uri":"ajax\/delete-gglpost","name":"ajax.delete-gglpost","action":"App\Http\Controllers\Googleplus\PostsController@delete"},{"host":null,"methods":["POST"],"uri":"ajax\/unpublish-gglpost","name":"ajax.unpublish-gglpost","action":"App\Http\Controllers\Googleplus\PostsController@unpublish"},{"host":null,"methods":["POST"],"uri":"ajax\/add-favourite-fbpost","name":"ajax.add-favourite-fbpost","action":"App\Http\Controllers\Facebook\PostsController@addToFavouriteForUser"},{"host":null,"methods":["POST"],"uri":"ajax\/delete-favourite-fbpost","name":"ajax.delete-favourite-fbpost","action":"App\Http\Controllers\Facebook\PostsController@deleteFromFavouriteForUser"},{"host":null,"methods":["POST"],"uri":"ajax\/add-favourite-twpost","name":"ajax.add-favourite-twpost","action":"App\Http\Controllers\Twitter\TweetsController@addToFavouriteForUser"},{"host":null,"methods":["POST"],"uri":"ajax\/delete-favourite-twpost","name":"ajax.delete-favourite-twpost","action":"App\Http\Controllers\Twitter\TweetsController@deleteFromFavouriteForUser"},{"host":null,"methods":["POST"],"uri":"ajax\/add-favourite-ytpost","name":"ajax.add-favourite-ytpost","action":"App\Http\Controllers\Youtube\VideosController@addToFavouriteForUser"},{"host":null,"methods":["POST"],"uri":"ajax\/delete-favourite-ytpost","name":"ajax.delete-favourite-ytpost","action":"App\Http\Controllers\Youtube\VideosController@deleteFromFavouriteForUser"},{"host":null,"methods":["POST"],"uri":"ajax\/add-favourite-gglpost","name":"ajax.add-favourite-ggpost","action":"App\Http\Controllers\Googleplus\PostsController@addToFavouriteForUser"},{"host":null,"methods":["POST"],"uri":"ajax\/delete-favourite-gglpost","name":"ajax.delete-favourite-ggpost","action":"App\Http\Controllers\Googleplus\PostsController@deleteFromFavouriteForUser"},{"host":null,"methods":["POST"],"uri":"ajax\/get-published-posts","name":"ajax.get-published-posts","action":"App\Http\Controllers\CommonPostsController@getAllPosts"},{"host":null,"methods":["POST"],"uri":"ajax\/composer-save-post","name":"ajax.composer-save-post","action":"App\Http\Controllers\ComposerController@savePost"},{"host":null,"methods":["POST"],"uri":"ajax\/get-saved-posts-dashboard","name":"ajax.get-saved-posts-dashboard","action":"App\Http\Controllers\DashboardCommonPostsController@getAllPosts"},{"host":null,"methods":["POST"],"uri":"ajax\/link-fb-entities","name":"ajax.link-fb-entities","action":"App\Http\Controllers\Auth\AuthController@linkFacebookAccNPages"},{"host":null,"methods":["POST"],"uri":"ajax\/user\/update","name":"ajax.update-user","action":"App\Http\Controllers\UserController@updateUser"},{"host":null,"methods":["POST"],"uri":"ajax\/user-password\/update","name":"ajax.update-user-password","action":"App\Http\Controllers\UserController@updateUserPassword"},{"host":null,"methods":["POST"],"uri":"ajax\/get-suggested-fbposts","name":"ajax.get-suggested-fbposts","action":"App\Http\Controllers\SuggestedPostsController@getFacebookPosts"},{"host":null,"methods":["POST"],"uri":"ajax\/get-suggested-twposts","name":"ajax.get-suggested-twposts","action":"App\Http\Controllers\SuggestedPostsController@getTwitterPosts"},{"host":null,"methods":["POST"],"uri":"ajax\/get-suggested-ytposts","name":"ajax.get-suggested-ytposts","action":"App\Http\Controllers\SuggestedPostsController@getYoutubeVideos"},{"host":null,"methods":["POST"],"uri":"ajax\/get-suggested-ggposts","name":"ajax.get-suggested-ggposts","action":"App\Http\Controllers\SuggestedPostsController@getGooglePosts"},{"host":null,"methods":["POST"],"uri":"ajax\/add-to-favourite","name":"ajax.add-to-favourite","action":"App\Http\Controllers\DashboardCommonPostsController@addToFavourite"},{"host":null,"methods":["POST"],"uri":"ajax\/delete-from-favourite","name":"ajax.delete-from-favourite","action":"App\Http\Controllers\DashboardCommonPostsController@deleteFromFavourite"},{"host":null,"methods":["POST"],"uri":"ajax\/get-users-suggested-posts","name":"ajax.get-users-suggested-posts","action":"App\Http\Controllers\UsersSuggestedPostsController@getAllPosts"},{"host":null,"methods":["POST"],"uri":"ajax\/get-user-favourite-posts","name":"ajax.get-user-favourite-posts","action":"App\Http\Controllers\MyStreamsController@getAllFavouritePosts"},{"host":null,"methods":["POST"],"uri":"ajax\/get-random-users-forfollow","name":"ajax.get-random-users-forfollow","action":"App\Http\Controllers\UserController@getRandomUsers"},{"host":null,"methods":["POST"],"uri":"ajax\/follow-user","name":"ajax.follow-user","action":"App\Http\Controllers\UserController@followUser"},{"host":null,"methods":["POST"],"uri":"ajax\/unfollow-user","name":"ajax.unfollow-user","action":"App\Http\Controllers\UserController@unfollowUser"},{"host":null,"methods":["POST"],"uri":"ajax\/get-followed-posts","name":"ajax.get-followed-posts","action":"App\Http\Controllers\MyStreamsController@getAllFollowedPosts"},{"host":null,"methods":["POST"],"uri":"ajax\/get-favourite-posts","name":"ajax.get-favourite-posts","action":"App\Http\Controllers\MyStreamsController@getAllFavouritePosts"},{"host":null,"methods":["POST"],"uri":"ajax\/get-stream-posts","name":"ajax.get-stream-posts","action":"App\Http\Controllers\MyStreamsController@getAllStreamPosts"},{"host":null,"methods":["POST"],"uri":"ajax\/get-stream-posts-by-user","name":"ajax.get-stream-posts-by-user","action":"App\Http\Controllers\MyStreamsController@getAllStreamPostsByUser"},{"host":null,"methods":["GET","HEAD"],"uri":"ajax\/get-facebook-streams","name":"ajax.get-facebook-streams","action":"App\Http\Controllers\SocialStreamsController@getFacebookStreams"},{"host":null,"methods":["GET","HEAD"],"uri":"ajax\/get-twitter-streams","name":"ajax.get-twitter-streams","action":"App\Http\Controllers\SocialStreamsController@getTwitterStreams"},{"host":null,"methods":["GET","HEAD"],"uri":"ajax\/get-youtube-streams","name":"ajax.get-youtube-streams","action":"App\Http\Controllers\SocialStreamsController@getYoutubeStreams"},{"host":null,"methods":["GET","HEAD"],"uri":"ajax\/get-google-streams","name":"ajax.get-google-streams","action":"App\Http\Controllers\SocialStreamsController@getGoogleStreams"},{"host":null,"methods":["POST"],"uri":"ajax\/add-facebook-stream","name":"ajax.add-facebook-stream","action":"App\Http\Controllers\SocialStreamsController@addFacebookStream"},{"host":null,"methods":["POST"],"uri":"ajax\/add-twitter-stream","name":"ajax.add-twitter-stream","action":"App\Http\Controllers\SocialStreamsController@addTwitterStream"},{"host":null,"methods":["POST"],"uri":"ajax\/add-youtube-stream","name":"ajax.add-youtube-stream","action":"App\Http\Controllers\SocialStreamsController@addYoutubeStream"},{"host":null,"methods":["POST"],"uri":"ajax\/add-google-stream","name":"ajax.add-google-stream","action":"App\Http\Controllers\SocialStreamsController@addGoogleStream"},{"host":null,"methods":["POST"],"uri":"ajax\/get-followed-users","name":"ajax.get-followed-users","action":"App\Http\Controllers\UserController@getFollowedUsers"},{"host":null,"methods":["POST"],"uri":"ajax\/get-followers","name":"ajax.get-followers","action":"App\Http\Controllers\UserController@getFollowers"},{"host":null,"methods":["POST"],"uri":"ajax\/get-followed-users-by-user","name":"ajax.get-followed-users-by-user","action":"App\Http\Controllers\UserController@getFollowedUsersByUser"},{"host":null,"methods":["POST"],"uri":"ajax\/get-followers-by-user","name":"ajax.get-followers-by-user","action":"App\Http\Controllers\UserController@getFollowersByUser"},{"host":null,"methods":["POST"],"uri":"ajax\/add-fbpost-to-userstream","name":"ajax.add-fbpost-to-userstream","action":"App\Http\Controllers\Facebook\PostsController@addToUserStream"},{"host":null,"methods":["POST"],"uri":"ajax\/remove-fbpost-from-userstream","name":"ajax.remove-fbpost-from-userstream","action":"App\Http\Controllers\Facebook\PostsController@deleteFromUserStream"},{"host":null,"methods":["POST"],"uri":"ajax\/add-twpost-to-userstream","name":"ajax.add-twpost-to-userstream","action":"App\Http\Controllers\Twitter\TweetsController@addToUserStream"},{"host":null,"methods":["POST"],"uri":"ajax\/remove-twpost-from-userstream","name":"ajax.remove-twpost-from-userstream","action":"App\Http\Controllers\Twitter\TweetsController@deleteFromUserStream"},{"host":null,"methods":["POST"],"uri":"ajax\/add-ytpost-to-userstream","name":"ajax.add-ytpost-to-userstream","action":"App\Http\Controllers\Youtube\VideosController@addToUserStream"},{"host":null,"methods":["POST"],"uri":"ajax\/remove-ytpost-from-userstream","name":"ajax.remove-ytpost-from-userstream","action":"App\Http\Controllers\Youtube\VideosController@deleteFromUserStream"},{"host":null,"methods":["POST"],"uri":"ajax\/add-ggpost-to-userstream","name":"ajax.add-ggpost-to-userstream","action":"App\Http\Controllers\Googleplus\PostsController@addToUserStream"},{"host":null,"methods":["POST"],"uri":"ajax\/remove-ggpost-from-userstream","name":"ajax.remove-ggpost-from-userstream","action":"App\Http\Controllers\Googleplus\PostsController@deleteFromUserStream"},{"host":null,"methods":["GET","HEAD"],"uri":"ajax\/get-scheduled-posts","name":"ajax.get-scheduled-posts","action":"App\Http\Controllers\ScheduledPostsController@getScheduledPosts"},{"host":null,"methods":["POST"],"uri":"ajax\/get-scheduled-post-info","name":"ajax.get-scheduled-post-info","action":"App\Http\Controllers\ScheduledPostsController@getScheduledPostInfo"},{"host":null,"methods":["POST"],"uri":"ajax\/edit-scheduled-post","name":"ajax.edit-scheduled-post","action":"App\Http\Controllers\ScheduledPostsController@editScheduledPost"},{"host":null,"methods":["POST"],"uri":"ajax\/delete-scheduled-post","name":"ajax.delete-scheduled-post","action":"App\Http\Controllers\ScheduledPostsController@deleteScheduledPost"},{"host":null,"methods":["POST"],"uri":"ajax\/massive-delete-scheduled-posts","name":"ajax.massive-delete-scheduled-posts","action":"App\Http\Controllers\ScheduledPostsController@massiveDeleteScheduledPosts"},{"host":null,"methods":["GET","HEAD"],"uri":"ajax\/get-profile-facebook-accounts","name":"ajax.get-profile-facebook-accounts","action":"App\Http\Controllers\SocialLoginsController@getFacebookSocialLogins"},{"host":null,"methods":["GET","HEAD"],"uri":"ajax\/get-profile-twitter-accounts","name":"ajax.get-profile-twitter-accounts","action":"App\Http\Controllers\SocialLoginsController@getTwitterSocialLogins"},{"host":null,"methods":["GET","HEAD"],"uri":"ajax\/get-profile-google-accounts","name":"ajax.get-profile-google-accounts","action":"App\Http\Controllers\SocialLoginsController@getGoogleSocialLogins"},{"host":null,"methods":["POST"],"uri":"ajax\/upload-user-avatar","name":"ajax.upload-user-avatar","action":"App\Http\Controllers\ProfileController@changeUserAvatar"},{"host":null,"methods":["POST"],"uri":"ajax\/store-promote-keywords","name":"ajax.store-promote-keywords","action":"App\Http\Controllers\PromotePagesController@storeKeywords"},{"host":null,"methods":["GET","HEAD"],"uri":"ajax\/get-promote-pages","name":"ajax.get-promote-pages","action":"App\Http\Controllers\PromotePagesController@getPromotePages"},{"host":null,"methods":["POST"],"uri":"ajax\/store-promote-page","name":"ajax.store-promote-page","action":"App\Http\Controllers\PromotePagesController@storePromotePage"},{"host":null,"methods":["POST"],"uri":"ajax\/delete-promote-page","name":"ajax.delete-promote-page","action":"App\Http\Controllers\PromotePagesController@deletePromotePage"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/get-suggested-posts","name":"get-suggested-posts-page","action":"App\Http\Controllers\SuggestedPostsController@getPage"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/get-users-suggested-posts","name":"get-users-suggested-posts-page","action":"App\Http\Controllers\UsersSuggestedPostsController@getPage"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/dashboard\/my-favourite-posts","name":"get-my-favourite-posts","action":"App\Http\Controllers\MyStreamsController@getFavouritePostsPage"},{"host":null,"methods":["GET","HEAD"],"uri":"foo","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/open","name":"debugbar.openhandler","action":"Barryvdh\Debugbar\Controllers\OpenHandlerController@handle"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/clockwork\/{id}","name":"debugbar.clockwork","action":"Barryvdh\Debugbar\Controllers\OpenHandlerController@clockwork"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/assets\/stylesheets","name":"debugbar.assets.css","action":"Barryvdh\Debugbar\Controllers\AssetController@css"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/assets\/javascript","name":"debugbar.assets.js","action":"Barryvdh\Debugbar\Controllers\AssetController@js"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);

