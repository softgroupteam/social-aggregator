$( document ).ready(function() {

    function post_template(post) {
        // if(typeof post.entities.media == "undefined") {
        //     return '';
        // }

        var template = '<div class="panel panel-default " style="width:350px; display:inline-block; margin:5px;">' +
            '<div class="panel-body">';
        if(typeof post.entities.media != "undefined") {
            template += '<div class="text-center">' +
                '<img src="' + post.entities.media[0].media_url + '" style="width:100%;">' +
                '</div>';
        }
            template += '<div class="box box-info">' +
            '<div class="box-header with-border">' +
                '<p class="text-blue" >'+post.createdLocal+'</p>'+
                '<div class="tw-post-statistics">'+
                    '<span><i class="fa fa-retweet"></i>'+post.retweet_count+'</span>'+
                    '<span><i class="fa fa-star-o"></i>'+post.favorite_count+'</span>'+
                '</div>'+
            '<p class="box-title text-center">' +
            post.formatted_text +
            '</p>' +
            '</div><!-- /.box-header -->' +
            '<div class="box-body">' +
                '<button type="button" class="btn btn-success save-twpost '+ ((post.state == 'saved_published' || post.state == 'saved_notpublished')?'hidden':'')+'" data-post-id="'+ post.id_str +'">Save</button>' +
                '<button type="button" class="btn btn-danger delete-twpost '+ ((post.state == 'notsaved')?'hidden':'')+'" data-post-id="'+ post.id_str +'" style="margin-right: 3px;">Delete</button>' +
                '<button type="button" class="btn btn-info publish-twpost '+ ((post.state == 'notsaved' || post.state == 'saved_published')?'hidden':'')+'" data-post-id="'+ post.id_str +'">Publish</button>' +
                '<button type="button" class="btn btn-info unpublish-twpost '+ ((post.state == 'notsaved' || post.state == 'saved_notpublished')?'hidden':'')+'" data-post-id="'+ post.id_str +'">Unpublish</button>' +
            '</div><!-- /.box-body -->' +
            '</div><!-- /.box -->' +
            '</div>' +
            '</div>';

        return template;
    }

    function get_posts(action, form_data) {
        $('.add-more-block').remove();
        $.ajax({
            url: action,
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                console.log(response);
                if (response.status) {
                    $.each(response.posts, function(key, item) {
                        var post = post_template(item);
                        $('.content > .container').append(post);
                    });
                    $('.content > .container').append('<div class="add-more-block col-md-12 text-center"><button class="add-more-posts btn btn-default" data-action="' + action + '" data-lastelement="' + response.lastElement + '" data-twitterusername="' + response.twitterUserName + '">Add more...</button></div>');
                }
            }
        });
    }


    $(document).on('click', '.add-more-posts', function (e) {
        e.preventDefault();
        var action = $(this).data('action');
        var lastElement = $(this).data('lastelement');
        var twitterUserName = $(this).data('twitterusername');

        var form_data = new FormData();
        form_data.append('lastElement', lastElement);
        form_data.append('twitterUserName', twitterUserName);
        get_posts(action, form_data);
    });

    $(document).on('click', '.save-twpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.store-twpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.publish-twpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.publish-twpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.delete-twpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.delete-twpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.unpublish-twpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.unpublish-twpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    function set_buttons_state(state,parent){

        switch(state){
            case 'saved_notpublished':
                $(parent.find('.publish-twpost')).removeClass('hidden');
                $(parent.find('.save-twpost')).addClass('hidden');
                $(parent.find('.delete-twpost')).removeClass('hidden');
                $(parent.find('.unpublish-twpost')).addClass('hidden');
                break;
            case 'saved_published':
                $(parent.find('.publish-twpost')).addClass('hidden');
                $(parent.find('.save-twpost')).addClass('hidden');
                $(parent.find('.delete-twpost')).removeClass('hidden');
                $(parent.find('.unpublish-twpost')).removeClass('hidden');
                break;
            case 'notsaved':
                $(parent.find('.publish-twpost')).addClass('hidden');
                $(parent.find('.save-twpost')).removeClass('hidden');
                $(parent.find('.delete-twpost')).addClass('hidden');
                $(parent.find('.unpublish-twpost')).addClass('hidden');
                break;
        }
    }

});