$(document).ready(function() {
    var test = {size: 1200, columns: 10};
    var test2 = {size: 700, columns: 8};
    var grid = $("#mosaic-grid").mosaic({
        tileModel: '.sizer',
        columns: 3,
        gutter: 1,
        heightFromWidth: true,
        breakpoints: [test, test2]
    });

    $(".link").click(function() {
        $(".active").removeClass("active");
        $(this).addClass("active");
    });
});