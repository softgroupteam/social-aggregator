$( document ).ready(function() {

    //add facebook page
    $('#submit-fbpage').click(function(){
        $.ajax({
            method: "POST",
            url: laroute.route('modal.add-fbpage'),
            data: $('#form-add-fb-page').serialize(),
            success: function(msg){
                if(msg.status == 'ok') {
                    $("#fbpage-modal").modal('hide');
                    window.location = laroute.route('dashboard');
                }
            },
            error: function(){

            }
        });
    })

    //add youtube channel
    $('#submit-ytchannel').click(function(){
        $.ajax({
            method: "POST",
            url: laroute.route('modal.add-ytchannel'),
            data: $('#form-add-yt-channel').serialize(),
            success: function(msg){
                if(msg.status == 'ok') {
                    $("#fbpage-modal").modal('hide');
                    window.location = laroute.route('dashboard');
                }
            },
            error: function(){

            }
        });
    })

    //add twitter page
    $('#submit-twpage').click(function(){
        $.ajax({
            method: "POST",
            url: laroute.route('modal.add-twpage'),
            data: $('#form-add-tw-page').serialize(),
            success: function(msg){
                if(msg.status == 'ok') {
                    $("#fbpage-modal").modal('hide');
                    window.location = laroute.route('dashboard');
                }
            },
            error: function(){

            }
        });
    })

    //delete social tab
    $('.delete-social-tab').click(function(event){
        event.preventDefault();
        var modal = $('#delete-socialtab-modal');
        var modal_submit = $('#submit-tab-delete');
        var target = $(event.target);
        modal_submit.attr('data-id',target.data('id'));
        modal_submit.attr('data-social',target.data('social'));
        modal.modal('show');
    })

    $('#submit-tab-delete').click(function(event){
        var target = $(event.target);
        var form_data = new FormData();
        form_data.append('id', target.attr('data-id'));
        form_data.append('social', target.data('social'));
        var table = {};
        var modal = $('#delete-socialtab-modal');
        switch(target.data('social')){
            case 'facebook':
                table = facebook_streams_table;
                break;
            case 'twitter':
                table = twitter_streams_table;
                break;
            case 'youtube':
                table = youtube_streams_table;
                break;
            case 'google_plus':
                table = google_streams_table;
                break;
        }
        $.ajax({
            url: laroute.route('modal.delete-socialtab'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                if (typeof table !== 'undefined') {
                    table.ajax.reload();
                }
                modal.modal('hide');
            }
        });
    });

    //add youtube channel
    $('#submit-gpchannel').click(function(){
        $.ajax({
            method: "POST",
            url: laroute.route('modal.add-gpchannel'),
            data: $('#form-add-gp-channel').serialize(),
            success: function(msg){
                if(msg.status == 'ok') {
                    $("#fbpage-modal").modal('hide');
                    window.location = laroute.route('dashboard');
                }
            },
            error: function(){

            }
        });
    })


    $('#submit-category').click(function(){
        $.ajax({
            method: "POST",
            url: laroute.route('modal.add-category'),
            data: $('#form-add-category').serialize(),
            success: function(msg){
                $("#gpchannel-modal").modal('hide');
                window.location = laroute.route('dashboard');
            },
            error: function(){

            }
        });
    })

    function fillCategories(modal_id){
        if($(modal_id+' .category-select').children().length == 0) {
            $.ajax({
                method: "Get",
                url: laroute.route('modal.get-categories'),
                success: function (msg) {
                    $.each(msg, function (key, item) {
                        $(modal_id+' .category-select').append('<option value="'+item.id+'">' + item.text + '</option>');
                    });
                },
                error: function () {

                }
            });
        }
    }

    $('.toogle-modal-with-categories').click(function(e){
        e.preventDefault();
        var modal_id = $(e.target).attr('data-target');
        fillCategories(modal_id);
        $(modal_id).modal('show');
    });

    /*toastr options*/
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
});