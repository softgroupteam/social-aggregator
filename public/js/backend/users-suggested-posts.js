$( document ).ready(function() {

    initialGetRandomUsers();
    intitalGetPosts();

    function post_template(post) {
        var template = '<div class="panel panel-default common-post-dashboard" data-user-id="'+post['interested_user_id']+'" style="width:350px; display:inline-block; margin:5px;">' +
            '<div class="panel-heading">' +
            '<span>Liked by <em>'+post['interested_user_name']+'</em></span>'+
            '</div>'+
            '<div class="panel-body">' +
            '<img class="post-img" src="' + ((post['image_url'] != null)?post['image_url']:'/img/mock/post.png') + '" style="width:100%">' +
            '<div class="box box-primary">' +
            ' <p class="text-blue" >'+post['social_created_at']+'</p>'+
            '<div class="box-header with-border">' +
            '<p class="box-title post-text" >' +
            post['message'] +
            '</p>' +
            '</div><!-- /.box-header -->' +
            '<div class="box-body text-center">' +
            '<button type="button" class="btn btn-success follow-user" data-user-id="'+post['interested_user_id']+'">Follow user</button>' +
            '</div><!-- /.box-body -->' +
            '</div><!-- /.box -->' +
            '</div>' +
            '</div>';

        return template;
    }


    function intitalGetPosts(){
        get_posts(laroute.route('ajax.get-users-suggested-posts'));
    }

    function initialGetRandomUsers(){
        get_random_users(laroute.route('ajax.get-random-users-forfollow'));
    }


    function get_random_users(action){
        $.ajax({
            url: action,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.users, function(key, item) {
                        $('#users-forfollow-container').append('<li class="user-forfollow" data-user-id="'+item.id+'"><a href="#">'+item.name+'<button class="btn-primary pull-right follow-user" data-user-id="'+item.id+'">follow</button></a></li>');
                    });
                }
            }
        });
    }

    function get_posts(action) {
        $('.add-more-block').remove();
        $.ajax({
            url: action,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    console.log(response.posts);
                    $.each(response.posts, function(key, item) {
                        var post = post_template(item);
                        $('.users-random-posts-container').append(post);
                    });
                    if(response.posts.length >0)
                        $('.users-random-posts-container').append('<div class="add-more-block col-md-12 text-center"><button class="add-more-users-suggested-posts btn btn-default" data-offset="' + response.offset +'">Refresh posts</button></div>');
                }
            }
        });
    }


    $(document).on('click', '.add-more-users-suggested-posts', function (e) {
        e.preventDefault();
        $('#users-random-posts-container').empty();
        get_posts(laroute.route('ajax.get-users-suggested-posts'));
    });

    $(document).on('click', '.follow-user', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('user_id', $(this).data('userId'));
        $.ajax({
            url: laroute.route('ajax.follow-user'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if(response.status == 'success'){
                    $('li.user-forfollow[data-user-id="'+response.user_id+'"]').remove();
                    $('div.common-post-dashboard[data-user-id="'+response.user_id+'"]').remove();
                    $(target).addClass('hidden');
                }
            }
        });
    });
});