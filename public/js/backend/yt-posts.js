$( document ).ready(function() {

    function post_template(post) {

        var template = '<div class="panel panel-default " style="width:350px; display:inline-block; margin:5px;">' +
            '<div class="panel-body">' +
            ' <div class="video-container">'+
            '<iframe width=100%" frameborder="0" allowfullscreen class="video" src="https://www.youtube.com/embed/'+post.id+'">'+
            '</iframe>' +
            '</div>'+
            '<div class="box box-info">' +
            '<div class="box-header with-border">' +
            '<p class="text-blue" >'+ post.createdLocal +'</p>'+
            '<div class="yt-post-statistics">'+
                '<span><i class="fa fa-eye"></i>'+post.statistics.viewCount+'</span>'+
                '<span><i class="fa fa-comment"></i>'+post.statistics.commentCount+'</span>'+
                '<span><i class="fa fa-thumbs-up"></i>'+post.statistics.likeCount+'</span>'+
                '<span><i class="fa fa-thumbs-down"></i>'+post.statistics.dislikeCount+'</span>'+
            '</div>'+
            '<p class="box-title text-center">' +
            post.snippet.title +
            '</p>' +
            '</div><!-- /.box-header -->' +
            '<div class="box-body">' +
                '<button type="button" class="btn btn-success save-ytpost '+ ((post.state == 'saved_published' || post.state == 'saved_notpublished')?'hidden':'')+'" data-post-id="'+ post.id +'">Save</button>' +
                '<button type="button" class="btn btn-danger delete-ytpost '+ ((post.state == 'notsaved')?'hidden':'')+'" data-post-id="'+ post.id +'" style="margin-right: 3px;">Delete</button>' +
                '<button type="button" class="btn btn-info publish-ytpost '+ ((post.state == 'notsaved' || post.state == 'saved_published')?'hidden':'')+'" data-post-id="'+ post.id +'">Publish</button>' +
                '<button type="button" class="btn btn-info unpublish-ytpost '+ ((post.state == 'notsaved' || post.state == 'saved_notpublished')?'hidden':'')+'" data-post-id="'+ post.id +'">Unpublish</button>' +
            '</div><!-- /.box-body -->' +
            '</div><!-- /.box -->' +
            '</div>' +
            '</div>';

        return template;
    }

    // function convertTime(date) {
    //     var d = new Date(date),	// Convert the passed timestamp to milliseconds
    //         yyyy = d.getFullYear(),
    //         mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
    //         dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
    //         hh = ('0' + d.getHours()).slice(-2),
    //         min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
    //         sec = ('0' + d.getSeconds()).slice(-2),
    //         time;
    //
    //     time = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;
    //
    //     return time;
    // }

    function get_posts(action, form_data) {
        $('.add-more-block').remove();
        $.ajax({
            url: action,
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                console.log(response);
                if (response.status) {
                    $.each(response.posts, function(key, item) {
                        var post = post_template(item);
                        $('.content > .container').append(post);
                    });
                    $('.content > .container').append('<div class="add-more-block col-md-12 text-center"><button class="add-more-posts btn btn-default" data-action="' + action + '" data-channelId="' + response.channelId + '" data-pageToken="' + response.pageToken + '">Add more...</button></div>');
                }
            }
        });
    }


    $(document).on('click', '.add-more-posts', function (e) {
        e.preventDefault();
        var action = $(this).data('action');
        var channelId = $(this).data('channelid');
        var pageToken = $(this).data('pagetoken');

        var form_data = new FormData();
        form_data.append('channelId', channelId);
        form_data.append('pageToken', pageToken);
        get_posts(action, form_data);
    });

    $(document).on('click', '.save-ytpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('video_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.store-ytpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.publish-ytpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('video_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.publish-ytpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.delete-ytpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('video_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.delete-ytpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.unpublish-ytpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('video_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.unpublish-ytpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    function set_buttons_state(state,parent){

        switch(state){
            case 'saved_notpublished':
                $(parent.find('.publish-ytpost')).removeClass('hidden');
                $(parent.find('.save-ytpost')).addClass('hidden');
                $(parent.find('.delete-ytpost')).removeClass('hidden');
                $(parent.find('.unpublish-ytpost')).addClass('hidden');
                break;
            case 'saved_published':
                $(parent.find('.publish-ytpost')).addClass('hidden');
                $(parent.find('.save-ytpost')).addClass('hidden');
                $(parent.find('.delete-ytpost')).removeClass('hidden');
                $(parent.find('.unpublish-ytpost')).removeClass('hidden');
                break;
            case 'notsaved':
                $(parent.find('.publish-ytpost')).addClass('hidden');
                $(parent.find('.save-ytpost')).removeClass('hidden');
                $(parent.find('.delete-ytpost')).addClass('hidden');
                $(parent.find('.unpublish-ytpost')).addClass('hidden');
                break;
        }
    }


});