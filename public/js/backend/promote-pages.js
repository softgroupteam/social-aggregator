var promotePagesFeature = (function() {

    var $pagesTable = $("#promote-pages-table");
    var pagesDatatable = {};
    var $saveKeywordButton = $('#save-keywords-button');
    var $keywordForm = $('#keywords-form');
    var $tableSpinner = $('.spinner-stack');


    var init = function() {
        $saveKeywordButton.click(saveKeywords);
        $(document).on('change', '.promote-switch', onPromoteSwitchChange)
        initPagesTable();
    };

    var saveKeywords = function(e){
        e.preventDefault();
        var formData = new FormData($keywordForm[0]);
        $.ajax({
            url: laroute.route('ajax.store-promote-keywords'),
            type: 'POST',
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.status == 'success') {
                    toastr.success(data.success_msg);
                }
                else{
                    $.each(data.errors, function(key, item) {
                        toastr.warning(item);
                    });
                }
                refreshPagesTable();
            }
        });
    };

    var initPagesTable = function (){
        $.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
        {
            return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
                return $('input', td).prop('checked') ? '1' : '0';
            } );
        };
        pagesDatatable = $pagesTable.DataTable({
            ajax: laroute.route('ajax.get-promote-pages'),
            searching: false,
            "order": [[ 4, "desc" ]],
            "bInfo" : false,
            "bLengthChange": false,
            columns: [
                {data:'id'},
                {
                    "render": function ( data, type, full, meta ) {
                        return '<a target="_blank" href="https://www.facebook.com/'+full.id+'">'+full.name+'</a>';
                    }
                },
                {data: 'likes'},
                {data: 'talkingAbout'},
                {
                    sClass: 'text-center',
                    sSortDataType: "dom-checkbox",
                    "render": function ( data, type, full, meta ) {
                        var template = '<div class="material-switch pull-center">'+
                                            '<input id="promote-switch-'+full.id+'" class="promote-switch" data-id="'+full.id+'" data-name="'+full.name+'" data-likes="'+full.likes+'" data-talking-about="'+full.talkingAbout+'" type="checkbox" '+((full.promoted == true)?'checked':'')+'/>'+
                                            '<label for="promote-switch-'+full.id+'" class="label-primary"></label>'+
                                        '</div>';
                        return template;
                    }
                }
            ]
        } );
    }

    var refreshPagesTable = function(){
        $tableSpinner.removeClass('hidden');
        pagesDatatable.ajax.reload(function ( json ) {
            $tableSpinner.addClass('hidden');
        });
    }

    var onPromoteSwitchChange = function(){
        if($(this).is(':checked')){
            savePromotePage($(this));
        }
        else{
            deletePromotePage($(this));
        }
    }

    var savePromotePage = function($target){
        var formData = new FormData();
        formData.append('id', $target.attr('data-id'));
        formData.append('name', $target.attr('data-name'));
        formData.append('likes', $target.attr('data-likes'));
        formData.append('talkingAbout', $target.attr('data-talking-about'));
        $.ajax({
            url: laroute.route('ajax.store-promote-page'),
            type: 'POST',
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.status == 'success') {
                    toastr.success(data.success_msg);
                }
                else{
                    $.each(data.errors, function(key, item) {
                        toastr.warning(item);
                    });
                }
            }
        });
    }

    var deletePromotePage = function($target){
        var formData = new FormData();
        formData.append('id', $target.attr('data-id'));
        $.ajax({
            url: laroute.route('ajax.delete-promote-page'),
            type: 'POST',
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.status == 'success') {
                    toastr.success(data.success_msg);
                }
                else{
                    $.each(data.errors, function(key, item) {
                        toastr.warning(item);
                    });
                }
            }
        });
    }

    return {
        init: init,
    };
})();

$( document ).ready( promotePagesFeature.init());