$( document ).ready(function() {

    function post_template(post) {
        // if(typeof post.entities.media == "undefined") {
        //     return '';
        // }

        var template = '<div class="panel panel-default " style="width:350px; display:inline-block; margin:5px;">' +
            '<div class="panel-body">';
        if(typeof post.entities.media != "undefined") {
            template += '<div class="text-center">' +
                '<img src="' + post.entities.media[0].media_url + '" style="width:100%;">' +
                '</div>';
        }
            template += '<div class="box box-info">' +
            '<div class="box-header with-border">' +
                '<p class="text-blue" >'+post.createdLocal+'</p>'+
                '<div class="tw-post-statistics">'+
                    '<span><i class="fa fa-retweet"></i>'+post.retweet_count+'</span>'+
                    '<span><i class="fa fa-star-o"></i>'+post.favorite_count+'</span>'+
                '</div>'+
            '<p class="box-title text-center">' +
            post.formatted_text +
            '</p>' +
            '</div><!-- /.box-header -->' +
            '<div class="box-body">' +
                '<button type="button" class="btn btn-success save-addfavourite-twpost '+ ((post.state == 'saved_favourite')?'hidden':'')+'" data-post-id="'+ post.id +'">Save to favourite</button>' +
                '<button type="button" class="btn btn-danger delete-removefavourite-twpost '+ ((post.state == 'notsaved_notfavourite')?'hidden':'')+'" data-post-id="'+ post.id +'" style="margin-right: 3px;">Delete from favourite</button>' +
            '</div><!-- /.box-body -->' +
            '</div><!-- /.box -->' +
            '</div>' +
            '</div>';

        return template;
    }

    // function convertTime(date) {
    //     var d = new Date(date),	// Convert the passed timestamp to milliseconds
    //         yyyy = d.getFullYear(),
    //         mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
    //         dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
    //         hh = ('0' + d.getHours()).slice(-2),
    //         min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
    //         sec = ('0' + d.getSeconds()).slice(-2),
    //         time;
    //
    //     time = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;
    //
    //     return time;
    // }

    function get_posts(action, form_data) {
        $('.add-more-block').remove();
        $.ajax({
            url: action,
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                console.log(response);
                if (response.status) {
                    $.each(response.posts, function(key, item) {
                        var post = post_template(item);
                        $('.content > .container').append(post);
                    });
                    $('.content > .container').append('<div class="add-more-block col-md-12 text-center"><button class="add-more-posts btn btn-default" data-action="' + action + '" data-lastelement="' + response.lastElement + '" data-twitterusername="' + response.twitterUserName + '">Add more...</button></div>');
                }
            }
        });
    }


    $(document).on('click', '.add-more-posts', function (e) {
        e.preventDefault();
        var action = $(this).data('action');
        var lastElement = $(this).data('lastelement');
        var twitterUserName = $(this).data('twitterusername');

        var form_data = new FormData();
        form_data.append('lastElement', lastElement);
        form_data.append('twitterUserName', twitterUserName);
        get_posts(action, form_data);
    });

    $(document).on('click', '.save-addfavourite-twpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        blockButtons(parent);
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.add-favourite-twpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
                releaseButtons(parent);
            }
        });
    });

    $(document).on('click', '.delete-removefavourite-twpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        blockButtons(parent);
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.delete-favourite-twpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
                releaseButtons(parent);
            }
        });
    });


    function set_buttons_state(state,parent){

        if(state == 'saved_favourite'){
            $(parent.find('.save-addfavourite-twpost')).addClass('hidden');
            $(parent.find('.delete-removefavourite-twpost')).removeClass('hidden');
        }
        else if('notsaved_notfavourite'){
            $(parent.find('.save-addfavourite-twpost')).removeClass('hidden');
            $(parent.find('.delete-removefavourite-twpost')).addClass('hidden');
        }
    }

    function blockButtons(parent){
        $(parent.find('.save-addfavourite-twpost')).prop( "disabled", true );
        $(parent.find('.delete-removefavourite-twpost')).prop( "disabled", true );
    }

    function releaseButtons(parent){
        $(parent.find('.save-addfavourite-twpost')).prop( "disabled", false );
        $(parent.find('.delete-removefavourite-twpost')).prop( "disabled", false );
    }

});