$( document ).ready(function() {
    initialGetStreamPosts();
    initialGetFollowers();
    initialGetFollowedUsers();

    /* my streams */


    function stream_post_template(post) {
        var template = '<div class="panel panel-default stream-post stream-post-'+post['social_type']+'-'+post['id']+'"  style="width:350px; display:inline-block; margin:5px;">' +
            '<div class="panel-body">' +
            ((post['embed_video_html'] != '')?'<div class="fb-post-embed-wrapper" >'+post['embed_video_html']+'</div>':((post['image_url'] != null)?'<img class="post-img" src="'+post['image_url']+'" style="width:100%">':'/img/mock/post.png')) +
            '<div class="box box-primary">' +
            ' <p class="text-blue" >'+post['social_created_at']+'</p>'+
            '<div class="box-header with-border">' +
            '<p class="box-title post-text" >' +
            post['message'] +
            '</p>' +
            '</div><!-- /.box-header -->' +
            '<div class="box-body text-center">' +
            '</div><!-- /.box-body -->' +
            '</div><!-- /.box -->' +
            '</div>' +
            '</div>';
        return template;
    }



    function initialGetStreamPosts(){
        var form_data = new FormData();
        form_data.append('offset', 0);
        form_data.append('userId', $('.box-profile').attr('data-user-id'));
        get_stream_posts(form_data,laroute.route('ajax.get-stream-posts-by-user'));
    }

    function get_stream_posts(data, action){
        $('.refresh-stream-posts-wrapper').remove();
        $.ajax({
            url: action,
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    console.log(response.posts);
                    $.each(response.posts, function(key, item) {
                        var post = stream_post_template(item);
                        $('#profile-my-streams').append(post);
                    });
                    if(response.posts.length >0)
                        $('#profile-my-streams').append('<div class="refresh-stream-posts-wrapper col-md-12 text-center v-mrgn-5"><button class="refresh-stream-posts btn btn-primary" data-offset="' + response.offset +'">Add more</button></div>');
                }
            }
        });
    }

    $(document).on('click', '.refresh-stream-posts', function (e) {
        e.preventDefault();
        var offset = $(this).data('offset');
        var form_data = new FormData();
        form_data.append('offset', offset);
        get_stream_posts(form_data, laroute.route('ajax.get-stream-posts'));
    });


    /* followers */

    function follower_user_template(user){
        var template =  '<div class="follower-user-acc row" data-user-id="'+user.id+'">'+
            '<div class="col-sm-2">'+
            '<img class="picture" src="/img/mock/mock_user_avatar_small.png">'+
            '</div>'+
            '<div class="col-sm-8">'+
            '<p class="checkbox-label">'+user.name+'</p>'+
            '</div>'+
            '<div class="col-sm-2 profile-follower-users-actions" style="padding-top:30px;">'+
            '<div style="overflow: auto">'+
            '<a type="button" href="'+laroute.route('profile-by-user',{id : user.id})+'" class="pull-right btn btn-success profile-follower-user-view" data-user-id="'+user.id+'">view</a>'+
            '</div>'+
            '</div>'+
            '</div>';
        return template;
    }

    function initialGetFollowers(){
        get_followers(laroute.route('ajax.get-followers-by-user'));
    }


    function get_followers(action){
        var form_data = new FormData();
        form_data.append('userId', $('.box-profile').attr('data-user-id'));
        $.ajax({
            url: action,
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.users, function(key, item) {
                        var user = follower_user_template(item);
                        $('#profile-followers').append(user);
                    });
                }
            }
        });
    }

    /* following */

    function followed_user_template(user){
        var template =  '<div class="followed-user-acc row" data-user-id="'+user.id+'">'+
            '<div class="col-sm-2">'+
            '<img class="picture" src="/img/mock/mock_user_avatar_small.png">'+
            '</div>'+
            '<div class="col-sm-8">'+
            '<p class="checkbox-label">'+user.name+'</p>'+
            '</div>'+
            '<div class="col-sm-2 profile-followed-users-actions" style="padding-top:30px;">'+
            '<div style="overflow: auto">'+
            '<a type="button" href="'+laroute.route('profile-by-user',{id : user.id})+'" class="pull-right btn btn-success profile-followed-user-view" data-user-id="'+user.id+'">view</a>'+
            '</div>'+
            '</div>'+
            '</div>';
        return template;
    }

    function initialGetFollowedUsers(){
        get_followed_users(laroute.route('ajax.get-followed-users-by-user'));
    }


    function get_followed_users(action){
        var form_data = new FormData();
        form_data.append('userId', $('.box-profile').attr('data-user-id'));
        $.ajax({
            url: action,
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.users, function(key, item) {
                        var user = follower_user_template(item);
                        $('#profile-following').append(user);
                    });
                }
            }
        });
    }

});