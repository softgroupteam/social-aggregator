$( document ).ready(function() {

    function post_template(post) {
        console.log(post);
        var template = '<div class="panel panel-default " style="width:350px; display:inline-block; margin:5px;">' +
            '<div class="panel-body">' +

                '<div class="text-center">' +
            ((typeof post['image'] != "undefined")?'<img src="' + post['image'] + '" style="max-width:100%;">':'') +
                '</div>' +
                '<div class="box box-info">' +
                    '<div class="box-header with-border">' +
                        '<p class="text-blue" >'+ post['createdLocal'] +'</p>'+
                        '<div class="gg-post-statistics">'+
                            '<span><i class="fa fa-share-square-o"></i>'+post['resharers']+'</span>'+
                            '<span><i class="fa fa-comment"></i>'+post['comments']+'</span>'+
                            '<span><i class="fa fa-thumbs-up"></i>'+post['likes']+'</span>'+
                        '</div>'+
                        '<p class="box-title text-center">' +
                            post['content'] +
                        '</p>' +
                    '</div><!-- /.box-header -->' +
                    '<div class="box-body">' +
                        '<button type="button" class="btn btn-success save-gglpost '+ ((post.state == 'saved_published' || post.state == 'saved_notpublished')?'hidden':'')+'" data-post-id="'+ post.id +'">Save</button>' +
                        '<button type="button" class="btn btn-danger delete-gglpost '+ ((post.state == 'notsaved')?'hidden':'')+'" data-post-id="'+ post.id +'" style="margin-right: 3px;">Delete</button>' +
                        '<button type="button" class="btn btn-info publish-gglpost '+ ((post.state == 'notsaved' || post.state == 'saved_published')?'hidden':'')+'" data-post-id="'+ post.id +'">Publish</button>' +
                        '<button type="button" class="btn btn-info unpublish-gglpost '+ ((post.state == 'notsaved' || post.state == 'saved_notpublished')?'hidden':'')+'" data-post-id="'+ post.id +'">Unpublish</button>' +
                    '</div><!-- /.box-body -->' +
                '</div><!-- /.box -->' +
            '</div>' +
        '</div>';

        return template;
    }

    function get_posts(action, form_data) {
        $('.add-more-block').remove();
        //console.log(form_data);
        $.ajax({
            url: action,
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.posts, function(key, item) {
                        var post = post_template(item);
                        $('.content > .container').append(post);
                    });
                    $('.content > .container').append('<div class="add-more-block col-md-12 text-center"><button class="add-more-posts btn btn-default" data-group="' + response.group + '" data-action="' + action + '" data-page="' + response.page_token + '" data-token="' + response.token + '">Add more...</button></div>');
                }
            }
        });
    }

    function convertTime(date) {
        var d = new Date(date),	// Convert the passed timestamp to milliseconds
            yyyy = d.getFullYear(),
            mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
            dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
            hh = ('0' + d.getHours()).slice(-2),
            min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
            sec = ('0' + d.getSeconds()).slice(-2),
            time;

        time = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;

        return time;
    }

    $('#get-posts-form').on('submit', function (e) {
        e.preventDefault();

        var action = $(this).attr('action');
        var form_data = new FormData();
        $(this).find('input').each(function () {
            var name = $(this).attr('name');
            var value = $(this).val();
            if (value) {
                form_data.append(name, value);
            }
        });

        $('.content > .container').html('');
        get_posts(action, form_data);
    });

    $(document).on('click', '.add-more-posts', function (e) {
        e.preventDefault();
        console.log(1);
        var action = $(this).data('action');
        var group = $(this).data('group');
        var page_token = $(this).data('page');
        var _token = $(this).data('token');

        var form_data = new FormData();
        form_data.append('google_group', group);
        form_data.append('page_token', page_token);
        form_data.append('_token', _token);

        get_posts(action, form_data);
    });

    $(document).on('click', '.save-gglpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.store-gglpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.publish-gglpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.publish-gglpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.delete-gglpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.delete-gglpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.unpublish-gglpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.unpublish-gglpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    function set_buttons_state(state,parent){

        switch(state){
            case 'saved_notpublished':
                $(parent.find('.publish-gglpost')).removeClass('hidden');
                $(parent.find('.save-gglpost')).addClass('hidden');
                $(parent.find('.delete-gglpost')).removeClass('hidden');
                $(parent.find('.unpublish-gglpost')).addClass('hidden');
                break;
            case 'saved_published':
                $(parent.find('.publish-gglpost')).addClass('hidden');
                $(parent.find('.save-gglpost')).addClass('hidden');
                $(parent.find('.delete-gglpost')).removeClass('hidden');
                $(parent.find('.unpublish-gglpost')).removeClass('hidden');
                break;
            case 'notsaved':
                $(parent.find('.publish-gglpost')).addClass('hidden');
                $(parent.find('.save-gglpost')).removeClass('hidden');
                $(parent.find('.delete-gglpost')).addClass('hidden');
                $(parent.find('.unpublish-gglpost')).addClass('hidden');
                break;
        }
    }
});