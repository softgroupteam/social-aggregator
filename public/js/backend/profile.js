$( document ).ready(function() {
    initialGetRandomUsers();
    initialGetStreamPosts();
    initialGetFollowers();
    initialGetFollowedUsers();

    $('#submit-maininfo-edit').on('click',function (e) {
        e.preventDefault();
        var formData = new FormData($('#maininfo-profile-form')[0]);
        $.ajax({
            url: laroute.route('ajax.update-user'),
            type: 'POST',
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if(data.status == 'success') {
                    toastr.success(data.success_msg);
                }
                else{
                    $.each(data.errors, function(key, item) {
                        toastr.warning(item);
                    });
                }
            }
        });
    })

    $('#submit-password-edit').on('click',function (e) {
        e.preventDefault();
        var formData = new FormData($('#password-profile-form')[0]);
        $.ajax({
            url: laroute.route('ajax.update-user-password'),
            type: 'POST',
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if(data.status == 'success') {
                    toastr.success(data.success_msg);
                }
                else{
                    $.each(data.errors, function(key, item) {
                        toastr.warning(item);
                    });
                }
            }
        });
    });

    /* my streams */


    function stream_post_template(post) {
        var template = '<div class="panel panel-default stream-post stream-post-'+post['social_type']+'-'+post['id']+'"  style="width:350px; display:inline-block; margin:5px;">' +
            '<div class="panel-body">' +
            ((post['embed_video_html'] != '')?'<div class="fb-post-embed-wrapper" >'+post['embed_video_html']+'</div>':((post['image_url'] != null)?'<img class="post-img" src="'+post['image_url']+'" style="width:100%">':'/img/mock/post.png')) +
            '<div class="box box-primary">' +
            ' <p class="text-blue" >'+post['social_created_at']+'</p>'+
            '<div class="box-header with-border">' +
            '<p class="box-title post-text" >' +
            post['message'] +
            '</p>' +
            '</div><!-- /.box-header -->' +
            '<div class="box-body text-center">' +
            '<button type="button" class="btn btn-danger remove-from-stream" data-post-id="'+ post['id'] + '" data-socialtype="'+ post['social_type'] +'" style="margin-left: 3px;">Unpublish</button>' +
            '</div><!-- /.box-body -->' +
            '</div><!-- /.box -->' +
            '</div>' +
            '</div>';
        return template;
    }



    function initialGetStreamPosts(){
        var offset = 0;
        var form_data = new FormData();
        form_data.append('offset', offset);
        get_stream_posts(form_data,laroute.route('ajax.get-stream-posts'));
    }

    function get_stream_posts(data, action){
        $('.refresh-stream-posts-wrapper').remove();
        $.ajax({
            url: action,
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    console.log(response.posts);
                    $.each(response.posts, function(key, item) {
                        var post = stream_post_template(item);
                        $('#profile-my-streams').append(post);
                    });
                    if(response.posts.length >0)
                        $('#profile-my-streams').append('<div class="refresh-stream-posts-wrapper col-md-12 text-center v-mrgn-5"><button class="refresh-stream-posts btn btn-primary" data-offset="' + response.offset +'">Add more</button></div>');
                }
            }
        });
    }

    $(document).on('click', '.refresh-stream-posts', function (e) {
        e.preventDefault();
        var offset = $(this).data('offset');
        var form_data = new FormData();
        form_data.append('offset', offset);
        get_stream_posts(form_data, laroute.route('ajax.get-stream-posts'));
    });

    $(document).on('click', '.remove-from-stream',function (e) {
        e.preventDefault();
        var $target = $(e.target);
        var $parent = $target.parent();
        var actionUrl = '';
        var form_data = new FormData();
        form_data.append('postId', $(this).data('postId'));
        form_data.append('post_type', $(this).data('socialtype'));
        switch($(this).data('socialtype')){
            case 'facebook':
                actionUrl = laroute.route('ajax.remove-fbpost-from-userstream')
                break;
            case 'twitter':
                actionUrl = laroute.route('ajax.remove-twpost-from-userstream')
                break;
            case 'youtube':
                actionUrl = laroute.route('ajax.remove-ytpost-from-userstream')
                break;
            case 'google':
                actionUrl = laroute.route('ajax.remove-ggpost-from-userstream')
                break;
        }
        $.ajax({
            url: actionUrl,
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == 'success'){
                    var offset = $('.refresh-stream-posts').attr('data-offset');
                    $('.refresh-stream-posts').attr('data-offset',offset-1);
                    $target.closest('.stream-post').remove();
                }
            }
        });
    });

    /* users */
    function random_user_template(user){
        var template =  '<div class="random-user-acc row" data-user-id="'+user.id+'">'+
                            '<div class="col-sm-2">'+
                                '<img class="picture" src="'+user.avatarUrl+'">'+
                            '</div>'+
                            '<div class="col-sm-8">'+
                                '<p class="checkbox-label">'+user.name+'</p>'+
                            '</div>'+
                            '<div class="col-sm-2 profile-random-users-actions">'+
                                '<div style="overflow: auto">'+
                                    '<a type="button" href="'+laroute.route('profile-by-user',{id : user.id})+'" class="pull-right btn btn-success profile-random-user-view" data-user-id="'+user.id+'">view</a>'+
                                    '<a type="button" class="pull-right btn btn-success profile-random-user-follow" data-user-id="'+user.id+'">follow</a>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        return template;
    }

    function initialGetRandomUsers(){
        get_random_users(laroute.route('ajax.get-random-users-forfollow'));
    }


    function get_random_users(action){
        $.ajax({
            url: action,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.users, function(key, item) {
                        var user = random_user_template(item);
                        $('#profile-users').append(user);
                    });
                    $('#profile-users').append('<div class="refresh-random-users-wrapper text-center v-mrgn-10"><button class="refresh-random-users btn btn-primary" data-offset="' + response.offset +'">Refresh</button></div>');
                }
            }
        });
    }

    $(document).on('click', '.refresh-random-users', function (e) {
        e.preventDefault();
        $('#profile-users').empty();
        get_random_users(laroute.route('ajax.get-random-users-forfollow'));
    });

    $(document).on('click', '.profile-random-user-follow', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('user_id', $(this).data('userId'));
        $.ajax({
            url: laroute.route('ajax.follow-user'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if(response.status == 'success'){
                    $('div.random-user-acc[data-user-id="'+response.user_id+'"]').remove();
                    $('#profile-following').empty();
                    get_followed_users(laroute.route('ajax.get-followed-users'));

                }
            }
        });
    });

    /* followers */

    function follower_user_template(user){
        var template =  '<div class="follower-user-acc row" data-user-id="'+user.id+'">'+
            '<div class="col-sm-2">'+
            '<img class="picture" src="'+user.avatarUrl+'">'+
            '</div>'+
            '<div class="col-sm-8">'+
            '<p class="checkbox-label">'+user.name+'</p>'+
            '</div>'+
            '<div class="col-sm-2 profile-follower-users-actions">'+
            '<div style="overflow: auto">'+
            '<a type="button" href="'+laroute.route('profile-by-user',{id : user.id})+'" class="pull-right btn btn-success profile-follower-user-view" data-user-id="'+user.id+'">view</a>'+
            '<a type="button" class="pull-right btn btn-success profile-follower-user-follow '+((user.actions['follow'] == 'false')?'hidden':'')+'" data-user-id="'+user.id+'">follow</a>'+
            '<a type="button" class="pull-right btn btn-danger profile-follower-user-unfollow '+((user.actions['unfollow'] == 'false')?'hidden':'')+'" data-user-id="'+user.id+'">unfollow</a>'+
            '</div>'+
            '</div>'+
            '</div>';
        return template;
    }

    function initialGetFollowers(){
        get_followers(laroute.route('ajax.get-followers'));
    }


    function get_followers(action){
        $.ajax({
            url: action,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.users, function(key, item) {
                        var user = follower_user_template(item);
                        $('#profile-followers').append(user);
                    });
                }
            }
        });
    }

    $(document).on('click', '.profile-follower-user-follow', function (e) {
        var $target = $(e.target);
        var $parent = $target.parent();
        var form_data = new FormData();
        form_data.append('user_id', $(this).data('userId'));
        $.ajax({
            url: laroute.route('ajax.follow-user'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if(response.status == 'success'){
                    setFollowersButtons($parent,response.actions);
                    $('#profile-following').empty();
                    get_followed_users(laroute.route('ajax.get-followed-users'));
                }
            }
        });
    });

    $(document).on('click', '.profile-follower-user-unfollow', function (e) {
        var $target = $(e.target);
        var $parent = $target.parent();
        var form_data = new FormData();
        form_data.append('user_id', $(this).data('userId'));
        $.ajax({
            url: laroute.route('ajax.unfollow-user'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if(response.status == 'success'){
                    setFollowersButtons($parent,response.actions);
                    $('#profile-following').empty();
                    get_followed_users(laroute.route('ajax.get-followed-users'));
                }
            }
        });
    });

    function setFollowersButtons($buttonsContainer,actions){
        if(actions['follow'] == 'true'){
            $buttonsContainer.find('.profile-follower-user-follow').removeClass('hidden');
            $buttonsContainer.find('.profile-follower-user-unfollow').addClass('hidden');
        }
        else if(actions['unfollow'] == 'true'){
            $buttonsContainer.find('.profile-follower-user-follow').addClass('hidden');
            $buttonsContainer.find('.profile-follower-user-unfollow').removeClass('hidden');
        }
    }

    /* following */

    function followed_user_template(user){
        var template =  '<div class="followed-user-acc row" data-user-id="'+user.id+'">'+
            '<div class="col-sm-2">'+
            '<img class="picture" src="'+user.avatarUrl+'">'+
            '</div>'+
            '<div class="col-sm-8">'+
            '<p class="checkbox-label">'+user.name+'</p>'+
            '</div>'+
            '<div class="col-sm-2 profile-followed-users-actions">'+
            '<div style="overflow: auto">'+
            '<a type="button" href="'+laroute.route('profile-by-user',{id : user.id})+'" class="pull-right btn btn-success profile-followed-user-view" data-user-id="'+user.id+'">view</a>'+
            '<a type="button" class="pull-right btn btn-success profile-followed-user-follow '+((user.actions['follow'] == 'false')?'hidden':'')+'" data-user-id="'+user.id+'">follow</a>'+
            '<a type="button" class="pull-right btn btn-danger profile-followed-user-unfollow '+((user.actions['unfollow'] == 'false')?'hidden':'')+'" data-user-id="'+user.id+'">unfollow</a>'+
            '</div>'+
            '</div>'+
            '</div>';
        return template;
    }

    function initialGetFollowedUsers(){
        get_followed_users(laroute.route('ajax.get-followed-users'));
    }


    function get_followed_users(action){
        $.ajax({
            url: action,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.users, function(key, item) {
                        var user = follower_user_template(item);
                        $('#profile-following').append(user);
                    });
                }
            }
        });
    }

    $(document).on('click', '.profile-followed-user-follow', function (e) {
        var $target = $(e.target);
        var $parent = $target.parent();
        var form_data = new FormData();
        form_data.append('user_id', $(this).data('userId'));
        $.ajax({
            url: laroute.route('ajax.follow-user'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if(response.status == 'success'){
                    setFollowersButtons($parent,response.actions);
                    $('#profile-following').empty();
                    get_followed_users(laroute.route('ajax.get-followed-users'));
                }
            }
        });
    });

    $(document).on('click', '.profile-followed-user-unfollow', function (e) {
        var $target = $(e.target);
        var $parent = $target.parent();
        var form_data = new FormData();
        form_data.append('user_id', $(this).data('userId'));
        $.ajax({
            url: laroute.route('ajax.unfollow-user'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if(response.status == 'success'){
                    setFollowersButtons($parent,response.actions);
                    $('#profile-following').empty();
                    get_followed_users(laroute.route('ajax.get-followed-users'));
                }
            }
        });
    });

    function setFollowedUserButtons($buttonsContainer,actions){
        if(actions['follow'] == 'true'){
            $buttonsContainer.find('.profile-followed-user-follow').removeClass('hidden');
            $buttonsContainer.find('.profile-followed-user-unfollow').addClass('hidden');
        }
        else if(actions['unfollow'] == 'true'){
            $buttonsContainer.find('.profile-followed-user-follow').addClass('hidden');
            $buttonsContainer.find('.profile-followed-user-unfollow').removeClass('hidden');
        }
    }

    /* account management */

    profile_account_management_facebook = $("#profile-fb-social-accounts").DataTable({
        ajax: laroute.route('ajax.get-profile-facebook-accounts'),
        searching: false,
        "bLengthChange": false,
        columns: [
            {data: 'name'},
            {data: 'type'},
            {
                sortable: false,
                "render": function ( data, type, full, meta ) {
                    var buttonID = full.id;
                    var action = full.deleteAction
                    return '<div class="text-center"><a data-id='+buttonID+' data-action="'+action+'" data-social-type="facebook" class="btn btn-danger delete-social-acc" role="button">Delete</a></div>';
                }
            }
        ]
    } );

    profile_account_management_twitter = $("#profile-tw-social-accounts").DataTable({
        ajax: laroute.route('ajax.get-profile-twitter-accounts'),
        searching: false,
        "bLengthChange": false,
        columns: [
            {data: 'name'},
            {data: 'type'},
            {
                sortable: false,
                "render": function ( data, type, full, meta ) {
                    var buttonID = full.id;
                    var action = full.deleteAction
                    return '<div class="text-center"><a data-id='+buttonID+' data-action="'+action+'" data-social-type="twitter" class="btn btn-danger delete-social-acc" role="button">Delete</a></div>';
                }
            }
        ]
    } );

    profile_account_management_google = $("#profile-gg-social-accounts").DataTable({
        ajax: laroute.route('ajax.get-profile-google-accounts'),
        searching: false,
        "bLengthChange": false,
        columns: [
            {data: 'name'},
            {data: 'type'},
            {
                sortable: false,
                "render": function ( data, type, full, meta ) {
                    var buttonID = full.id;
                    var action = full.deleteAction
                    return '<div class="text-center"><a data-id='+buttonID+' data-action="'+action+'" data-social-type="google" class="btn btn-danger delete-social-acc" role="button">Delete</a></div>';
                }
            }
        ]
    } );

    $(document).on('click', '.delete-social-acc', function (event) {
        event.preventDefault();
        var modal = $('#confirmation-modal');
        var modal_submit = $('#confirmation-modal .submit');
        var target = $(event.target);
        modal_submit.attr('data-id',target.attr('data-id'));
        modal_submit.attr('data-social-type',target.attr('data-social-type'));
        modal_submit.attr('data-action',target.attr('data-action'));
        modal.modal('show');
    });

    $('#confirmation-modal .submit').click(function(event){
        var target = $(event.target);
        var form_data = new FormData();
        var table = '';
        switch(target.attr('data-social-type'))
        {
            case 'facebook':
                table = profile_account_management_facebook;
                break;
            case 'twitter':
                table = profile_account_management_twitter;
                break;
            case 'google':
                table = profile_account_management_google;
                break;
        }
        form_data.append('id', target.attr('data-id'));
        $.ajax({
            url: target.attr('data-action'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                $('#confirmation-modal').modal('hide');
                table.ajax.reload();
            }
        });
    });

    $('.profile-account-management-facebook-tab').on('click', function(){
        $('.add-social-acc').text('Add facebook account/page').attr('href', laroute.route('social.auth',{'provider':'facebook'}));
    });

    $('.profile-account-management-twitter-tab').on('click', function(){
        $('.add-social-acc').text('Add twitter account').attr('href', laroute.route('social.auth',{'provider':'twitter'}));
    });

    $('.profile-account-management-google-tab').on('click', function(){
        $('.add-social-acc').text('Add google account').attr('href', laroute.route('social.auth',{'provider':'google'}));
    });

    /* change avatar */

    $('#userAvatarInput').on('change', function(e){
        $target = $(e.target);
        var formData = new FormData();
        formData.append('image', $target[0].files[0]);
        $.ajax({
            url: laroute.route('ajax.upload-user-avatar'),
            type: 'POST',
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.status == 'success') {
                    toastr.success(data.success_msg);
                    $('.profile-user-img').prop('src',data.avatarPath)
                    $('.uploadUserAvatar').tooltip('hide');
                }
                else{
                    $.each(data.errors, function(key, item) {
                        toastr.warning(item);
                    });
                }
            }
        });
    });

});