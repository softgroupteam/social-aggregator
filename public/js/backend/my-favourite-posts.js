$( document ).ready(function() {


    intitalGetFavouritePosts()



    /*******************************************favourite posts************************/

    function favourite_post_template(post) {
        var template = '<div class="panel panel-default favourite-post favourite-post-'+post['social_type']+'-'+post['id']+'"  style="width:350px; display:inline-block; margin:5px;">' +
            '<div class="panel-body">' +
            ((post['embed_video_html'] != '')?'<div class="fb-post-embed-wrapper" >'+post['embed_video_html']+'</div>':((post['image_url'] != null)?'<img class="post-img" src="'+post['image_url']+'" style="width:100%">':'/img/mock/post.png')) +
            '<div class="box box-primary">' +
            ' <p class="text-blue" >'+post['social_created_at']+'</p>'+
            '<div class="box-header with-border">' +
            '<p class="box-title post-text" >' +
            post['message'] +
            '</p>' +
            '</div><!-- /.box-header -->' +
            '<div class="box-body text-center">' +
            '<button type="button" class="btn btn-success delete-favourite-post" data-post-id="'+ post['id'] + '" data-socialtype="'+ post['social_type'] +'">Delete from favourite</button>' +
            '<button type="button" data-social-type="'+post['social_type']+'" data-post-id="'+post['id']+'" class="btn btn-primary edit-in-composer" style="margin-left: 3px;">Edit</button>'+
            '<button type="button" class="btn btn-success publish-to-stream '+((post['actions']['publish_to_stream']=='false')?'hidden':'')+'" data-post-id="'+ post['id'] + '" data-socialtype="'+ post['social_type'] +'" style="margin-left: 3px;">Publish</button>' +
            '<button type="button" class="btn btn-danger remove-from-stream '+((post['actions']['unpublish_from_stream']=='false')?'hidden':'')+'" data-post-id="'+ post['id'] + '" data-socialtype="'+ post['social_type'] +'" style="margin-left: 3px;">Unpublish</button>' +
            '</div><!-- /.box-body -->' +
            '</div><!-- /.box -->' +
            '</div>' +
            '</div>';

        return template;
    }

    function intitalGetFavouritePosts(){
        get_favourite_posts(0,laroute.route('ajax.get-favourite-posts'));
    }



    function get_favourite_posts(data, action) {
        $('.refresh-favourite-users-posts-wrapper').remove();
        $.ajax({
            url: action,
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    console.log(response.posts);
                    $.each(response.posts, function(key, item) {
                        var post = favourite_post_template(item);
                        $('.user-favourite-posts-container').append(post);
                    });
                    if(response.posts.length >0)
                        $('.user-favourite-posts-container').append('<div class="refresh-favourite-users-posts-wrapper col-md-12 text-center v-mrgn-5"><button class="refresh-favourite-user-posts btn btn-primary" data-offset="' + response.offset +'">Add more</button></div>');
                }
            }
        });
    }

    $(document).on('click', '.refresh-favourite-user-posts', function (e) {
        e.preventDefault();
        var offset = $(this).data('offset');
        var form_data = new FormData();
        form_data.append('offset', offset);
        get_favourite_posts(form_data, laroute.route('ajax.get-favourite-posts'));
    });



    $(document).on('click', '.delete-favourite-post', function (e) {
        var $target = $(e.target);
        var $parent = $target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        form_data.append('post_type', $(this).data('socialtype'));
        $.ajax({
            url: laroute.route('ajax.delete-from-favourite'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == 'success'){
                    toastr.success(json.success_msg);
                    $('.favourite-post-'+json.post_type+'-'+json.post_id).remove();
                }
            }
        });
    });


    $(document).on('click', '.edit-in-composer',function (e) {
        e.preventDefault();
        var imageUrl = $(e.target).closest('.common-post-dashboard').find('.post-img').attr('src');
        var form = document.createElement("form");
        form.method = 'post';
        form.action = laroute.route('composer-load-post');
        var socialType = document.createElement('input');
        socialType.type = "text";
        socialType.name = "social_type";
        socialType.value = $(this).attr('data-social-type');
        form.appendChild(socialType);
        var postId = document.createElement('input');
        postId.type = "text";
        postId.name = "post_id";
        postId.value = $(this).attr('data-post-id');
        form.appendChild(postId);
        $('body').append(form);
        form.submit();
    });

    $(document).on('click', '.publish-to-stream ',function (e) {
        e.preventDefault();
        var $target = $(e.target);
        var $parent = $target.parent();
        var actionUrl = '';
        var form_data = new FormData();
        form_data.append('postId', $(this).data('postId'));
        form_data.append('post_type', $(this).data('socialtype'));
        switch($(this).data('socialtype')){
            case 'facebook':
                actionUrl = laroute.route('ajax.add-fbpost-to-userstream')
                break;
            case 'twitter':
                actionUrl = laroute.route('ajax.add-twpost-to-userstream')
                break;
            case 'youtube':
                actionUrl = laroute.route('ajax.add-ytpost-to-userstream')
                break;
            case 'google':
                actionUrl = laroute.route('ajax.add-ggpost-to-userstream')
                break;
        }
        $.ajax({
            url: actionUrl,
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == 'success'){
                    $parent.find('.publish-to-stream').addClass('hidden');
                    $parent.find('.remove-from-stream').removeClass('hidden');
                }
            }
        });
    });

    $(document).on('click', '.remove-from-stream',function (e) {
        e.preventDefault();
        var $target = $(e.target);
        var $parent = $target.parent();
        var actionUrl = '';
        var form_data = new FormData();
        form_data.append('postId', $(this).data('postId'));
        form_data.append('post_type', $(this).data('socialtype'));
        switch($(this).data('socialtype')){
            case 'facebook':
                actionUrl = laroute.route('ajax.remove-fbpost-from-userstream')
                break;
            case 'twitter':
                actionUrl = laroute.route('ajax.remove-twpost-from-userstream')
                break;
            case 'youtube':
                actionUrl = laroute.route('ajax.remove-ytpost-from-userstream')
                break;
            case 'google':
                actionUrl = laroute.route('ajax.remove-ggpost-from-userstream')
                break;
        }
        $.ajax({
            url: actionUrl,
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == 'success'){
                    $parent.find('.publish-to-stream').removeClass('hidden');
                    $parent.find('.remove-from-stream').addClass('hidden');
                }
            }
        });
    });

    function refreshFavouriteContainer(){
        $('#user-favourite-posts-container').empty();
        get_favourite_posts(laroute.route('ajax.get-favourite-posts'));
    }

    function refreshFollowedContainer() {
        $('#users-followed-posts-container').empty();
        get_followed_posts(laroute.route('ajax.get-followed-posts'));
    }

});