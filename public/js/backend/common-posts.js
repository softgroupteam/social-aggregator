$( document ).ready(function() {

    intitalGetPosts();

    function post_template(post) {
        var mediaBlock = '';
        if(post['type'] == 'image'){
            mediaBlock = '<img class="post-img" src="' + ((post['image_url'] != null)?post['image_url']:'/img/mock/post.png') + '" style="width:100%">';
        }
        if(post['type'] == 'video'){
            mediaBlock = '<div class="fb-post-embed-wrapper" >'+post['embed_video_html']+'</div>';
        }
        var template = '<div class="panel panel-default common-post-dashboard" style="width:350px; display:inline-block; margin:5px;">' +
                            '<div class="panel-body">' +
                                mediaBlock+
                                '<div class="box box-primary">' +
                                    ' <p class="text-blue" >'+post['social_created_at']+'</p>'+
                                        '<div class="box-header with-border">' +
                                            '<p class="box-title post-text" >' +
                                            post['message'] +
                                            '</p>' +
                                        '</div><!-- /.box-header -->' +
                                        '<div class="box-body">' +
                                            '<button type="button" class="btn btn-success save-fbpost '+ ((post['state'] == 'saved_published' || post['state'] == 'saved_notpublished')?'hidden':'')+'" data-post-id="'+ post['id'] + '" data-action="'+ post['save_action'] +'">Save</button>' +
                                            '<button type="button" class="btn btn-danger delete-fbpost '+ ((post['state'] == 'notsaved')?'hidden':'')+'" data-post-id="'+ post['id'] + '" data-action="'+ post['delete_action'] +'" style="margin-right: 3px;">Delete</button>' +
                                            '<button type="button" class="btn btn-info publish-fbpost '+ ((post['state'] == 'notsaved' || post['state'] == 'saved_published')?'hidden':'')+'" data-post-id="'+ post['id'] + '" data-action="'+ post['publish_action'] +'">Publish</button>' +
                                            '<button type="button" class="btn btn-info unpublish-fbpost '+ ((post['state'] == 'notsaved' || post['state'] == 'saved_notpublished')?'hidden':'')+'" data-post-id="'+ post['id'] + '" data-action="'+ post['unpublish_action'] +'">Unpublish</button>' +
                                            '<button type="button" data-social-type="'+post['social_type']+'" data-post-id="'+post['local_id']+'" class="btn btn-primary edit-in-composer" style="margin-left: 3px;">Edit</button>'+
                                        '</div><!-- /.box-body -->' +
                                '</div><!-- /.box -->' +
                            '</div>' +
                        '</div>';
        return template;
    }

    $(document).on('click', '.edit-in-composer',function (e) {
        e.preventDefault();
        var imageUrl = $(e.target).closest('.common-post-dashboard').find('.post-img').attr('src');
        var form = document.createElement("form");
        form.method = 'post';
        form.action = laroute.route('composer-load-post');
        var socialType = document.createElement('input');
        socialType.type = "text";
        socialType.name = "social_type";
        socialType.value = $(this).attr('data-social-type');
        form.appendChild(socialType);
        var postId = document.createElement('input');
        postId.type = "text";
        postId.name = "post_id";
        postId.value = $(this).attr('data-post-id');
        form.appendChild(postId);
        $('body').append(form);
        form.submit();
    });

    function intitalGetPosts(){
        var form_data = new FormData();
        form_data.append('offset', '0');
        get_posts(laroute.route('ajax.get-saved-posts-dashboard'), form_data);
    }

    function get_posts(action, form_data) {
        $('.add-more-block').remove();
        $.ajax({
            url: action,
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.posts, function(key, item) {
                        var post = post_template(item);
                        $('.content > .container').append(post);
                    });
                    $('.content > .container').append('<div class="add-more-block col-md-12 text-center"><button class="add-more-posts btn btn-default" data-offset="' + response.offset +'">Add more...</button></div>');
                }
            }
        });
    }


    $(document).on('click', '.add-more-posts', function (e) {
        e.preventDefault();
        var offset = $(this).data('offset');

        var form_data = new FormData();
        form_data.append('offset', offset);
        get_posts(laroute.route('ajax.get-saved-posts-dashboard'), form_data);
    });

    $(document).on('click', '.save-fbpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: $(this).data('action'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.publish-fbpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: $(this).data('action'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.delete-fbpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: $(this).data('action'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    $(document).on('click', '.unpublish-fbpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: $(this).data('action'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
            }
        });
    });

    function set_buttons_state(state,parent){

        switch(state){
            case 'saved_notpublished':
                $(parent.find('.publish-fbpost')).removeClass('hidden');
                $(parent.find('.save-fbpost')).addClass('hidden');
                $(parent.find('.delete-fbpost')).removeClass('hidden');
                $(parent.find('.unpublish-fbpost')).addClass('hidden');
                break;
            case 'saved_published':
                $(parent.find('.publish-fbpost')).addClass('hidden');
                $(parent.find('.save-fbpost')).addClass('hidden');
                $(parent.find('.delete-fbpost')).removeClass('hidden');
                $(parent.find('.unpublish-fbpost')).removeClass('hidden');
                break;
            case 'notsaved':
                $(parent.find('.publish-fbpost')).addClass('hidden');
                $(parent.find('.save-fbpost')).removeClass('hidden');
                $(parent.find('.delete-fbpost')).addClass('hidden');
                $(parent.find('.unpublish-fbpost')).addClass('hidden');
                break;
        }
    }
});