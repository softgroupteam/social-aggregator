$( document ).ready(function() {



    function post_template(post) {
        var template = '<div class="panel panel-default " style="width:350px; display:inline-block; margin:5px;">' +
            '<div class="panel-body">' +
            ((post.embed_video_html != '')?'<div class="fb-post-embed-wrapper" >'+post.embed_video_html+'</div>':((post.full_picture != undefined)?'<img src="' + post.full_picture + '" style="width:100%">':'')) +
            '<div class="box box-primary">' +
                    '<p class="text-blue" >'+post.createdLocal+'</p>'+
                    '<div class="fb-post-statistics">'+
                        '<span><i class="fa fa-share-square-o"></i>'+((post.shares != undefined)?post.shares.count:'0')+'</span>'+
                        '<span><i class="fa fa-comment"></i>'+post.comments.summary.total_count+'</span>'+
                        '<span><i class="fa fa-thumbs-up"></i>'+post.likes.summary.total_count+'</span>'+
                    '</div>'+
                    '<div class="box-header with-border">' +
                        '<p class="box-title post-text" >' +
                        ((post.message != undefined)?post.message:'')+((post.story != undefined)?post.story:'')+
                        ((post.link != undefined)?' <a href="'+post.link+'">'+post.link+'</a>':'')+
                        '</p>' +
                    '</div><!-- /.box-header -->' +
                    '<div class="box-body">' +
                        '<button type="button" class="btn btn-success save-addfavourite-fbpost '+ ((post.state == 'saved_favourite')?'hidden':'')+'" data-post-id="'+ post.id +'">Save to favourite</button>' +
                        '<button type="button" class="btn btn-danger delete-removefavourite-fbpost '+ ((post.state == 'notsaved_notfavourite')?'hidden':'')+'" data-post-id="'+ post.id +'" style="margin-right: 3px;">Delete from favourite</button>' +
                    '</div><!-- /.box-body -->' +
                    '</div><!-- /.box -->' +
                '</div>' +
            '</div>';

        return template;
    }

    // function convertTimestamp(timestamp) {
    //     var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
    //         yyyy = d.getFullYear(),
    //         mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
    //         dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
    //         hh = ('0' + d.getHours()).slice(-2),
    //         min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
    //         sec = ('0' + d.getSeconds()).slice(-2),
    //         time;
    //
    //     time = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;
    //
    //     return time;
    // }

    function get_posts(action, form_data) {
        $('.add-more-block').remove();
        $.ajax({
            url: action,
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.posts, function(key, item) {
                        var post = post_template(item);
                        $('.content > .container').append(post);
                    });
                    $('.content > .container').append('<div class="add-more-block col-md-12 text-center"><button class="add-more-posts btn btn-default" data-action="' + action + '" data-nextpage="' + response.nextPage + '">Add more...</button></div>');
                }
            }
        });
    }


    $(document).on('click', '.add-more-posts', function (e) {
        e.preventDefault();
        var action = $(this).data('action');
        var nextPage = $(this).data('nextpage');

        var form_data = new FormData();
        form_data.append('nextPage', nextPage);
        get_posts(action, form_data);
    });

    $(document).on('click', '.save-addfavourite-fbpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        blockButtons(parent);
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.add-favourite-fbpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
                releaseButtons(parent);
            }
        });
    });

    $(document).on('click', '.delete-removefavourite-fbpost', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        blockButtons(parent);
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        $.ajax({
            url: laroute.route('ajax.delete-favourite-fbpost'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == '1'){
                    set_buttons_state(json.state,parent);
                }
                releaseButtons(parent);
            }
        });
    });

    function set_buttons_state(state,parent){
        if(state == 'saved_favourite'){
            $(parent.find('.save-addfavourite-fbpost')).addClass('hidden');
            $(parent.find('.delete-removefavourite-fbpost')).removeClass('hidden');
        }
        else if('notsaved_notfavourite'){
            $(parent.find('.save-addfavourite-fbpost')).removeClass('hidden');
            $(parent.find('.delete-removefavourite-fbpost')).addClass('hidden');
        }
    }

    function blockButtons(parent){
        $(parent.find('.save-addfavourite-fbpost')).prop( "disabled", true );
        $(parent.find('.delete-removefavourite-fbpost')).prop( "disabled", true );
    }

    function releaseButtons(parent){
        $(parent.find('.save-addfavourite-fbpost')).prop( "disabled", false );
        $(parent.find('.delete-removefavourite-fbpost')).prop( "disabled", false );
    }
});