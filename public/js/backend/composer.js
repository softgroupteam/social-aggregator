$( document ).ready(function() {
    $("#composer-post-image").fileinput();

   // $('.composer-remote-video-wrapper').append('<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FFunnyVideosbyDiply%2Fvideos%2F2083811528336119%2F&width=720" width="720" height="720" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>');

    if($('input[name="social_schedule_check"]:checked').val() == 'schedule')
        $('#schedule-form').removeClass('hidden');

    $('#publish-composer-post').on('click',function (e) {
        e.preventDefault();
        $('#publish-composer-post .spinner-stack').toggleClass('hidden');
        $('#publish-composer-post span').toggleClass('hidden');
        var formData = new FormData($('#composer-post-form')[0]);
        formData.append('post_type', $(this).attr('data-post-type'));
        formData.append('video_html', $('.composer-remote-video-inner-wrapper').html());
        $.ajax({
            url: laroute.route('ajax.composer-save-post'),
            type: 'POST',
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                $('#publish-composer-post .spinner-stack').toggleClass('hidden');
                $('#publish-composer-post span').toggleClass('hidden');
                if(data.status == 'success') {
                    toastr.success(data.success_msg);
                    restoreInitialComposerState();
                    scheduled_posts_table.ajax.reload();
                }
                else{
                    $.each(data.errors, function(key, item) {
                        toastr.warning(item);
                    });
                }
            }
        });
    })

    $('input[name="social_schedule_check"]').on('change',function(){
        if($(this).val() == 'schedule')
            $('#schedule-form').removeClass('hidden');
        else
            $('#schedule-form').addClass('hidden');
    });

    /*datepicker init*/
    $('#post-date').datepicker({
        autoclose: true,
        startDate: "now",
        todayHighlight: true
    });



    /* scheduled posts */

    scheduled_posts_table = $("#scheduled-posts-table").DataTable({
        ajax: laroute.route('ajax.get-scheduled-posts'),
        searching: false,
        "bInfo" : false,
        "bLengthChange": false,
        columns: [
            {
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                sortable: false,
                "render": function ( data, type, full, meta ) {
                    var buttonID = full.id;
                    return '<input type="checkbox" class="text-center scheduled-post-checkbox" value="'+full.id+'"></input>';
                }
            },
            {
                sortable: false,
                "render": function ( data, type, full, meta ) {
                    if(full.image_path != '') {
                        var buttonID = full.id;
                        return '<img src="' + full.image_path + '" style="max-width:100%;">';
                    }
                    else
                        return '';
                }
            },
            {data: 'message'},
            {data: 'time'},
            {
                sortable: false,
                "render": function ( data, type, full, meta ) {
                    var buttonID = full.id;
                    return '<div class="text-center v-mrgn-5"><a  class="btn btn-info edit-scheduled-post" role="button" data-id="'+buttonID+'">Edit</a></div>'+
                    '<div class="text-center"><a data-id='+buttonID+' data-social-type="facebook" class="btn btn-danger delete-scheduled-post" role="button">Delete</a></div>';

                }
            }
        ]
    } );

    $(document).on('click', '.edit-scheduled-post', function (e) {
        $target = $(e.target);
        var form_data = new FormData();
        form_data.append('postId', $target.attr('data-id'));
        $.ajax({
            url: laroute.route('ajax.get-scheduled-post-info'),
            type: 'POST',
            data: form_data,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                restoreInitialComposerState();
                $('#publish-composer-post').addClass('hidden');
                $('#edit-composer-post').attr('data-post-id',data.id).removeClass('hidden');
                $('#cancel-edit-composer-post').removeClass('hidden');


                $.each( data.socialLogins, function( key, value ){
                   switch(value.provider){
                       case 'facebook':
                           $('input[name="fbaccounts['+value.id+']"]').prop('checked', true);
                       case 'twitter':
                           $('input[name="twaccounts['+value.id+']"]').prop('checked', true);
                   }
                });
                $.each( data.socialFbPages, function( key, value ){
                    $('input[name="fbsocialpages['+value.id+']"]').prop('checked', true);
                });
                $('#social_schedule').prop('checked', true).trigger('change');
                $('#message-text').val(data.message);

                if(data.post_type == 'video'){
                    $('.composer-remote-video-inner-wrapper').html(data.video_html);
                    $('.composer-remote-video-wrapper').removeClass('hidden');
                    $('.composer-post-image-wrapper').addClass('hidden');
                    $('#edit-composer-post').attr('data-post-type','video');
                }
                else {
                    $('#edit-composer-post').attr('data-post-type', 'image');
                    if (data.image != '') {
                        $('#edited-post-image').prop('src', data.image);
                        $('#edited-image-url').prop('value', data.image);
                        $('.edited-post-image-wrapper').removeClass('hidden');
                        $('.image-changebutton-wrapper').removeClass('hidden');
                        $('.composer-post-image-wrapper').addClass('hidden');
                    }
                }
                $('#post-date').val(data.date);
                $('#scheduler-hours').val(data.hour);
                $('#scheduler-minutes').val(data.minute);
            }
        });
    });

    $('#post-image-changebutton, .remove-image').on('click', function(e){
        e.preventDefault();
        $('.composer-remote-image-wrapper').addClass('hidden');
        $('.edited-post-image-wrapper').addClass('hidden');
        $('#remote-image-url').val('');
        $('.composer-post-image-wrapper').removeClass('hidden');
        $('.image-changebutton-wrapper').addClass('hidden');
        clearPostImage();
    });

    function clearPostImage(){
        $fileinput = $('#composer-post-image');
        $('input[name="remote-image-url"]').attr('value', '');
        $('input[name="edited-image-url"]').attr('value', '');
        if ($fileinput.data('fileinput')) {
            $fileinput.fileinput('refresh');
        }
    }

    $('#edit-composer-post').on('click', function(e){
        e.preventDefault();
        $target = $(e.target);
        var formData = new FormData($('#composer-post-form')[0]);
        formData.append('postId', $target.attr('data-post-id'));
        $.ajax({
            url: laroute.route('ajax.edit-scheduled-post'),
            type: 'POST',
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if(data.status == 'success') {
                    toastr.success(data.success_msg);
                    restoreInitialComposerState();
                    scheduled_posts_table.ajax.reload();
                }
                else{
                    $.each(data.errors, function(key, item) {
                        toastr.warning(item);
                    });
                }
            }
        });
    });

    $('#cancel-edit-composer-post').on('click', function(e){
        e.preventDefault();
        restoreInitialComposerState();
    });

    $(document).on('click', '.delete-scheduled-post', function (e) {
        event.preventDefault();
        var modal = $('#confirmation-modal');
        var modal_submit = $('#confirmation-modal .submit');
        var target = $(event.target);
        modal_submit.attr('data-id',target.attr('data-id'));
        modal_submit.attr('data-action',laroute.route('ajax.delete-scheduled-post'));
        modal.modal('show');
    });

    $('#confirmation-modal .submit').click(function(e){
        $target = $(e.target);
        var form_data = new FormData();
        form_data.append('postId', $target.attr('data-id'));
        $.ajax({
            url: $target.attr('data-action'),
            type: 'POST',
            data: form_data,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.status == 'success') {
                    toastr.success(data.success_msg);
                    scheduled_posts_table.ajax.reload();
                    $('#confirmation-modal').modal('hide');
                }
            }
        });
    });

    $('#select-all-scheduled-posts').on('change',function(e) {
        $target = $(e.target);
        if($target.prop("checked")){
            $('.scheduled-post-checkbox').prop('checked',true);
            $('#delete-selected-scheduled-posts').removeClass('hidden');
        }
        else{
            $('.scheduled-post-checkbox').prop('checked',false);
            $('#delete-selected-scheduled-posts').addClass('hidden');
        }
    });

    $(document).on('click','.scheduled-post-checkbox',function() {
        if($('.scheduled-post-checkbox:checked').length == 0){
            $('#delete-selected-scheduled-posts').addClass('hidden');
        }
        else{
            $('#delete-selected-scheduled-posts').removeClass('hidden');
        }
    });


    $('#delete-selected-scheduled-posts').on('click',function(){
        event.preventDefault();
        var modal = $('#confirmation-modal');
        var modal_submit = $('#confirmation-modal .submit');
        var target = $(event.target);
        var postIds = [];
        $('.scheduled-post-checkbox:checked').each(function(){
            postIds.push($(this).val());
        });
        var postIdsSerialized = JSON.stringify(postIds);
        modal_submit.attr('data-id',postIdsSerialized);
        modal_submit.attr('data-action',laroute.route('ajax.massive-delete-scheduled-posts'));
        modal.modal('show');
    });

    function restoreInitialComposerState(){
        $('#publish-composer-post').removeClass('hidden');
        $('#edit-composer-post').addClass('hidden');
        $('#cancel-edit-composer-post').addClass('hidden');
        $('.composer-post-image-wrapper').removeClass('hidden');
        $('.image-changebutton-wrapper').addClass('hidden');
        $('.composer-remote-image-wrapper').addClass('hidden');
        $('.composer-remote-video-wrapper').addClass('hidden');
        $('#remote-image-url').val('');
        $('#remote-video-link').val('');
        $('#remote-original-link').val('');
        $('ul.composer-profiles input').prop('checked',false);
        $('#social_post_now').prop('checked', true).trigger('change');
        $('#message-text').val('');
        $('.edited-post-image-wrapper').addClass('hidden');
        $('#post-date').val('');
        $('#scheduler-hours').val('HH');
        $('#scheduler-minutes').val('MM');
        clearPostImage();
    }
});
