$( document ).ready(function() {
     facebook_streams_table = $("#facebook-streams-table").DataTable({
            ajax: laroute.route('ajax.get-facebook-streams'),
        columns: [
        {
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        {data: 'machine_name'},
        {data: 'title'},
        {
            sortable: false,
            "render": function ( data, type, full, meta ) {
                var buttonID = full.id;
                return '<div class="text-center"><a data-id='+buttonID+' data-social-type="facebook" class="btn btn-danger delete-social-stream" role="button">Delete</a></div>'+
                '<div class="text-center v-mrgn-5"><a href="'+laroute.route('get-posts-by-page',{pageTitle : full.machine_name})+'" class="btn btn-info view-social-stream" role="button">View</a></div>';
            }
        }
    ]
    } );

     twitter_streams_table = $("#twitter-streams-table").DataTable({
        ajax: laroute.route('ajax.get-twitter-streams'),
        columns: [
        {
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        {data: 'page_name'},
        {data: 'title'},
        {
            sortable: false,
            "render": function ( data, type, full, meta ) {
                var buttonID = full.id;
                return '<div class="text-center"><a data-id='+buttonID+' data-social-type="twitter" class="btn btn-danger delete-social-stream" role="button">Delete</a></div>'+
                    '<div class="text-center v-mrgn-5"><a href="'+laroute.route('get-tweets-by-user',{userName: full.page_name})+'" class="btn btn-info view-social-stream" role="button">View</a></div>';
            }
        }
    ]
    } );

     youtube_streams_table =  $("#youtube-streams-table").DataTable({
        ajax: laroute.route('ajax.get-youtube-streams'),
        columns: [
        {
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        {data: 'channel_id'},
        {data: 'title'},
        {
            sortable: false,
            "render": function ( data, type, full, meta ) {
                var buttonID = full.id;
                return '<div class="text-center"><a data-id='+buttonID+' data-social-type="youtube" class="btn btn-danger delete-social-stream" role="button">Delete</a></div>'+
                '<div class="text-center v-mrgn-5"><a href="'+laroute.route('get-feed-by-youtubeChannel',{channelId : full.channel_id})+'" class="btn btn-info view-social-stream" role="button">View</a></div>';
            }
        }
    ]
    } );

     google_streams_table = $("#google-streams-table").DataTable({
        ajax: laroute.route('ajax.get-google-streams'),
        columns: [
        {
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        {data: 'channel_id'},
        {data: 'title'},
        {
            sortable: false,
            "render": function ( data, type, full, meta ) {
                var buttonID = full.id;
                return '<div class="text-center"><a data-id='+buttonID+' data-social-type="google_plus" class="btn btn-danger delete-social-stream" role="button">Delete</a></div>'+
                    '<div class="text-center v-mrgn-5"><a href="'+laroute.route('google-plus')+'?channelId='+encodeURIComponent(full.channel_id)+'" class="btn btn-info view-social-stream" role="button">View</a></div>';
            }
        }
    ]
    } );

    $(document).on('click', '.delete-social-stream',function (e) {
        e.preventDefault();
        var modal = $('#delete-socialtab-modal');
        var modal_submit = $('#submit-tab-delete');
        var target = $(e.target);
        modal_submit.attr('data-id',target.data('id'));
        modal_submit.attr('data-social',target.data('socialType'));
        modal.modal('show');
    });

    $('.add-stream').on('click', function(e){
        e.preventDefault();
        blockAddButton();
        var action = '';
        var data = '';
        var table = {};
        switch($(this).data('socialType')){
            case 'facebook':
                action = laroute.route('ajax.add-facebook-stream');
                data = new FormData($('#add-facebook-stream-form')[0]);
                table = facebook_streams_table;
                break;
            case 'twitter':
                action = laroute.route('ajax.add-twitter-stream');
                data = new FormData($('#add-twitter-stream-form')[0]);
                table = twitter_streams_table;
                break;
            case 'youtube':
                action = laroute.route('ajax.add-youtube-stream');
                data = new FormData($('#add-youtube-stream-form')[0]);
                table = youtube_streams_table;
                break;
            case 'google':
                action = laroute.route('ajax.add-google-stream');
                data = new FormData($('#add-google-stream-form')[0]);
                table = google_streams_table;
                break;
        }
        $.ajax({
            method: "POST",
            url: action,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function(msg){
                if(msg.status == 'success') {
                    toastr.success(msg.success_msg);
                    clear_stream_forms();
                    table.ajax.reload();
                }
                else{
                    $.each(msg.errors, function(key, item) {
                        toastr.warning(item);
                    });
                }
                releaseAddButton();
            }
        });
    });

    function clear_stream_forms(){
        $('#yt-stream-url').val('');
        $('#yt-stream-title').val('');
        $('#fb-stream-url').val('');
        $('#fb-stream-title').val('');
        $('#gg-stream-url').val('');
        $('#gg-stream-title').val('');
        $('#tw-stream-url').val('');
        $('#tw-stream-title').val('');
    }

    function blockAddButton(){
        $('.add-stream').prop('disabled',true);
    }

    function releaseAddButton(){
        $('.add-stream').prop('disabled',false);
    }
});