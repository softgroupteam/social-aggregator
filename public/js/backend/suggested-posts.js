$( document ).ready(function() {

    intitalGetPosts();

    function post_template(post) {
        var template = '<div class="panel panel-default suggested-post-'+ post['social_type']+'-'+post['id']+'" style="width:350px; display:inline-block; margin:5px;">' +
            '<div class="panel-body">' +
            '<img class="post-img" src="' + ((post['image_url'] != null)?post['image_url']:'/img/mock/post.png') + '" style="width:100%">' +
            '<div class="box box-primary">' +
            ' <p class="text-blue" >'+post['social_created_at']+'</p>'+
            '<div class="box-header with-border">' +
            '<p class="box-title post-text" >' +
            post['message'] +
            '</p>' +
            '</div><!-- /.box-header -->' +
            '<div class="box-body text-center">' +
            '<button type="button" class="btn btn-success add-favourite-post" data-post-id="'+ post['id'] + '" data-socialtype="'+ post['social_type'] +'">Add to favourite</button>' +
            '</div><!-- /.box-body -->' +
            '</div><!-- /.box -->' +
            '</div>' +
            '</div>';

        return template;
    }

    function intitalGetPosts(){
        var form_data = new FormData();
        form_data.append('offset', '0');
        $('#get-suggested-fbposts').addClass('active');
        $('#suggested-fbposts-container').removeClass('hidden');
        get_suggested_fbposts(form_data);
    }

    $('#get-suggested-fbposts').on('click',function(){
        $('#get-suggested-fbposts').addClass('active');
        $('#get-suggested-twposts').removeClass('active');
        $('#get-suggested-ytposts').removeClass('active');
        $('#get-suggested-ggposts').removeClass('active');
        $('#suggested-twposts-container').addClass('hidden');
        $('#suggested-ggposts-container').addClass('hidden');
        $('#suggested-ytposts-container').addClass('hidden');
        $('#suggested-fbposts-container').removeClass('hidden');
       if ( $('#suggested-fbposts-container').children().length == 0 ) {
           var form_data = new FormData();
           form_data.append('offset', '0');
           get_suggested_fbposts(form_data);
       }
    });

    function get_suggested_fbposts(form_data) {
        $('.add-more-block').remove();
        $.ajax({
            url: laroute.route('ajax.get-suggested-fbposts'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.posts, function(key, item) {
                        var post = post_template(item);
                        $('#suggested-fbposts-container').append(post);
                    });
                    $('#suggested-fbposts-container').append('<div class="add-more-block width-100 text-center"><button class="add-more-suggested-fbposts btn btn-default" data-offset="' + response.offset +'">Add more...</button></div>');
                }
            }
        });
    }


    $(document).on('click', '.add-more-suggested-fbposts', function (e) {
        e.preventDefault();
        var offset = $(this).data('offset');

        var form_data = new FormData();
        form_data.append('offset', offset);
        get_suggested_fbposts(form_data);
    });

    $('#get-suggested-twposts').on('click',function(){
        $('#get-suggested-fbposts').removeClass('active');
        $('#get-suggested-twposts').addClass('active');
        $('#get-suggested-ytposts').removeClass('active');
        $('#get-suggested-ggposts').removeClass('active');
        $('#suggested-twposts-container').removeClass('hidden');
        $('#suggested-ggposts-container').addClass('hidden');
        $('#suggested-ytposts-container').addClass('hidden');
        $('#suggested-fbposts-container').addClass('hidden');
        if ( $('#suggested-twposts-container').children().length == 0 ) {
            var form_data = new FormData();
            form_data.append('offset', '0');
            get_suggested_twposts(form_data);
        }
    });

    function get_suggested_twposts(form_data) {
        $('.add-more-block').remove();
        $.ajax({
            url: laroute.route('ajax.get-suggested-twposts'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.posts, function(key, item) {
                        var post = post_template(item);
                        $('#suggested-twposts-container').append(post);
                    });
                    $('#suggested-twposts-container').append('<div class="add-more-block width-100 text-center"><button class="add-more-suggested-twposts btn btn-default" data-offset="' + response.offset +'">Add more...</button></div>');
                }
            }
        });
    }


    $(document).on('click', '.add-more-suggested-twposts', function (e) {
        e.preventDefault();
        var offset = $(this).data('offset');

        var form_data = new FormData();
        form_data.append('offset', offset);
        get_suggested_twposts(form_data);
    });

    $('#get-suggested-ytposts').on('click',function(){
        $('#get-suggested-fbposts').removeClass('active');
        $('#get-suggested-twposts').removeClass('active');
        $('#get-suggested-ytposts').addClass('active');
        $('#get-suggested-ggposts').removeClass('active');
        $('#suggested-twposts-container').addClass('hidden');
        $('#suggested-ggposts-container').addClass('hidden');
        $('#suggested-ytposts-container').removeClass('hidden');
        $('#suggested-fbposts-container').addClass('hidden');
        if ( $('#suggested-ytposts-container').children().length == 0 ) {
            var form_data = new FormData();
            form_data.append('offset', '0');
            get_suggested_ytposts(form_data);
        }
    });

    function get_suggested_ytposts(form_data) {
        $('.add-more-block').remove();
        $.ajax({
            url: laroute.route('ajax.get-suggested-ytposts'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.posts, function(key, item) {
                        var post = post_template(item);
                        $('#suggested-ytposts-container').append(post);
                    });
                    $('#suggested-ytposts-container').append('<div class="add-more-block width-100 text-center"><button class="add-more-suggested-ytposts btn btn-default" data-offset="' + response.offset +'">Add more...</button></div>');
                }
            }
        });
    }


    $(document).on('click', '.add-more-suggested-ytposts', function (e) {
        e.preventDefault();
        var offset = $(this).data('offset');

        var form_data = new FormData();
        form_data.append('offset', offset);
        get_suggested_ytposts(form_data);
    });

    $('#get-suggested-ggposts').on('click',function(){
        $('#get-suggested-fbposts').removeClass('active');
        $('#get-suggested-twposts').removeClass('active');
        $('#get-suggested-ytposts').removeClass('active');
        $('#get-suggested-ggposts').addClass('active');
        $('#suggested-twposts-container').addClass('hidden');
        $('#suggested-ggposts-container').removeClass('hidden');
        $('#suggested-ytposts-container').addClass('hidden');
        $('#suggested-fbposts-container').addClass('hidden');
        if ( $('#suggested-ggposts-container').children().length == 0 ) {
            var form_data = new FormData();
            form_data.append('offset', '0');
            get_suggested_ggposts(form_data);
        }
    });

    function get_suggested_ggposts(form_data) {
        $('.add-more-block').remove();
        $.ajax({
            url: laroute.route('ajax.get-suggested-ggposts'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.posts, function(key, item) {
                        var post = post_template(item);
                        $('#suggested-ggposts-container').append(post);
                    });
                    $('#suggested-ggposts-container').append('<div class="add-more-block width-100 text-center"><button class="add-more-suggested-ggposts btn btn-default" data-offset="' + response.offset +'">Add more...</button></div>');
                }
            }
        });
    }


    $(document).on('click', '.add-more-suggested-ggposts', function (e) {
        e.preventDefault();
        var offset = $(this).data('offset');

        var form_data = new FormData();
        form_data.append('offset', offset);
        get_suggested_ggposts(form_data);
    });

    $(document).on('click', '.add-favourite-post', function (e) {
        var target = $(e.target);
        var parent = target.parent();
        var form_data = new FormData();
        form_data.append('post_id', $(this).data('postId'));
        form_data.append('post_type', $(this).data('socialtype'));
        $.ajax({
            url: laroute.route('ajax.add-to-favourite'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(json){
                console.log(json);
                if(json.status == 'success'){
                    toastr.success(json.success_msg);
                    $('.suggested-post-'+json.post_type+'-'+json.post_id).remove();
                    switch(json.post_type){
                        case 'facebook':
                            var offset = parseInt($('.add-more-suggested-fbposts').attr('data-offset'));
                            $('.add-more-suggested-fbposts').attr('data-offset',offset-1);
                        case 'twitter':
                            var offset = parseInt($('.add-more-suggested-twposts').attr('data-offset'));
                            $('.add-more-suggested-twposts').attr('data-offset',offset-1);
                        case 'google':
                            var offset = parseInt($('.add-more-suggested-ggposts').attr('data-offset'));
                            $('.add-more-suggested-ggposts').attr('data-offset',offset-1);
                        case 'youtube':
                            var offset = parseInt($('.add-more-suggested-ytposts').attr('data-offset'));
                            $('.add-more-suggested-ytposts').attr('data-offset',offset-1);
                    }
                }
            }
        });
    });
});