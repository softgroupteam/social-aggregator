$( document ).ready(function() {


    function post_template(post) {
        var template =
        '<div class="social-stream-item '+post['filter_class']+'" data-created-timestamp="'+post['created_at_timestamp']+'">'+
            '<div class="social-stream-header">'+
                '<a href="#">'+
                    '<img src="'+((post['image_url'] != null)?post['image_url']:'img/mock/post.png')+'" style="width:100%">'+
                '</a>'+
                '<div class="overlay open-popup-link" data-id="'+post['id']+'" data-type="'+post['type']+'" data-imgurl="'+post['image_url']+'" '+((post['embed_url']!= undefined)?'data-embedurl="'+post['embed_url']+'"':'')+' data-readmore="'+post['readmore_url']+'">'+
                    '<div style="text-center">'+
                        '<span class="fa fa-facebook-square fa-2x" style="margin:5px;"></span>'+
                        '<span class="fa fa-twitter-square fa-2x" style="margin:5px;"></span>'+
                        '<span class="fa fa-youtube-square fa-2x " style="margin:5px;"></span>'+
                        '<span class="fa fa-google-plus-square fa-2x" style="margin:5px;"></span>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="social-stream-footer">'+
                '<p class="social-stream-time text-center">'+ post['social_created_at'] +
                '</p>'+
                '<div class="social-stream-message">'+
                    '<p class="box-title post-text">'+
                    post['message']+
                    '</p>'+
                '</div>'+
                '<p class="text-center"><a class="btn btn-primary read-more" href="'+post['readmore_url']+'">Read more ></a></p>'+
            '</div>'+
        '</div>';

        return template;
    }

    function get_posts(form_data) {
        $('.add-more-published-block').remove();
        $.ajax({
            url: laroute.route('ajax.get-published-posts'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function (json) {
                var response = jQuery.parseJSON(json);
                if (response.status) {
                    $.each(response.posts, function (key, item) {
                        var $post = $(post_template(item));
                        $post.imagesLoaded( function() {
                            $('.social-stream-container').append($post);
                            $('.social-stream-container').isotope( 'appended', $post);
                            $('.social-stream-container').isotope('updateSortData').isotope();
                        });
                    });
                    if(response.posts.length > 0)
                        $('.uss-container').append('<div class="add-more-published-block col-md-12 text-center"><button class="add-more-published-posts btn btn-default"  data-offset="' + response.offset + '">Load more</button></div>');
                }
            }
        });
    }


    $(document).on('click', '.add-more-published-posts', function (e) {
        e.preventDefault();
        var offset = $(this).data('offset');
        var form_data = new FormData();
        form_data.append('offset', offset);
        get_posts(form_data);
    });

    function intitalGetPosts(){
        var tz = jstz.determine();
        document.cookie = "user_time_zone="+tz.name()+"; path=/;";
        var form_data = new FormData();
        form_data.append('offset', '0');
        get_posts(form_data);
    }






    //isotope

    $grid = $('.social-stream-container').isotope({
        // options
        itemSelector: '.social-stream-item',
        masonry: {
            columnWidth: 40,
            isFitWidth: true
        },
        getSortData: {
            time: function( itemElem ) {
                var time = $( itemElem ).attr('data-created-timestamp');
                return parseInt(time);
            }
        },
        sortBy: [ 'time' ],
        sortAscending: false
    });

    $('.filters-button-group').on( 'click', 'button', function() {
        var filterValue = $( this ).attr('data-filter');
        $('.social-stream-container').isotope({ filter: filterValue });
    });

    //start getting posts
    intitalGetPosts();


    //popup set

    function wrapUrlDedicatedWindow(str){
        return result = "javascript:window.open('"+str+"','_blank','width=400,height=500');";
    }

    $(document).on('click', '.btn-share', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        window.open(href,'_blank','width=400,height=500');
    });



    $(document).on('click', '.open-popup-link', function (e) {
        e.preventDefault();
        var header_template = '';
        var type = $(this).data('type');
        var img_url = $(this).data('imgurl');
        var post_text = $(this).closest('.social-stream-item').find('.post-text').html();
        var share_fb = 'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent($(this).data('readmore'))+'&title=';
        var share_tw = 'https://twitter.com/intent/tweet?url='+encodeURIComponent($(this).data('readmore'))+'&text=';
        var share_gg = 'https://plus.google.com/share?url='+encodeURIComponent($(this).data('readmore'));
        if(type == 'image'){
            header_template = '<img src="'+((img_url != null)?img_url:'img/mock/post.png')+'" style="width:100%">';
        }

        if(type == 'video'){
            header_template = '<div class="video-container">'+
                                    '<iframe width=100%" frameborder="0" allowfullscreen class="video"'+
                                        'src="'+$(this).data('embedurl')+'">'+
                                    '</iframe>'+
                                '</div>';
        }

        var template = '<div class="social-stream-popup" style="">'+
                            '<div class="popup-header">'+
                                header_template+
                                '<div class="social-share-buttons">'+
                                    '<div class="social-share-link-wr">'+
                                        '<a href="'+share_fb+'" class="btn btn-lg btn-block btn-share btn-share-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">'+
                                            '<i class="fa fa-facebook "></i>'+
                                            '<span class="hidden-xs"></span>'+
                                        '</a>'+
                                    '</div>'+
                                    '<div class="social-share-link-wr">'+
                                        '<a href="'+share_tw+'" class="btn btn-lg btn-block btn-share btn-share-twitter" data-toggle="tooltip" data-placement="top" title="Facebook">'+
                                            '<i class="fa fa-twitter "></i>'+
                                            '<span class="hidden-xs"></span>'+
                                        '</a>'+
                                    '</div>'+
                                    '<div class="social-share-link-wr">'+
                                        '<a href="'+share_gg+'" class="btn btn-lg btn-block btn-share btn-share-google-plus" data-toggle="tooltip" data-placement="top" title="Facebook">'+
                                            '<i class="fa fa-google-plus "></i>'+
                                            '<span class="hidden-xs"></span>'+
                                        '</a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="popup-footer" style="margin-top:20px;">'+
                                '<p>'+post_text+'</p>'+
                            '</div>'+
                            '<p><a class="btn btn-primary read-more" href="'+$(this).data('readmore')+'">Read more ></a></p>'+
                            '<button title="Close (Esc)" type="button" class="mfp-close">×</button>'+
                        '</div>';
        var popup = $(template);
        $.magnificPopup.open({
            items: {
                src: popup,
            },
            type: 'inline'
        });
    });


});