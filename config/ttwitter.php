<?php

// You can find the keys here : https://apps.twitter.com/

return [
	'debug'               => function_exists('env') ? env('APP_DEBUG', false) : false,

	'API_URL'             => 'api.twitter.com',
	'UPLOAD_URL'          => 'upload.twitter.com',
	'API_VERSION'         => '1.1',
	'AUTHENTICATE_URL'    => 'https://api.twitter.com/oauth/authenticate',
	'AUTHORIZE_URL'       => 'https://api.twitter.com/oauth/authorize',
	'ACCESS_TOKEN_URL'    => 'https://api.twitter.com/oauth/access_token',
	'REQUEST_TOKEN_URL'   => 'https://api.twitter.com/oauth/request_token',
	'USE_SSL'             => true,

	'CONSUMER_KEY'        => function_exists('env') ? env('TWITTER_CLIENT_ID', 'wXTu81nPDYNTi3OghzIApMp0E') : 'wXTu81nPDYNTi3OghzIApMp0E',
	'CONSUMER_SECRET'     => function_exists('env') ? env('TWITTER_CLIENT_SECRET', 'g5SsmkVKlquqE4kHgVWNdYQUBNAACHGzhGk7Bf40DM6kdGE1rk') : 'g5SsmkVKlquqE4kHgVWNdYQUBNAACHGzhGk7Bf40DM6kdGE1rk',
	'ACCESS_TOKEN'        => function_exists('env') ? env('TWITTER_ACCESS_TOKEN', '869829205999177728-u9sF13YuuhCRA6H6ZbzpAOSf7ZMgvwz') : '869191753194188800-eoFCKshsFzXFl3NgAR2lZ1bJozxiGOQ',
	'ACCESS_TOKEN_SECRET' => function_exists('env') ? env('TWITTER_ACCESS_TOKEN_SECRET', 'eEGDT2n6Y7uJM5JRnq8U2v6Uju676sj43ByPyGT2rJ0Zz') : 'IWVuGgC5iF0Sd8omwDMqhvkXAnvnEaozjEgfXSfidN3wl',
];
