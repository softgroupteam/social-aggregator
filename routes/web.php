<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('register','Auth\RegisterController@getRegister')->name('register');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout' , 'Auth\LoginController@logout');


// OAuth Routes
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider')->name('social.auth');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    Route::get('/promote-pages','DashboardController@promotePagesView')->name('promote-pages');
    Route::get('/dashboard/facebook-page-posts/{pageTitle}','Facebook\PostsController@getPagePosts')->name('get-posts-by-page');
    Route::get('/dashboard/youtube-channel-feed/{channelId}','Youtube\VideosController@getVideosByChannel')->name('get-feed-by-youtubeChannel');
    Route::get('/dashboard/twitter-user-tweets/{userName}/','Twitter\TweetsController@getTweetsByUser')->name('get-tweets-by-user');
    Route::get('/dashboard/google-plus', 'Googleplus\PostsController@getGooglePosts')->name('google-plus');
    Route::post('/get-google-plus', 'Googleplus\PostsController@getGooglePostsAjax')->name('get-google-plus');
    Route::get('/dashboard/composer/','DashboardController@getComposerView')->name('composer');
    Route::post('/dashboard/composer/load-post','DashboardController@loadPostToComposer')->name('composer-load-post');
    Route::get('/dashboard/saved-posts','DashboardController@getSavedPostsView')->name('get-saved-posts-dashboard');
    Route::get('/dashboard/profile','DashboardController@getProfile')->name('profile');
    Route::get('/dashboard/profile/{id}','DashboardController@getProfileByUser')->name('profile-by-user');
    Route::get('/dashboard/social-streams','DashboardController@getSocialStreamsPage')->name('social-streams-page');
});

Route::group(['prefix' => 'modal'], function () {
    Route::post('/add-fbpage','ModalController@addFacebookPage')->name('modal.add-fbpage');
    Route::post('/add-ytchannel','ModalController@addYoutubeChannel')->name('modal.add-ytchannel');
    Route::post('/add-twpage','ModalController@addTwitterPage')->name('modal.add-twpage');
    Route::post('/delete-socialtab','ModalController@deleteSocialTab')->name('modal.delete-socialtab');
    Route::post('/delete-socialacc','ModalController@deleteSocialAcc')->name('modal.delete-socialacc');
    Route::post('/delete-socialfbpage','ModalController@deleteSocialFbPage')->name('modal.delete-socialfbpage');
    Route::post('/add-gpchannel','ModalController@addGooglePlusChannel')->name('modal.add-gpchannel');
    Route::post('/add-category','ModalController@addCategory')->name('modal.add-category');
    Route::get('/get-categories','ModalController@getCategories')->name('modal.get-categories');
});

Route::group(['prefix' => 'ajax'], function () {
    Route::post('/facebook-user-posts/','Facebook\PostsController@getPostsByUserAjax')->name('ajax.get-posts-by-fbuser');
    Route::post('/facebook-user-feed/','Facebook\PostsController@getFeedAjax')->name('ajax.get-feed-by-fbuser');
    Route::post('/facebook-page-posts/','Facebook\PostsController@getPagePostsAjax')->name('ajax.get-posts-by-page');
    Route::post('/youtube-channel-feed/','Youtube\VideosController@getVideosByChannelAjax')->name('ajax.get-feed-by-youtubeChannel');
    Route::post('/twitter-user-tweets/','Twitter\TweetsController@getTweetsByUserAjax')->name('ajax.get-tweets-by-user');

    Route::post('/facebook-page-posts-foruser/','Facebook\PostsController@getPagePostsAjaxForUser')->name('ajax.get-posts-by-page-foruser');
    Route::post('/youtube-channel-feed-foruser/','Youtube\VideosController@getVideosByChannelAjaxForUser')->name('ajax.get-feed-by-youtubeChannel-foruser');
    Route::post('/twitter-user-tweets-foruser/','Twitter\TweetsController@getTweetsByUserAjaxForUser')->name('ajax.get-tweets-by-user-foruser');


    Route::post('/store-fbpost/','Facebook\PostsController@store')->name('ajax.store-fbpost');
    Route::post('/publish-fbpost/','Facebook\PostsController@publish')->name('ajax.publish-fbpost');
    Route::post('/delete-fbpost/','Facebook\PostsController@delete')->name('ajax.delete-fbpost');
    Route::post('/unpublish-fbpost/','Facebook\PostsController@unpublish')->name('ajax.unpublish-fbpost');
    Route::post('/store-twpost/','Twitter\TweetsController@store')->name('ajax.store-twpost');
    Route::post('/publish-twpost/','Twitter\TweetsController@publish')->name('ajax.publish-twpost');
    Route::post('/delete-twpost/','Twitter\TweetsController@delete')->name('ajax.delete-twpost');
    Route::post('/unpublish-twpost/','Twitter\TweetsController@unpublish')->name('ajax.unpublish-twpost');
    Route::post('/store-ytpost/','Youtube\VideosController@store')->name('ajax.store-ytpost');
    Route::post('/publish-ytpost/','Youtube\VideosController@publish')->name('ajax.publish-ytpost');
    Route::post('/delete-ytpost/','Youtube\VideosController@delete')->name('ajax.delete-ytpost');
    Route::post('/unpublish-ytpost/','Youtube\VideosController@unpublish')->name('ajax.unpublish-ytpost');
    Route::post('/store-gglpost/','Googleplus\PostsController@store')->name('ajax.store-gglpost');
    Route::post('/publish-gglpost/','Googleplus\PostsController@publish')->name('ajax.publish-gglpost');
    Route::post('/delete-gglpost/','Googleplus\PostsController@delete')->name('ajax.delete-gglpost');
    Route::post('/unpublish-gglpost/','Googleplus\PostsController@unpublish')->name('ajax.unpublish-gglpost');

    Route::post('/add-favourite-fbpost/','Facebook\PostsController@addToFavouriteForUser')->name('ajax.add-favourite-fbpost');
    Route::post('/delete-favourite-fbpost/','Facebook\PostsController@deleteFromFavouriteForUser')->name('ajax.delete-favourite-fbpost');
    Route::post('/add-favourite-twpost/','Twitter\TweetsController@addToFavouriteForUser')->name('ajax.add-favourite-twpost');
    Route::post('/delete-favourite-twpost/','Twitter\TweetsController@deleteFromFavouriteForUser')->name('ajax.delete-favourite-twpost');
    Route::post('/add-favourite-ytpost/','Youtube\VideosController@addToFavouriteForUser')->name('ajax.add-favourite-ytpost');
    Route::post('/delete-favourite-ytpost/','Youtube\VideosController@deleteFromFavouriteForUser')->name('ajax.delete-favourite-ytpost');
    Route::post('/add-favourite-gglpost/','Googleplus\PostsController@addToFavouriteForUser')->name('ajax.add-favourite-ggpost');
    Route::post('/delete-favourite-gglpost/','Googleplus\PostsController@deleteFromFavouriteForUser')->name('ajax.delete-favourite-ggpost');

    Route::post('/get-published-posts/','CommonPostsController@getAllPosts')->name('ajax.get-published-posts');
    Route::post('/composer-save-post','ComposerController@savePost')->name('ajax.composer-save-post');
    Route::post('/get-saved-posts-dashboard/','DashboardCommonPostsController@getAllPosts')->name('ajax.get-saved-posts-dashboard');
    Route::post('/link-fb-entities/','Auth\AuthController@linkFacebookAccNPages')->name('ajax.link-fb-entities');
    Route::post('/user/update','UserController@updateUser')->name('ajax.update-user');
    Route::post('/user-password/update','UserController@updateUserPassword')->name('ajax.update-user-password');

    /*user role*/
    Route::post('/get-suggested-fbposts/','SuggestedPostsController@getFacebookPosts')->name('ajax.get-suggested-fbposts');
    Route::post('/get-suggested-twposts/','SuggestedPostsController@getTwitterPosts')->name('ajax.get-suggested-twposts');
    Route::post('/get-suggested-ytposts/','SuggestedPostsController@getYoutubeVideos')->name('ajax.get-suggested-ytposts');
    Route::post('/get-suggested-ggposts/','SuggestedPostsController@getGooglePosts')->name('ajax.get-suggested-ggposts');
    Route::post('/add-to-favourite','DashboardCommonPostsController@addToFavourite')->name('ajax.add-to-favourite');
    Route::post('/delete-from-favourite','DashboardCommonPostsController@deleteFromFavourite')->name('ajax.delete-from-favourite');

    Route::post('/get-users-suggested-posts/','UsersSuggestedPostsController@getAllPosts')->name('ajax.get-users-suggested-posts');
    Route::post('/get-user-favourite-posts/','MyStreamsController@getAllFavouritePosts')->name('ajax.get-user-favourite-posts');
    Route::post('/get-random-users-forfollow/','UserController@getRandomUsers')->name('ajax.get-random-users-forfollow');
    Route::post('/follow-user/','UserController@followUser')->name('ajax.follow-user');
    Route::post('/unfollow-user/','UserController@unfollowUser')->name('ajax.unfollow-user');
    Route::post('/get-followed-posts/','MyStreamsController@getAllFollowedPosts')->name('ajax.get-followed-posts');
    Route::post('/get-favourite-posts/','MyStreamsController@getAllFavouritePosts')->name('ajax.get-favourite-posts');
    Route::post('/get-stream-posts/','MyStreamsController@getAllStreamPosts')->name('ajax.get-stream-posts');
    Route::post('/get-stream-posts-by-user/','MyStreamsController@getAllStreamPostsByUser')->name('ajax.get-stream-posts-by-user');

    Route::get('/get-facebook-streams','SocialStreamsController@getFacebookStreams')->name('ajax.get-facebook-streams');
    Route::get('/get-twitter-streams','SocialStreamsController@getTwitterStreams')->name('ajax.get-twitter-streams');
    Route::get('/get-youtube-streams','SocialStreamsController@getYoutubeStreams')->name('ajax.get-youtube-streams');
    Route::get('/get-google-streams','SocialStreamsController@getGoogleStreams')->name('ajax.get-google-streams');

    Route::post('/add-facebook-stream','SocialStreamsController@addFacebookStream')->name('ajax.add-facebook-stream');
    Route::post('/add-twitter-stream','SocialStreamsController@addTwitterStream')->name('ajax.add-twitter-stream');
    Route::post('/add-youtube-stream','SocialStreamsController@addYoutubeStream')->name('ajax.add-youtube-stream');
    Route::post('/add-google-stream','SocialStreamsController@addGoogleStream')->name('ajax.add-google-stream');

    Route::post('/get-followed-users/','UserController@getFollowedUsers')->name('ajax.get-followed-users');
    Route::post('/get-followers/','UserController@getFollowers')->name('ajax.get-followers');
    Route::post('/get-followed-users-by-user/','UserController@getFollowedUsersByUser')->name('ajax.get-followed-users-by-user');
    Route::post('/get-followers-by-user/','UserController@getFollowersByUser')->name('ajax.get-followers-by-user');

    /* favourite posts*/
    Route::post('/add-fbpost-to-userstream','Facebook\PostsController@addToUserStream')->name('ajax.add-fbpost-to-userstream');
    Route::post('/remove-fbpost-from-userstream','Facebook\PostsController@deleteFromUserStream')->name('ajax.remove-fbpost-from-userstream');
    Route::post('/add-twpost-to-userstream','Twitter\TweetsController@addToUserStream')->name('ajax.add-twpost-to-userstream');
    Route::post('/remove-twpost-from-userstream','Twitter\TweetsController@deleteFromUserStream')->name('ajax.remove-twpost-from-userstream');
    Route::post('/add-ytpost-to-userstream','Youtube\VideosController@addToUserStream')->name('ajax.add-ytpost-to-userstream');
    Route::post('/remove-ytpost-from-userstream','Youtube\VideosController@deleteFromUserStream')->name('ajax.remove-ytpost-from-userstream');
    Route::post('/add-ggpost-to-userstream','Googleplus\PostsController@addToUserStream')->name('ajax.add-ggpost-to-userstream');
    Route::post('/remove-ggpost-from-userstream','Googleplus\PostsController@deleteFromUserStream')->name('ajax.remove-ggpost-from-userstream');

    /* scheduled posts */
    Route::get('/get-scheduled-posts','ScheduledPostsController@getScheduledPosts')->name('ajax.get-scheduled-posts');
    Route::post('/get-scheduled-post-info','ScheduledPostsController@getScheduledPostInfo')->name('ajax.get-scheduled-post-info');
    Route::post('/edit-scheduled-post','ScheduledPostsController@editScheduledPost')->name('ajax.edit-scheduled-post');
    Route::post('/delete-scheduled-post','ScheduledPostsController@deleteScheduledPost')->name('ajax.delete-scheduled-post');
    Route::post('/massive-delete-scheduled-posts','ScheduledPostsController@massiveDeleteScheduledPosts')->name('ajax.massive-delete-scheduled-posts');



    /* profile account management */
    Route::get('/get-profile-facebook-accounts','SocialLoginsController@getFacebookSocialLogins')->name('ajax.get-profile-facebook-accounts');
    Route::get('/get-profile-twitter-accounts','SocialLoginsController@getTwitterSocialLogins')->name('ajax.get-profile-twitter-accounts');
    Route::get('/get-profile-google-accounts','SocialLoginsController@getGoogleSocialLogins')->name('ajax.get-profile-google-accounts');

    Route::post('/upload-user-avatar','ProfileController@changeUserAvatar')->name('ajax.upload-user-avatar');

    /* promote pages */
    Route::post('/store-promote-keywords','PromotePagesController@storeKeywords')->name('ajax.store-promote-keywords');
    Route::get('/get-promote-pages','PromotePagesController@getPromotePages')->name('ajax.get-promote-pages');
    Route::post('/store-promote-page','PromotePagesController@storePromotePage')->name('ajax.store-promote-page');
    Route::post('/delete-promote-page','PromotePagesController@deletePromotePage')->name('ajax.delete-promote-page');

});


Route::group(['middleware' => ['role:user'],'prefix' => 'admin'], function () {
    Route::get('dashboard/get-suggested-posts','SuggestedPostsController@getPage')->name('get-suggested-posts-page');
    Route::get('dashboard/get-users-suggested-posts','UsersSuggestedPostsController@getPage')->name('get-users-suggested-posts-page');
    Route::get('dashboard/my-favourite-posts','MyStreamsController@getFavouritePostsPage')->name('get-my-favourite-posts');
});


// test route
Route::get('/foo', function () {
    Artisan::call('scheduled-posts:check');
});

