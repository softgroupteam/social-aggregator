<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="{{asset('img/main/jumbotron.jpg')}}">
    <meta property="og:url" content="http://sgview17.lab-sg.com/">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Foodridge Social">
    <meta property="og:description" content="Foodridge is an exclusive social community" />

    <title>Foodridge Social</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('magnificPopup/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('AdminLTE/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/frontend/homepage.css')}}">
</head>
<body>
<div class="container-fluid">
    <div class="header clearfix">
        <nav class="navbar navbar-inverse" style="font-weight: bold;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Foodridge Social</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav  navbar-nav navbar-right">
                    @if (Auth::check())
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li><a href="{{ url('/logout') }}">Logout</a></li>
                    @else
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @endif
                </ul>
            </div>
        </nav>
    </div>

    <div class="jumbotron home-jumbotron">
        <h1 class="text-center">The Good Taste Society</h1>
        <p class="lead text-center">Foodridge is an exclusive social community that gives its members the possibility to reach one another instantly and grow their business. Our mission is to build bridges between the most refined gastronomy aficionados, the best restaurants and all-star chefs.</p>
        @if (!Auth::check())
        <p class="text-center"><a class="btn btn-lg btn-success" href="{{ url('/register') }}" role="button">Sign up today</a></p>
        @endif
    </div>

    <div class="content">
        <div class="container uss-container" >
            <div id="filters" class="button-group text-center filters-button-group">
                <button class="button social-filter is-checked" data-filter="*">All</button>
                <button class="button social-filter" data-filter=".uss-facebook">Facebook</button>
                <button class="button social-filter" data-filter=".uss-twitter">Twitter</button>
                <button class="button social-filter" data-filter=".uss-youtube">Youtube</button>
                <button class="button social-filter" data-filter=".uss-google">Google+</button>
            </div>
            <div class="social-stream-container">
            </div>
        </div>
    </div>

    {{--popup--}}
    <div id="test-popup" class="white-popup mfp-hide" style="background:white;width:400px;height:400px;margin:auto;">
        Popup content
    </div>
</div>
<script src="{{asset('AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script src="{{asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('magnificPopup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/laroute.js')}}"></script>
<script src="{{asset('js/jstz.min.js')}}"></script>
<script src="{{asset('js/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('js/frontend/mainpage-posts.js')}}"></script>

</body>
</html>
