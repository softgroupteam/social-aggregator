@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
                <h1>Facebook posts</h1>
            </div>
            <div>
                @foreach ($posts->data as $post)
                    <div class="panel panel-default">
                        <div class="panel-body">
                        @if (isset($post->full_picture))
                            <img src={{$post->full_picture}}>
                        @endif

                        @if (isset($post->message))
                            <div>{{$post->message}}</div>
                        @elseif (isset($post->story))
                            <div>{{$post->story}}</div>
                        @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
