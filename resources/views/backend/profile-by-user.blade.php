@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Profile
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile" data-user-id="{{$user->id}}">
                        <img class="profile-user-img img-responsive img-circle" src="{{asset('img/mock/mock_user_avatar_small.png')}}" alt="User profile picture">

                        <h3 class="profile-username text-center">{{$user->name}}</h3>

                        <p class="text-muted text-center">{{$user->userInfo->userOccupation->name}}</p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Followers</b> <a class="pull-right">{{$followersCount}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Following</b> <a class="pull-right">{{$followingCount}}</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">About Me</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                        <p class="text-muted">{{$user->userInfo->city}}, {{$user->userInfo->country_name}}</p>
                        <hr>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#profile-my-streams" data-toggle="tab">My streams</a></li>
                        <li class=""><a href="#profile-followers" data-toggle="tab">Followers</a></li>
                        <li class=""><a href="#profile-following" data-toggle="tab">Following</a></li>
                    </ul>
                    <div class="tab-content">
                        <!-- /.tab-pane -->
                        <div class="tab-pane row text-center active" id="profile-my-streams">
                        </div>
                        <div class="tab-pane" id="profile-followers">
                        </div>
                        <div class="tab-pane" id="profile-following">
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
    </section>
@endsection

@section('after-scripts')
    <script src="{{asset('js/backend/profile-by-user.js')}}"></script>
@endsection