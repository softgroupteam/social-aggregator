@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            No results
            {{--<small>Optional description</small>--}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Sorry, no results. Please check your settings.</p>
                </div>
            </div>
        </div>
    </section>
@endsection