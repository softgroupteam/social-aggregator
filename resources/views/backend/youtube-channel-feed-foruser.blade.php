@extends('backend.layouts.lte')

@section('content')
    <style>
        .video-container {
            position: relative;
            width: 100%;
            height: 0;
            padding-bottom: 56.25%;
        }
        .video {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    </style>
    <section class="content-header">
        <h1>
            Youtube
            {{--<small>Optional description</small>--}}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>--}}
            {{--<li class="active">Here</li>--}}
        {{--</ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container text-center">
            @foreach ($videos as $key => $video)
                <div class="panel panel-default" style="width:350px; display:inline-block; margin:5px;">
                        <div class="panel-body">
                            <div class="video-container">
                                <iframe width=100%" frameborder="0" allowfullscreen class="video"
                                        src="https://www.youtube.com/embed/{{$video->id}}">
                                </iframe>
                            </div>
                            <div class="box box-danger">
                                <div class="box-header with-border">
                                    <p class="text-blue" >{{$video->createdLocal}}</p>
                                    <div class="yt-post-statistics">
                                        <span><i class="fa fa-eye"></i>{{$video->statistics->viewCount}}</span>
                                        <span><i class="fa fa-comment"></i>{{$video->statistics->commentCount}}</span>
                                        <span><i class="fa fa-thumbs-up"></i>{{$video->statistics->likeCount}}</span>
                                        <span><i class="fa fa-thumbs-down"></i>{{$video->statistics->dislikeCount}}</span>
                                    </div>
                                    <h3 class="box-title">{{$video->snippet->title}}</h3>
                                    <div class="box-tools pull-right">
                                    </div><!-- /.box-tools -->
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <button  type="button" class="btn btn-success save-addfavourite-ytpost {{($video->state == 'saved_favourite')?'hidden':''}}" data-post-id="{{$video->id}}">Save to favourite</button>
                                    <button  type="button" class="btn btn-danger delete-removefavourite-ytpost {{($video->state == 'notsaved_notfavourite')?'hidden':''}}" data-post-id="{{$video->id}}">Delete from favourite</button>
                                </div>
                            </div><!-- /.box -->
                        </div>
                </div>
            @endforeach
                <div class="add-more-block col-md-12 text-center">
                    <button class="add-more-posts btn btn-default"
                            data-action="{{route('ajax.get-feed-by-youtubeChannel-foruser')}}"
                            data-channelid="{{$channelId}}"
                            data-pagetoken="{{$pageToken}}">Add more...</button>
                </div>
        </div>
    </div>
    </section>
@endsection

@section('after-scripts')
    <script src="{{asset('js/backend/yt-posts-foruser.js')}}"></script>
@endsection