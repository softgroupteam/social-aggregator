@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Users suggested posts
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div id="users-random-posts-container" class="users-random-posts-container text-center">
        </div>
    </section>
@endsection

@section('after-scripts')
    <script src="{{asset('js/backend/users-suggested-posts.js')}}"></script>
@endsection
