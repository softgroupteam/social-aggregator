@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1 class="text-center">
            Welcome to dashboard
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

    </section>
@endsection

@section('after-scripts')
    <script src="{{asset('js/jstz.min.js')}}"></script>
    <script>
        var tz = jstz.determine();
        document.cookie = "user_time_zone="+tz.name()+"; path=/;";
    </script>
@endsection