@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Google+
            {{--<small>Optional description</small>--}}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>--}}
            {{--<li class="active">Here</li>--}}
        {{--</ol>--}}
    </section>
    <!--section class="search-form text-center">
        <div class="row">
            <div class="col-md-offset-4 col-md-4">
                {{ Form::open(array('url' => '/admin/get-google-plus', 'method' => 'post', 'id' => 'get-posts-form')) }}
                <div class="form-group">
                    <label>Google+ group ID</label>{{ Form::text('google_group', '+lifehacker_ru', ['class' => "form-control"]) }}
                </div>
                {{ Form::submit('Get posts', ['class' => 'btn btn-default'])}}
                {{ Form::close() }}
                {{ $error }}
            </div>
        </div>
    </section--->

    <!-- Main content -->
    <section class="content">
        <div  class="container text-center" >
            @foreach ($posts as $post)
                <div class="panel panel-default " style="width:350px; display:inline-block; margin:5px;">
                    <div class="panel-body">
                        @if (isset($post->object->attachments[0]->image->url))
                            <div class="text-center">
                                <img src={{$post->object->attachments[0]->image->url}} style="max-width:100%;max-height:200px;">
                            </div>
                        @endif
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <p class="text-blue" >{{$post->createdLocal}}</p>
                                <div class="gg-post-statistics">
                                    <span><i class="fa fa-share-square-o"></i>{{$post->object->resharers->totalItems}}</span>
                                    <span><i class="fa fa-comment"></i>{{$post->object->replies->totalItems}}</span>
                                    <span><i class="fa fa-thumbs-up"></i>{{$post->object->plusoners->totalItems}}</span>
                                </div>
                                <p class="box-title">
                                    @if (isset($post->object->content))
                                        {!!$post->object->content!!}
                                    @endif
                                </p>
                                <div class="box-tools pull-right">
                                </div><!-- /.box-tools -->
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <button  type="button" class="btn btn-success save-gglpost {{($post->state == 'saved_published' || $post->state == 'saved_notpublished' )?'hidden':''}}" data-post-id="{{$post->id}}">Save</button>
                                <button  type="button" class="btn btn-danger delete-gglpost {{($post->state == 'notsaved')?'hidden':''}}" data-post-id="{{$post->id}}">Delete</button>
                                <button  type="button" class="btn btn-info publish-gglpost {{($post->state == 'notsaved' || $post->state == 'saved_published' )?'hidden':''}}" data-post-id="{{$post->id}}">Publish</button>
                                <button  type="button" class="btn btn-info unpublish-gglpost {{($post->state == 'saved_notpublished' || $post->state == 'notsaved' )?'hidden':''}}" data-post-id="{{$post->id}}">Unpublish</button>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            @endforeach
                <div class="add-more-block col-md-12 text-center"><button class="add-more-posts btn btn-default" data-group="{{ $group }}" data-action="{{route('get-google-plus')}}" data-page="{{ $pageToken }}" data-token="{{ csrf_token() }}">Add more...</button></div>
        </div>
    </section>
@endsection


@section('after-scripts')
    <script src="{{asset('js/backend/ggl-posts.js')}}"></script>
@endsection
