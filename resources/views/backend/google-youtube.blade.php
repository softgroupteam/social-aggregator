@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Google+ posts
            <small>Optional description</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>
    <section class="search-form text-center">
        <div class="row">
            <div class="col-md-offset-4 col-md-4">
                {{ Form::open(array('url' => '/admin/get-youtube', 'method' => 'post', 'id' => 'get-posts-form')) }}
                <div class="form-group">
                    <label>Google+ group ID</label>{{ Form::text('google_group', 'UCbpMy0Fg74eXXkvxJrtEn3w', ['class' => "form-control"]) }}
                </div>
                {{ Form::submit('Get posts', ['class' => 'btn btn-default'])}}
                {{ Form::close() }}
                {{ $error }}
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div  class="container" >
            @foreach ($posts as $post)
                <div class="panel panel-default " style="width:350px; display:inline-block;">
                    <div class="panel-body">
                        @if (isset($post->object->attachments[0]->image->url))
                            <div class="text-center">
                                <img src={{$post->object->attachments[0]->image->url}} style="max-width:100%;max-height:200px;">
                            </div>
                        @endif
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    @if (isset($post->title))
                                        <h3 class="text-center">{{$post->title}}</h3>
                                    @endif
                                </h3>
                                <div class="box-tools pull-right">
                                </div><!-- /.box-tools -->
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <button type="button" class="btn btn-success">Publish</button>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            @endforeach

        </div>
    </section>
@endsection


@section('after-scripts')
    <script src="{{asset('js/backend/ggl-posts.js')}}"></script>
@endsection
