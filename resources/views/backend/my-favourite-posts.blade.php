@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Favourite posts
        </h1>
    </section>
    <section class="content row">
        <div id="user-favourite-posts-container" class="user-favourite-posts-container text-center">
        </div>
    </section>
@endsection

@section('after-scripts')
    <script src="{{asset('js/backend/my-favourite-posts.js')}}"></script>
@endsection