@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Saved posts
            {{--<small>Optional description</small>--}}
        </h1>
        {{--<ol class="breadcrumb">--}}
        {{--<li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>--}}
        {{--<li class="active">Here</li>--}}
        {{--</ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container text-center">
        </div>
    </section>
@endsection

@section('after-scripts')
    <script src="{{asset('js/backend/common-posts.js')}}"></script>
@endsection