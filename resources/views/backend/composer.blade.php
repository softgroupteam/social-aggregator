@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header with-border text-center">
                        <h3 class="box-title">Composer</h3>
                    </div>
                    <div class="box-body">
                        <form id="composer-post-form">
                            <div class="input-group input-group-lg composer-profiles-wrapper">
                                <div class="btn-group facebook-profiles-btngroup">
                                    <button type="button" class="btn btn-primary"><span class="fa  fa-facebook"></span></button>
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu composer-profiles">
                                        @if (count($accounts['facebook'])>0)
                                            @foreach ($accounts['facebook'] as $acc)
                                                <li class="composer-profile-listitem"><a class="checkbox-inline"><input value="{{$acc->id}}" name="fbaccounts[{{$acc->id}}]" type="checkbox">{{$acc->name}}</a></li>
                                            @endforeach
                                        @endif
                                        @if (count($fbSocialPages)>0)
                                            @foreach ($fbSocialPages as $page)
                                                <li class="composer-profile-listitem"><a class="checkbox-inline"><input value="{{$page->id}}" name="fbsocialpages[{{$page->id}}]" type="checkbox">{{$page->name}}</a></li>
                                            @endforeach
                                        @endif
                                            <li class="composer-profile-listitem"><a class="checkbox-inline"><input value="1" name="include-promote-pages" type="checkbox">Include promote pages</a></li>
                                    </ul>
                                </div>
                                <div class="btn-group twitter-profiles-btngroup">
                                    <button type="button" class="btn btn-info"><span class="fa  fa-twitter"></span></button>
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu composer-profiles">
                                        @if (count($accounts['twitter'])>0)
                                            @foreach ($accounts['twitter'] as $acc)
                                                <li class="composer-profile-listitem"><a class="checkbox-inline"><input value="{{$acc->id}}" name="twaccounts[{{$acc->id}}]" type="checkbox">{{$acc->name}}</a></li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                {{--<div class="btn-group googleplus-profiles-btngroup">--}}
                                    {{--<button type="button" class="btn btn-warning"><span class="fa  fa-google-plus"></span></button>--}}
                                    {{--<button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                        {{--<span class="caret"></span>--}}
                                        {{--<span class="sr-only">Toggle Dropdown</span>--}}
                                    {{--</button>--}}
                                    {{--<ul class="dropdown-menu composer-profiles">--}}
                                        {{--@if (count($accounts['google'])>0)--}}
                                            {{--@foreach ($accounts['google'] as $acc)--}}
                                                {{--<li class="composer-profile-listitem"><a class="checkbox-inline"><input value="{{$acc->id}}" name="ggaccounts[{{$acc->id}}]" type="checkbox">{{$acc->name}}</a></li>--}}
                                            {{--@endforeach--}}
                                        {{--@endif--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                <button class="btn btn-success" data-post-type="{{$postType}}" id="publish-composer-post">
                                    <span>Publish</span>
                                    <div class="spinner-stack hidden">
                                        <div class="rect1"></div>
                                        <div class="rect2"></div>
                                        <div class="rect3"></div>
                                        <div class="rect4"></div>
                                        <div class="rect5"></div>
                                    </div>
                                </button>
                                <button class="btn btn-success hidden" id="edit-composer-post" data-post-id="">
                                    Submit changes
                                </button>
                                <button class="btn btn-default hidden" id="cancel-edit-composer-post">
                                    Cancel
                                </button>
                            </div>
                            <br>
                            <div class="input-group" style="width:100%">
                                <label class="radio-inline">
                                    <input type="radio" value="now" id="social_post_now" name="social_schedule_check" checked>Post now
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" value="schedule" id="social_schedule" name="social_schedule_check">Schedule now
                                </label>
                            </div>
                            <br>
                            <div class="input-group schedule-form row hidden" id="schedule-form" style="width:100%">
                                <div class="col-sm-4">
                                    <input type="text" name="scheduled-post-date" class="form-control pull-right" placeholder="Choose date" id="post-date">
                                </div>
                                <div class="col-sm-4">
                                    <select name="scheduled-post-hours" class="form-control" id="scheduler-hours">
                                        <option value="HH" selected>HH</option>
                                        <option value="00" >00</option>
                                        <option value="01" >01</option>
                                        <option value="02" >02</option>
                                        <option value="03" >03</option>
                                        <option value="04" >04</option>
                                        <option value="05" >05</option>
                                        <option value="06" >06</option>
                                        <option value="07" >07</option>
                                        <option value="08" >08</option>
                                        <option value="09" >09</option>
                                        <option value="10" >10</option>
                                        <option value="11" >11</option>
                                        <option value="12" >12</option>
                                        <option value="13" >13</option>
                                        <option value="14" >14</option>
                                        <option value="15" >15</option>
                                        <option value="16" >16</option>
                                        <option value="17" >17</option>
                                        <option value="18" >18</option>
                                        <option value="19" >19</option>
                                        <option value="20" >20</option>
                                        <option value="21" >21</option>
                                        <option value="22" >22</option>
                                        <option value="23" >23</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="scheduled-post-minutes" class="form-control" id="scheduler-minutes">
                                        <option value="MM" selected>MM</option>
                                        <option value="00" >00</option>
                                        <option value="05" >05</option>
                                        <option value="10" >10</option>
                                        <option value="15" >15</option>
                                        <option value="20" >20</option>
                                        <option value="25" >25</option>
                                        <option value="30" >30</option>
                                        <option value="35" >35</option>
                                        <option value="40" >40</option>
                                        <option value="45" >45</option>
                                        <option value="50" >50</option>
                                        <option value="55" >55</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="input-group" style="width:100%;">
                                <textarea id="message-text" name="message-text" class="form-control post-message-textarea" rows="3" placeholder="Enter message" style="width:100%;">{{ ( isset($message) ? $message : '') }}</textarea>
                            </div>
                            <br>
                            <div>
                                <input class="form-control" name="user-attached-link" type="text" style="width:100%; padding-left:10px;" placeholder="Attach link to post">
                            </div>
                            <br>
                            <div class="text-center edited-post-image-wrapper hidden">
                                <div class="edited-post-image-inner-wrapper">
                                    <div class="remove-image">×</div>
                                    <img id="edited-post-image" class="edited-post-image" src="">
                                </div>
                            </div>

                            <div class="text-center composer-remote-video-wrapper">
                                <div class="composer-remote-video-inner-wrapper">
                                    @if(isset($embedVideoHtml))
                                        @if($embedVideoHtml != '')
                                            {!!$embedVideoHtml!!}
                                        @endif
                                    @endif
                                </div>
                            </div>
                            @if(isset($imageUrlFromSocial) && $embedVideoHtml == '')
                                <div class="text-center composer-remote-image-wrapper">
                                    <div class="composer-remote-image-inner-wrapper">
                                        <div class="remove-image {{($postType == "video")?'hidden':''}}">×</div>
                                         <img class="composer-remote-image" src="{{$imageUrlFromSocial}}">
                                    </div>
                                </div>
                            @endif
                            <div class="image-changebutton-wrapper text-center {{ ( isset($imageUrlFromSocial) ? '' : 'hidden') }}" style="margin-top:10px;">
                                <button class="btn btn-warning {{($postType == "video")?'hidden':''}}" id="post-image-changebutton">
                                    <span>Change image</span>
                                </button>
                            </div>
                            <div class="text-center composer-post-image-wrapper {{ ( isset($imageUrlFromSocial) ? 'hidden' : '') }}">
                                <label class="control-label">Attach image</label>
                                <input id="composer-post-image" name="composer-post-image" type="file" class="file" data-preview-file-type="text" data-show-upload="false" style="opacity:0;">
                            </div>
                            <input id="remote-image-url" name="remote-image-url" type="hidden" value="{{ ( isset($imageUrlFromSocial) ? $imageUrlFromSocial : '') }}">
                            <input id="edited-image-url" name="edited-image-url" type="hidden" value="">
                            <input id="original-link" name="original-link" type="hidden" value="{{(isset($originalLink)? $originalLink : '')}}">
                            <input id="remote-video-link" name="remote-video-link" type="hidden" value="{{(isset($videoLink)? $videoLink : '')}}">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header with-border  text-center">
                        <h3 class="box-title">Scheduled posts</h3>
                    </div>
                    <table id="scheduled-posts-table" class="table table-bordered table-hover">
                        <button class="btn btn-danger mrgn-5 hidden" id="delete-selected-scheduled-posts" data-post-id="">
                            Delete selected
                        </button>
                        <thead>
                            <tr>
                                <th class="text-center" style="max-width:30px;">№</th>
                                <th class="text-center" style="max-width:10px;" ><input type="checkbox" id="select-all-scheduled-posts"></th>
                                <th class="text-center" style="max-width:100px;">Picture</th>
                                <th class="text-center" >Message</th>
                                <th class="text-center" style="max-width:200px;">Time</th>
                                <th class="text-center" >Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('after-styles')
    <link rel="stylesheet" href="{{asset('bootstrap-fileinput/css/fileinput.css')}}">
    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/datepicker/datepicker3.css')}}">
@endsection

@section('after-scripts')
    <script src="{{asset("AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
    <script src="{{asset('bootstrap-fileinput/js/fileinput.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/backend/composer.js')}}"></script>
@endsection