@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Suggested posts
        </h1>
    </section>
    <div class="text-center v-mrgn-10">
        <div class="btn-group" role="group" aria-label="...">
            <button id="get-suggested-fbposts" type="button" class="btn btn-default">Facebook</button>
            <button id="get-suggested-twposts" type="button" class="btn btn-default">Twitter</button>
            <button id="get-suggested-ytposts" type="button" class="btn btn-default">Youtube</button>
            <button id="get-suggested-ggposts" type="button" class="btn btn-default">Google</button>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="width-100 text-center hidden" id="suggested-fbposts-container">
        </div>
        <div class="width-100 text-center hidden" id="suggested-twposts-container">
        </div>
        <div class="width-100 text-center hidden" id="suggested-ytposts-container">
        </div>
        <div class="width-100 text-center hidden" id="suggested-ggposts-container">
        </div>
    </section>
@endsection

@section('after-scripts')
    <script src="{{asset('js/backend/suggested-posts.js')}}"></script>
@endsection
