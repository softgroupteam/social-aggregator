@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Page Header
            <small>Optional description</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="row" >
                @foreach ($posts->data as $key => $post)

                    <div class="col-md-4 ">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                @if (isset($post->full_picture))
                                    <img src={{$post->full_picture}} style="width:100%">
                                @endif
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <p class="box-title">
                                            @if (isset($post->message))
                                                {{$post->message}}
                                            @elseif (isset($post->story))
                                                {{$post->story}}
                                            @endif
                                        </p>
                                        <div class="box-tools pull-right">
                                        </div><!-- /.box-tools -->
                                    </div><!-- /.box-header -->
                                    <div class="box-body">
                                        <button type="button" class="btn btn-success">Publish</button>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                            </div>
                        </div>
                    </div>
                    @if((($key+1) % 3 == 0) and ($key != 0))
            </div>
            <div class="row">
                @endif
                @endforeach
            </div>
        </div>
    </section>
@endsection