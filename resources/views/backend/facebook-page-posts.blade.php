@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Facebook
            {{--<small>Optional description</small>--}}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>--}}
            {{--<li class="active">Here</li>--}}
        {{--</ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="container text-center">
        @foreach ($posts->data as  $post)

                <div class="panel panel-default" style="width:350px; display:inline-block; margin:5px;">
                    <div class="panel-body">
                        @if($post->embed_video_html != '')
                            <div class="fb-post-embed-wrapper" >{!!$post->embed_video_html!!}</div>
                        @elseif (isset($post->full_picture))
                            <img src={{$post->full_picture}} style="width:100%">
                        @endif
                            <div class="box box-primary">
                                <p class="text-blue" >{{$post->createdLocal}}</p>
                                <div class="fb-post-statistics">
                                    <span><i class="fa fa-share-square-o"></i>{{(isset($post->shares))?$post->shares->count:'0'}}</span>
                                    <span><i class="fa fa-comment"></i>{{$post->comments->summary->total_count}}</span>
                                    <span><i class="fa fa-thumbs-up"></i>{{$post->likes->summary->total_count}}</span>
                                </div>
                                <div class="box-header with-border">
                                    <p class="box-title post-text">
                                        @if (isset($post->message))
                                            {!!$post->message!!}
                                        @elseif (isset($post->story))
                                            {!! $post->story !!}
                                        @endif
                                        @if (isset($post->link))
                                            {!!' <a href="'.$post->link.'">'.$post->link.'</a>'!!}
                                            @endif
                                    </p>
                                    <div class="box-tools pull-right">
                                    </div><!-- /.box-tools -->
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <button  type="button" class="btn btn-success save-fbpost {{($post->state == 'saved_published' || $post->state == 'saved_notpublished' )?'hidden':''}}" data-post-id="{{$post->id}}">Save</button>
                                    <button  type="button" class="btn btn-danger delete-fbpost {{($post->state == 'notsaved')?'hidden':''}}" data-post-id="{{$post->id}}">Delete</button>
                                    <button  type="button" class="btn btn-info publish-fbpost {{($post->state == 'notsaved' || $post->state == 'saved_published' )?'hidden':''}}" data-post-id="{{$post->id}}">Publish</button>
                                    <button  type="button" class="btn btn-info unpublish-fbpost {{($post->state == 'saved_notpublished' || $post->state == 'notsaved' )?'hidden':''}}" data-post-id="{{$post->id}}">Unpublish</button>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                    </div>
                </div>

        @endforeach

        <div class="add-more-block col-md-12 text-center">
            <button class="add-more-posts btn btn-default"
                    data-action="{{route('ajax.get-posts-by-page')}}"
                    data-nextpage="{{$nextPage}}">Add more...</button>
        </div>

    </div>
    </section>
@endsection

@section('after-scripts')
    <script src="{{asset('js/backend/fb-posts.js')}}"></script>
@endsection
