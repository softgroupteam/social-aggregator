@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Profile
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="{{Storage::url($user->userInfo->avatar)}}" alt="User profile picture">
                        <div class="uploadUserAvatar" data-toggle="tooltip" data-placement="top" title="change avatar">
                            <input type="file" id="userAvatarInput"/>
                        </div>
                        <h3 class="profile-username text-center">{{$user->name}}</h3>

                        <p class="text-muted text-center">{{$user->userInfo->userOccupation->name}}</p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Followers</b> <a class="pull-right">{{$followersCount}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Following</b> <a class="pull-right">{{$followingCount}}</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">About Me</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                        <p class="text-muted">{{$user->userInfo->city}}, {{$user->userInfo->country_name}}</p>
                        <hr>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#profile-account-management" data-toggle="tab">Account management</a></li>
                        <li class=""><a href="#settings" data-toggle="tab">Settings</a></li>
                        @if(auth()->user()->hasRole('user'))
                            <li class=""><a href="#profile-my-streams" data-toggle="tab">My streams</a></li>
                        @endif
                        <li class=""><a href="#profile-users" data-toggle="tab">Users</a></li>
                        <li class=""><a href="#profile-followers" data-toggle="tab">Followers</a></li>
                        <li class=""><a href="#profile-following" data-toggle="tab">Following</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="profile-account-management">
                            <div class="nav-tabs-custom" style="position:relative;">
                                <a class="btn btn-primary add-social-acc" href="{{route('social.auth',['provider' => 'facebook'])}}">Add facebook account/page</a>
                                <ul class="nav nav-tabs">
                                    <li class="active profile-account-management-facebook-tab"><a href="#profile-account-management-facebook" data-toggle="tab">Facebook</a></li>
                                    <li class="profile-account-management-twitter-tab"><a href="#profile-account-management-twitter" data-toggle="tab">Twitter</a></li>
                                    <li class="profile-account-management-google-tab"><a href="#profile-account-management-google" data-toggle="tab">Google</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile-account-management-facebook">
                                        <table id="profile-fb-social-accounts" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Type</th>
                                                    <th class="text-center">Actions</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="profile-account-management-twitter">
                                        <table id="profile-tw-social-accounts" class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="profile-account-management-google">
                                        <table id="profile-gg-social-accounts" class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="settings">
                            <label class="text-center text-info">Edit main info</label>
                            <form id="maininfo-profile-form" class="form-horizontal">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-sm-3 control-label">Name</label>

                                    <div class="col-sm-9">
                                        <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-sm-3 control-label">E-Mail Address</label>

                                    <div class="col-sm-9">
                                        <input id="email" type="email" class="form-control" name="email" value="{{$user->email}}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('occupation') ? ' has-error' : '' }}">
                                    <label for="occupation" class="col-sm-3 control-label">Occupation</label>

                                    <div class="col-sm-9">
                                        <select name="occupation" class="form-control category-select">
                                            @foreach($occupations as $occupation)
                                                <option value="{{$occupation->id}}" {{(($occupation->id == $user->userInfo->userOccupation->id)?'selected':'')}}>{{$occupation->name}}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('occupation'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('occupation') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('interest') ? ' has-error' : '' }}">
                                    <label for="interest" class="col-sm-3 control-label">Bio & Area of Specialization/Interest</label>

                                    <div class="col-sm-9">
                                        <input id="interest" type="text" class="form-control" name="interest" value="{{ $user->userInfo->interest }}">

                                        @if ($errors->has('interest'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('interest') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button id="submit-maininfo-edit" type="submit" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                            <hr class="text-info">
                            <label class="text-center text-info">Edit password</label>
                            <form id="password-profile-form" class="form-horizontal">
                                <div class="form-group{{ $errors->has('old-password') ? ' has-error' : '' }}">
                                    <label for="old-password" class="col-sm-3 control-label">Old Password</label>

                                    <div class="col-sm-9">
                                        <input id="old-password" type="password" class="form-control" name="old-password" required>

                                        @if ($errors->has('old-password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('old-password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                    <label for="new-password" class="col-sm-3 control-label">New password</label>

                                    <div class="col-sm-9">
                                        <input id="new-password" type="password" class="form-control" name="new-password" required>

                                        @if ($errors->has('new-password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('new-password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="confirm-new-password" class="col-sm-3 control-label">Confirm new password</label>

                                    <div class="col-sm-9">
                                        <input id="confirm-new-password" type="password" class="form-control" name="confirm-new-password" required>

                                        @if ($errors->has('confirm-new-password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('confirm-new-password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button id="submit-password-edit" type="submit" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane row text-center" id="profile-my-streams">
                        </div>
                        <div class="tab-pane" id="profile-users">
                        </div>
                        <div class="tab-pane" id="profile-followers">
                        </div>
                        <div class="tab-pane" id="profile-following">
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
    </section>

    @if(isset($socialUser) || isset($socialFbPages))
    <div class="modal fade" id="fb-callback-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Choose account/page</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="form-facebook-linking">
                        <h2>Account</h2>
                        <hr>
                        <div class="user-acc">
                            <label class="wrapper-label-item">
                                <img class="picture" src="{{$socialUser->avatar}}">
                                <label class="checkbox-label">{{$socialUser->name}} <input type="checkbox" name="fb_user" value="{{json_encode($socialUser)}}" class="pull-right"></label>
                            </label>
                        </div>
                        <h2>Pages</h2>
                        <hr>
                        @foreach($socialFbPages->data as $page)
                            <div class="user-page">
                                <label class="wrapper-label-item">
                                    <img class="picture" src="http://graph.facebook.com/{{$page->id}}/picture">
                                    <label class="checkbox-label">{{$page->name}} <input type="checkbox" name="fb_pages[{{$loop->iteration}}]" value="{{json_encode($page)}}" class="pull-right"></label>
                                </label>
                            </div>
                        @endforeach
                        <input id="fb-user-id" name="fb-user-id" type="hidden" value="{{ ( isset($socialFbPages->userId) ? $socialFbPages->userId : '') }}">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="submit-fb-linking" type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('after-scripts')
    @if(isset($socialUser) || isset($socialFbPages))
        <script>
            $( document ).ready(function() {
                window.history.replaceState('Dashboard', 'Dashboard', '/admin/dashboard/profile');
                $('#fb-callback-modal').modal('show');
                $('#submit-fb-linking').on('click',function(e){
                    e.preventDefault();
                    var formData = new FormData($('#form-facebook-linking')[0]);
                    $.ajax({
                        url: laroute.route('ajax.link-fb-entities'),
                        type: 'POST',
                        data: formData,
                        async: true,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            console.log(data);
                            if(data.status == 'success') {
                                toastr.success(data.success_msg);
                                window.location = laroute.route('profile');
                            }
                            else{
                                $.each(data.errors, function(key, item) {
                                    toastr.warning(item);
                                });
                            }
                        }
                    });
                });
            });
        </script>
    @endif
    <script src="{{asset("AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
    <script src="{{asset('js/backend/profile.js')}}"></script>
@endsection