@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Social streams
        </h1>
    </section>
    <section class="content">
        <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#facebook-streams" data-toggle="tab">Facebook</a></li>
                    <li class=""><a href="#twitter-streams" data-toggle="tab">Twitter</a></li>
                    <li class=""><a href="#youtube-streams" data-toggle="tab">Youtube</a></li>
                    <li class=""><a href="#google-streams" data-toggle="tab">Google</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="facebook-streams">
                        @if($fbUser)
                        <form id="add-facebook-stream-form" class="form-inline v-mrgn-10">
                            <div class="form-group" style="width:50%;">
                                <input type="text" class="form-control stream-url" id="fb-stream-url" name="fb-stream-url" placeholder="enter url" style="width:100%;">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control stream-title" id="fb-stream-title" name="fb-stream-title" placeholder="enter title">
                            </div>
                            {{--<div class="form-group">--}}
                                 {{--<select name="category" class="form-control category-select">--}}
                                     {{--@foreach($streamCategories as $category)--}}
                                         {{--<option value ={{$category->id}} >{{$category->text}}</option>--}}
                                     {{--@endforeach--}}
                                 {{--</select>--}}
                            {{--</div>--}}
                            <button type="button" data-social-type="facebook" class="btn btn-success add-stream">add</button>
                        </form>
                        <hr>
                        <table id="facebook-streams-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width:70px;">Number</th>
                                <th>Id</th>
                                <th>Title</th>
                                <th style="width:70px;">Action</th>
                            </tr>
                            </thead>
                        </table>
                        @else
                            <div class="box box-warning">
                                <div class="box-header">
                                    <h3 class="box-title"><a href="{{route('profile')}}" style="cursor:pointer;">Link your facebook account</a></h3>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="tab-pane" id="twitter-streams">
                        <form id="add-twitter-stream-form" class="form-inline v-mrgn-10">
                            <div class="form-group" style="width:50%;">
                                <input type="text" class="form-control stream-url" id="tw-stream-url" name="tw-stream-url" placeholder="enter url" style="width:100%;">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control stream-title" id="tw-stream-title" name="tw-stream-title" placeholder="enter title">
                            </div>
                            <button type="button" data-social-type="twitter" class="btn btn-success add-stream">add</button>
                        </form>
                        <hr>
                        <table id="twitter-streams-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width:70px;">Number</th>
                                <th>Id</th>
                                <th>Title</th>
                                <th style="width:70px;">Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="tab-pane" id="youtube-streams">
                        <form id="add-youtube-stream-form" class="form-inline v-mrgn-10">
                            <div class="form-group" style="width:50%;">
                                <input type="text" class="form-control stream-url" id="yt-stream-url" name="yt-stream-url" placeholder="enter url" style="width:100%;">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control stream-title" id="yt-stream-title" name="yt-stream-title" placeholder="enter title">
                            </div>
                            <button type="button" data-social-type="youtube" class="btn btn-success add-stream">add</button>
                        </form>
                        <hr>
                        <table id="youtube-streams-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width:70px;">Number</th>
                                <th>Id</th>
                                <th>Title</th>
                                <th style="width:70px;">Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="tab-pane" id="google-streams">
                        <form id="add-google-stream-form" class="form-inline v-mrgn-10">
                            <div class="form-group" style="width:50%;">
                                <input type="text" class="form-control stream-url" id="gg-stream-url" name="gg-stream-url" placeholder="enter url" style="width:100%;">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control stream-title" id="gg-stream-title" name="gg-stream-title" placeholder="enter title">
                            </div>
                            <button type="button" data-social-type="google" class="btn btn-success add-stream">add</button>
                        </form>
                        <hr>
                        <table id="google-streams-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width:70px;">Number</th>
                                <th>Id</th>
                                <th>Title</th>
                                <th style="width:70px;">Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
        </div>
    </section>
@endsection

@section('after-scripts')
    <script src="{{asset("AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
    <script src="{{asset("js/backend/social-streams.js")}}"></script>
@endsection

