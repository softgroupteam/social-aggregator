<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Foodrige Social
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="#">Foodrige Social</a>.</strong> All rights reserved.
</footer>