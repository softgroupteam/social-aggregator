<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
    {{--<div class="user-panel">--}}
    {{--<div class="pull-left image">--}}
    {{--<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">--}}
    {{--</div>--}}
    {{--<div class="pull-left info">--}}
    {{--<p>Alexander Pierce</p>--}}
    {{--<!-- Status -->--}}
    {{--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
    {{--</div>--}}
    {{--</div>--}}

    <!-- search form (Optional) -->
    {{--<form action="#" method="get" class="sidebar-form">--}}
    {{--<div class="input-group">--}}
    {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
    {{--<span class="input-group-btn">--}}
    {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
    {{--</button>--}}
    {{--</span>--}}
    {{--</div>--}}
    {{--</form>--}}
    <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            @if(auth()->user()->hasRole('superadministrator'))
                {{--@if (count($accounts['facebook'])>0)--}}
                {{--<li class="treeview">--}}
                    {{--<a href="#"><i class="fa fa-facebook"></i> <span>Facebook pages</span>--}}
                        {{--<span class="pull-right-container">--}}
                            {{--<i class="fa fa-angle-left pull-right"></i>--}}
                         {{--</span>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--<li><a href={{route('get-posts-by-fbuser',['providerId'=>$accounts['facebook'][0]->provider_id])}}>My posts</a></li>--}}
                        {{--<li><a href={{route('get-feed-by-fbuser',['providerId'=>$accounts['facebook'][0]->provider_id])}}>My feed</a></li>--}}
                        {{--@if (count($fbPages)>0)--}}
                            {{--@foreach ($fbPages as $page)--}}
                                 {{--<li><a href={{route('get-posts-by-page',['providerId'=>$accounts['facebook'][0]->provider_id,'pageTitle'=>$page->machine_name])}}>{{$page->title}}<span data-id="{{$page->id}}" data-social="facebook" class="fa fa-times pull-right text-red delete-social-tab"></span></a></li>--}}
                            {{--@endforeach--}}
                        {{--@else--}}
                            {{--<li><a>No pages added</a></li>--}}
                        {{--@endif--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--@endif--}}
                {{--<li class="treeview">--}}
                    {{--<a href="#"><i class="fa fa-youtube"></i> <span>Youtube channels</span>--}}
                        {{--<span class="pull-right-container">--}}
                            {{--<i class="fa fa-angle-left pull-right"></i>--}}
                        {{--</span>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--@if (count($ytChannels)>0)--}}
                            {{--@foreach ($ytChannels as $channel)--}}
                        {{--<li><a href={{route('get-feed-by-youtubeChannel',['channelId' => $channel->channel_id])}}>{{$channel->title}}<span data-id="{{$channel->id}}" data-social="youtube" class="fa fa-times pull-right text-red delete-social-tab"></span></a></li>--}}
                            {{--@endforeach--}}
                            {{--@else--}}
                            {{--<li><a>No channels added</a></li>--}}
                        {{--@endif--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="treeview">--}}
                    {{--<a href="#"><i class="fa fa-twitter"></i> <span>Twitter pages</span>--}}
                        {{--<span class="pull-right-container">--}}
                            {{--<i class="fa fa-angle-left pull-right"></i>--}}
                        {{--</span>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--@if (count($twPages)>0)--}}
                            {{--@foreach ($twPages as $page)--}}
                                 {{--<li><a href={{route('get-tweets-by-user',['userName' => $page->page_name])}}>{{$page->title}}<span data-id="{{$page->id}}" data-social="twitter" class="fa fa-times pull-right text-red delete-social-tab"></span></a></li>--}}
                            {{--@endforeach--}}
                        {{--@else--}}
                            {{--<li><a>No pages added</a></li>--}}
                        {{--@endif--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li class="treeview">--}}
                    {{--<a href="#"><i class="fa fa-google-plus"></i> <span>Google+</span>--}}
                        {{--<span class="pull-right-container">--}}
                            {{--<i class="fa fa-angle-left pull-right"></i>--}}
                        {{--</span>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--@if (count($gpChannels)>0)--}}
                            {{--@foreach ($gpChannels as $channel)--}}
                                {{--<li><a href={{route('google-plus',['channelId' => $channel->channel_id])}}>{{$channel->title}}<span data-id="{{$channel->id}}" data-social="google_plus" class="fa fa-times pull-right text-red delete-social-tab"></span></a></li>--}}
                            {{--@endforeach--}}
                        {{--@else--}}
                            {{--<li><a>No channels added</a></li>--}}
                        {{--@endif--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <li><a href="{{ route('get-saved-posts-dashboard') }}"><i class="fa fa-floppy-o"></i><span>Saved Posts</span></a></li>
            @endif
            @if(auth()->user()->hasRole('user'))
                <li><a href="{{ route('get-suggested-posts-page') }}"><i class="fa fa-list"></i><span>Foodridge suggested posts</span></a></li>
                <li><a href="{{ route('get-users-suggested-posts-page') }}"><i class="fa fa-list-alt"></i><span>Users suggested posts</span></a></li>
                <li><a href="{{ route('get-my-favourite-posts') }}"><i class="fa fa-book"></i><span>Favourite</span></a></li>
            @endif
            <li><a href="{{ route('social-streams-page') }}"><i class="fa fa-exchange"></i><span>Social streams</span></a></li>
            <li><a href="{{ route('promote-pages') }}"><i class="fa fa-share-alt"></i><span>Promote Pages</span></a></li>
            <li><a href="{{ route('composer') }}"><i class="fa fa-pencil-square-o"></i><span>Composer</span></a></li>
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i><span>To front page</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>