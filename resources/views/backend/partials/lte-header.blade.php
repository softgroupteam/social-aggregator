<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{route('dashboard')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>FR</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Foodridge</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                {{--@if(auth()->user()->hasRole('superadministrator'))--}}
                    {{--<li class="dropdown">--}}
                        {{--<!-- Menu toggle button -->--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                            {{--Social streams--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li class="header toogle-modal-with-categories"><a  data-target="#fbpage-modal" href="">Add facebook page</a></li>--}}
                            {{--<li class="header toogle-modal-with-categories"><a  data-target="#twpage-modal" href="">Add twitter page</a></li>--}}
                            {{--<li class="header toogle-modal-with-categories"><a  data-target="#ytchannel-modal" href="">Add youtube channel</a></li>--}}
                            {{--<li class="header toogle-modal-with-categories"><a  data-target="#gpchannel-modal" href="">Add google+ channel</a></li>--}}
                            {{--<li class="header"><a  data-toggle="modal" data-target="#category-modal" href="">Add category</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--@endif--}}
                <li class="header"><a href="{{route("profile")}}">Profile</a></li>
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{asset('img/mock/mock_user_avatar_small.png')}}" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{auth()->user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{Storage::url(auth()->user()->userInfo->avatar)}}" class="img-circle profile-user-img" alt="User Image">

                            <p>
                                {{auth()->user()->name}}
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <!-- /.row -->
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{route('profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{route('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>

{{--@include('backend.partials.add-fbpage-modal')--}}
{{--@include('backend.partials.add-ytchannel-modal')--}}
{{--@include('backend.partials.add-gpchannel-modal')--}}
{{--@include('backend.partials.add-twpage-modal')--}}
@include('backend.modals.delete-tab-modal')
@include('backend.modals.confirmation-modal')
{{--@include('backend.modals.add-category-modal')--}}

