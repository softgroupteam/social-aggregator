<div class="modal fade" id="gpchannel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Google+ channel</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-add-gp-channel">
                    <fieldset>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Human-friendly title</label>
                            <div class="col-md-4">
                                <input  name="gpchannelTitle" type="text"  class="form-control input-md" required>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Machine name</label>
                            <div class="col-md-4">
                                <input  name="gpchannelId" type="text"  class="form-control input-md" required>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Category</label>
                            <div class="col-md-4">
                                <select name="category" class="form-control category-select">
                                </select>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="submit-gpchannel" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>

</script>