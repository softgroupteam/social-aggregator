<div class="modal fade" id="ytchannel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add youtube channel</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-add-yt-channel">
                    <fieldset>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Human-friendly title</label>
                            <div class="col-md-4">
                                <input  name="ytchannelTitle" type="text"  class="form-control input-md" required>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Channel id</label>
                            <div class="col-md-4">
                                <input  name="ytchannelId" type="text"  class="form-control input-md" required>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Category</label>
                            <div class="col-md-4">
                                <select name="category" class="form-control category-select">
                                </select>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="submit-ytchannel" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
