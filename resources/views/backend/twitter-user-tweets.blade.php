@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
            Twitter
            {{--<small>Optional description</small>--}}
        </h1>
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>--}}
            {{--<li class="active">Here</li>--}}
        {{--</ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
    <div  class="container text-center" >
        @foreach ($timeline as $item)
            <div class="panel panel-default " style="width:350px; display:inline-block; margin:5px;">
                <div class="panel-body">
                    @if (property_exists($item->entities,'media'))
                        @foreach ($item->entities->media as $media)
                            @if($media->type == 'photo')
                                <img src={{$media->media_url}} style="width:100%;height:auto;">
                            @endif
                        @endforeach
                    @endif
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <p class="text-blue" >{{$item->createdLocal}}</p>
                                <div class="tw-post-statistics">
                                    <span><i class="fa fa-retweet "></i>{{$item->retweet_count}}</span>
                                    <span><i class="fa fa-star-o "></i>{{$item->favorite_count}}</span>
                                </div>
                                <p class="box-title text-center">
                                    @if (isset($item->formatted_text))
                                        {!!$item->formatted_text!!}
                                    @endif
                                </p>
                                <div class="box-tools pull-right">
                                </div><!-- /.box-tools -->
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <button  type="button" class="btn btn-success save-twpost {{($item->state == 'saved_published' || $item->state == 'saved_notpublished' )?'hidden':''}}" data-post-id="{{$item->id}}">Save</button>
                                <button  type="button" class="btn btn-danger delete-twpost {{($item->state == 'notsaved')?'hidden':''}}" data-post-id="{{$item->id}}">Delete</button>
                                <button  type="button" class="btn btn-info publish-twpost {{($item->state == 'notsaved' || $item->state == 'saved_published' )?'hidden':''}}" data-post-id="{{$item->id}}">Publish</button>
                                <button  type="button" class="btn btn-info unpublish-twpost {{($item->state == 'saved_notpublished' || $item->state == 'notsaved' )?'hidden':''}}" data-post-id="{{$item->id}}">Unpublish</button>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                </div>
            </div>
        @endforeach
        <div class="add-more-block col-md-12 text-center">
            <button class="add-more-posts btn btn-default"
                    data-action="{{route('ajax.get-tweets-by-user')}}"
                    data-lastelement="{{$lastElementId}}"
                    data-twitterusername="{{$twitterUserName}}">Add more...</button>
        </div>
    </div>
    </section>
@endsection

@section('after-scripts')
    <script src="{{asset('js/backend/tw-posts.js')}}"></script>
@endsection

