@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1 class="text-center">
            Welcome to dashboard
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="modal fade" id="fb-callback-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Choose account/page</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="form-facebook-linking">
                            <h2>Account</h2>
                            <hr>
                            <div class="user-acc">
                                <label class="wrapper-label-item">
                                    <img class="picture" src="{{$user->avatar}}">
                                    <label class="checkbox-label">{{$user->name}} <input type="checkbox" name="fb_user" value="{{json_encode($user)}}" class="pull-right"></label>
                                </label>
                            </div>
                            <h2>Pages</h2>
                            <hr>
                            @foreach($pages->data as $page)
                                <div class="user-page">
                                    <label class="wrapper-label-item">
                                        <img class="picture" src="http://graph.facebook.com/{{$page->id}}/picture">
                                        <label class="checkbox-label">{{$page->name}} <input type="checkbox" name="fb_pages[{{$loop->iteration}}]" value="{{json_encode($page)}}" class="pull-right"></label>
                                    </label>
                                </div>
                            @endforeach
                            <input id="fb-user-id" name="fb-user-id" type="hidden" value="{{ ( isset($pages->userId) ? $pages->userId : '') }}">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="submit-fb-linking" type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('after-scripts')
    <script>
        $( document ).ready(function() {
            window.history.replaceState('Dashboard', 'Dashboard', '/admin/dashboard');
            $('#fb-callback-modal').modal('show');
            $('#submit-fb-linking').on('click',function(e){
                e.preventDefault();
                var formData = new FormData($('#form-facebook-linking')[0]);
                $.ajax({
                    url: laroute.route('ajax.link-fb-entities'),
                    type: 'POST',
                    data: formData,
                    async: true,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        console.log(data);
                        if(data.status == 'success') {
                            toastr.success(data.success_msg);
                            window.location = laroute.route('dashboard');
                        }
                        else{
                            $.each(data.errors, function(key, item) {
                                toastr.warning(item);
                            });
                        }
                    }
                });
            });
        });
    </script>
@endsection