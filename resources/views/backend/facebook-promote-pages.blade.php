@extends('backend.layouts.lte')

@section('content')
    <section class="content-header">
        <h1>
        Promote pages
        </h1>
    </section>
    <section class="content">
        <div class="container">
            @if($fbUser)
            <h3 class="promote-step">Step 1. Choose keywords related to your needs</h3>
            <form class="form-inline" id="keywords-form">
                @foreach($keywords as $keyword)
                    <div class="form-group">
                        <label for="keyword[{{$loop->index}}]">Keyword {{$loop->index+1}}:</label>
                        <input type="text" class="form-control"  name="keyword[{{$loop->index}}]" value="{{$keyword->name}}">
                    </div>
                @endforeach
                @for($i=count($keywords);$i<3;$i++)
                        <div class="form-group">
                            <label for="keyword[{{$i}}]">Keyword {{$i+1}}:</label>
                            <input type="text" class="form-control"  name="keyword[{{$i}}]" value="">
                        </div>
                    @endfor
                <button type="button" class="btn btn-default" id="save-keywords-button">Search</button>
            </form>
            <hr>
            <h3 class="promote-step">Step 2. Choose pages to promote to</h3>
            <div class="box box-info">
                <div class="box-header with-border  text-center">
                    <h3 class="box-title">Fetched pages</h3>
                    <div class="spinner-stack promote-pages-spinner hidden" style="position:absolute; right:0;top:10px;">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                    </div>
                </div>
                <table id="promote-pages-table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th >Id</th>
                        <th >Name</th>
                        <th >Likes</th>
                        <th >Talking about</th>
                        <th >Promote</th>
                    </tr>
                    </thead>
                </table>
            </div>
            @else
                <div class="box box-warning">
                    <div class="box-header text-center">
                        <h3 class="box-title"><a href="{{route('profile')}}" style="cursor:pointer;">Link your facebook account to unlock this feature</a></h3>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection

@section('after-scripts')
    <script src="{{asset("AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
    <script src="{{asset('js/backend/promote-pages.js')}}"></script>
@endsection