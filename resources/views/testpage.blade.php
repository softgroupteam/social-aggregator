<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Foodridge Social</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
            margin-top: 100px;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        /*social stream*/
        .social-stream-container{
            margin: auto;
        }

        .social-stream-item{
            height: auto;
            width: 300px;
            margin-top: 50px;
            float: left;
            margin-right: 10px;
            margin-left: 10px;
        }

        .social-stream-item img {
            width:100%;
        }

        .social-stream-header{
            border: 1px solid #E6AE48;
        }

        .social-stream-item .social-stream-time {
            color:#E6AE48;
        }

        .social-stream-item a.read-more {
            color:white;
            background: #4F4F4F;

            text-decoration: none;
        }

        button.social-filter{
            padding: 10px;
            color:white;
            background: #4F4F4F;
        }

        .add-more-published-block{
            margin-bottom:20px;
            margin-top:50px;
        }


    </style>
    <link rel="stylesheet" href="{{asset('magnificPopup/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('AdminLTE/bootstrap/css/bootstrap.min.css')}}">
</head>
<body>
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
                <a href="{{ url('/') }}">Home</a>
                <a href="{{route('dashboard')}}">Dashboard</a>
                <a href="{{ url('/logout') }}">Logout</a>
            @else
                <a href="{{ url('/login') }}">Login</a>
                <a href="{{ url('/register') }}">Register</a>
            @endif
        </div>
    @endif

    <div class="container uss-container" style="margin-top:100px;">
            <div id="filters" class="button-group text-center filters-button-group">
                <button class="button social-filter is-checked" data-filter="*">All</button>
                <button class="button social-filter" data-filter=".uss-facebook">Facebook</button>
                <button class="button social-filter" data-filter=".uss-twitter">Twitter</button>
                <button class="button social-filter" data-filter=".uss-youtube">Youtube</button>
                <button class="button social-filter" data-filter=".uss-google">Google+</button>
            </div>
            <div class="social-stream-container">
            </div>
    </div>

    {{--popup--}}
    <div id="test-popup" class="white-popup mfp-hide" style="background:white;width:400px;height:400px;margin:auto;">
        Popup content
    </div>

<script src="{{asset('AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script src="{{asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('magnificPopup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/laroute.js')}}"></script>
    <script src="{{asset('js/jstz.min.js')}}"></script>
    <script src="{{asset('js/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('js/frontend/mainpage-posts.js')}}"></script>

</body>
</html>
